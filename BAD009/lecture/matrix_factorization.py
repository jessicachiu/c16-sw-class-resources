import numpy
 
def matrix_factorization(R, P, Q, K, steps=20000, learning_rate=0.0002, beta=0.02):
    Q = Q.T
    for step in range(steps):
        # P同Q 就係weight
        for i in range(len(R)):
            for j in range(len(R[i])):
                if R[i][j] > 0:
                    eij = R[i][j] - numpy.dot(P[i,:],Q[:,j])
                    for k in range(K):
                        P[i][k] = P[i][k] + learning_rate * (2 * eij * Q[k][j] - beta * P[i][k])
                        Q[k][j] = Q[k][j] + learning_rate * (2 * eij * P[i][k] - beta * Q[k][j])
        eR = numpy.dot(P,Q)
        # e 就係loss
        e = 0
        for i in range(len(R)):
            for j in range(len(R[i])):
                if R[i][j] > 0:
                    e = e + pow(R[i][j] - eR[i][j], 2)
                    for k in range(K):
                        e = e + (beta/2) * (pow(P[i][k],2) + pow(Q[k][j],2))
        if e < 0.001:
            break
    return P, Q.T
    
R = [
     [5,3,0,1],
     [4,0,0,1],
     [1,1,0,5],
     [1,0,0,4],
     [0,1,5,4],
    ]
R = numpy.array(R)
N = len(R)
M = len(R[0])
K = 2
 
P = numpy.random.rand(N,K)
Q = numpy.random.rand(M,K)
 
nP, nQ = matrix_factorization(R, P, Q, K)
# nP and nQ are the model 
print(nP)
print(nQ)

nR = numpy.dot(nP, nQ.T)
print("=== Difference Between R and nR")
print(nR - R)

print("=== Round nR ====")
print(numpy.round(nR))