setTimeout(function(){
},1000);


function Sleep(ms : number){
    return new Promise<string>(resolve=>{
        setTimeout(function(){
            resolve('dd')
        },ms);
    })
}

Sleep(2000).then((res)=>{
    console.log(res);
    
})

async function main(){
   await Sleep(1000)

 await Sleep(1000)
    
}
import readLine from 'readline';

function readLineProm(){
    return new Promise((resolve,reject)=>{
        try{
        const readLineInterface = readLine.createInterface({
            input: process.stdin,
            output: process.stdout
        })

        readLineInterface.question("What is your name?",(answer:string)=>{
            console.log(`Your name is ${answer}`);
            resolve(answer);
            readLineInterface.close();
        });
    }catch(err){
            reject(err);
        }
    })
}

async function readLineResult(){
    await readLineProm()
}

main()
readLineResult()
