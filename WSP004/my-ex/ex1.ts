import fs from "fs";
function fsReadFilePromise(filePath: string): Promise<Buffer> {
    return new Promise<any>(function (resolve, reject) {
        fs.readFile(filePath, function(err:Error, data){
            if (err) {
                reject(err);
                return;
            }
            resolve(data)
        })
    });
}

function fsWriteFilePromise(filePath: string, data: any, options: fs.WriteFileOptions): Promise<void> {
    return new Promise<any>(function (resolve, reject) {
        fs.writeFile(filePath, data, options, function (err: Error) {
            if (err) {
                reject(err);
                return;
            }
            resolve("");
        }
        )
});
}

fsWriteFilePromise(
    "testing.txt",
    "this is a test",
    'utf-8'   
)
    .then(function(){
        console.log("File Written!!");
    })
    .catch(function(err){
        console.log(err);
    })

    fsReadFilePromise(
        "testing.txt"
    )
        .then(function(data){
            console.log(data.toString("utf8"));
            
        })
        .catch(function(err){
            console.log(err);
        })