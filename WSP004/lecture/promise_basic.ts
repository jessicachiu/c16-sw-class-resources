import fs from "fs";
function fsWriteFilePromise(
    file: string,
    content: string,
    options: { flag: string }
) {
    // Array<string>
    return new Promise<string>(function (resolve, reject) {
        fs.writeFile(file, content, options, function (err: Error) {
            if (err) {
                reject(err);
                return;
            }
            resolve("Write file ok");
        });
    });
}

fsWriteFilePromise(
    "quotes-dijkstra.txt",
    "Simplicity is prerequisite for reliability.\n",
    { flag: "w" }
)
    .then(function (data) {
        console.log("Content is written!:", data);
    })
    .catch(function (err) {
        console.log(err);
    });

function fsWriteFilePromise2(fileName: string, content: string) {
    return new Promise<any>((resolve, reject) => {
        fs.writeFile(fileName, content, function (err: Error) {
            if (err) {
                reject(err);
                return;
            }
            resolve("");
        });
    });
}

fsWriteFilePromise2("test.js", "demo").then((res) => {
    console.log(res);
});
