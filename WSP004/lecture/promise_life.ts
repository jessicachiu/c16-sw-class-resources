// 男
function 承諾上進() {
    return new Promise<string>((resolve, reject) => {
        if (lifeSwitch.承諾上進) {
            resolve("我已結讀緊Tecky");
        } else {
            reject("打多陣機先");
        }
    });
}
function 承諾買樓() {
    return new Promise<string>((resolve, reject) => {
        if (lifeSwitch.承諾買樓) {
            resolve("清水灣8號");
        } else {
            reject("住番公屋先。。");
        }
    });
}
function 承諾結婚() {
    return new Promise<string>((resolve, reject) => {
        setTimeout(() => {
            if (lifeSwitch.承諾上進) {
                resolve("我地去註冊");
            } else {
                reject("逃婚");
            }
        }, 2000);
    });
}

// 女

const lifeSwitch = {
    承諾上進: false,
    承諾買樓: true,
    承諾結婚: true,
};

// 承諾上進()
//     .then((res) => {
//         console.log(res);
//         return 承諾買樓();
//     })
//     // .catch((error) => {
//     //     console.log(error);
//     //     console.log("唔緊要啦b,睇定樓先");
//     //     return 承諾買樓();
//     // })
//     .then((res) => {
//         console.log(res);
//         return 承諾結婚();
//     })
//     .then((res) => {
//         console.log(res);
//         console.log("美滿人生");
//     })
//     .catch((error) => {
//         console.log(error);
//         console.log("3樣野都做唔到？！分手啦頂你");
//     });

// const 上進承諾 = 承諾上進();
// Promise.all([上進承諾, 承諾買樓(), 承諾結婚()])
//     .then(([上進, 買樓, 結婚]) => {
//         console.table({ 上進, 買樓, 結婚 });
//         console.log("美滿人生");
//     })
//     .catch((error) => {
//         console.log(error);
//         console.log("3樣野都做唔到？！分手啦頂你");
//     });

// const promise1 = Promise.resolve(3);
// const promise2 = Promise.resolve(5);

// promise1.then(function (res) {
//     console.log(res);
// });

async function asyncLife() {
    try {
        console.log("Step 1");
        const 上進 = await 承諾上進();
        console.log("Step 2");
        const 買樓 = await 承諾買樓();
        console.log("Step 3");
        const 結婚 = await 承諾結婚();
        console.log("Step 4");
        console.table({ 上進, 買樓, 結婚 });
    } catch (error) {
        console.log(error);
    }
}

asyncLife();
