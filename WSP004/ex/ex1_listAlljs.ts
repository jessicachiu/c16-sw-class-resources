import fs from "fs";
import path from "path";
import util from "util";

const fsReaddirUtilPromise = util.promisify(fs.readdir);
const fsStatUtilPromise = util.promisify(fs.stat);

async function listAllJsRecursive(dir: string) {
    let jsFiles: string[] = [];
    try {
        const fullFolderPath = path.resolve(dir);
        const files = await fsReaddirUtilPromise(fullFolderPath);

        for (let file of files) {
            const fullFilePath = path.resolve(path.join(fullFolderPath, file));
            const stats = await fsStatUtilPromise(fullFilePath);

            if (path.extname(file) === ".js" && stats.isFile()) {
                jsFiles.push(fullFilePath);
            } else if (stats.isDirectory()) {
                const jsFilesInner = await listAllJsRecursive(fullFilePath);
                jsFiles = jsFiles.concat(jsFilesInner);
            }
        }
    } catch (error) {
        console.log(error);
    }
    return jsFiles;
}
// fs.readdir and fs.stat
// listAllJs(
//   "C:\\Users\\Dickson\\Documents\\Tecky\\hk-map-16-jul-21\\tecky-exercises-solutions-sw\\WSP003\\ex"
// );
async function main() {
    try {
        const resultJsFiles = await listAllJsRecursive("./");
        console.log(resultJsFiles);
    } catch (error) {
        console.log(error);
    }
}

main();
