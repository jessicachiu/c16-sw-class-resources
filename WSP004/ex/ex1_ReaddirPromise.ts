import fs from "fs";

//Call back
// fs.readdir("./", (err, files) => {
//     if (err) {
//         console.log(err);
//         return;
//     }
//     console.log(files);
// });

//Promise

function fsReaddirPromise(dir: string) {
    return new Promise(function (resolve, reject) {
        fs.readdir(dir, (err, files) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(files);
        });
    });
}

// .then
fsReaddirPromise("./")
    .then((files) => {
        console.log(files);
    })
    .catch((error) => {
        console.log(error);
    });

// async await
async function main() {
    try {
        const files = await fsReaddirPromise("./");
        console.log(files);
    } catch (error) {
        console.log(error);
    }
}

main();
