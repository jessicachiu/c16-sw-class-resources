import readline from "readline";

const readLineInterface = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});

// readLineInterface.question("What is your name?", (answer: string) => {
//     console.log(`Your name is ${answer}`);
//     readLineInterface.close();
// });

function readLinePromise(question: string) {
    return new Promise((resolve, reject) => {
        readLineInterface.question(question, (answer: string) => {
            resolve(answer);
        });
    });
}

async function main() {
    const answer = await readLinePromise("How are you?");
    console.log("answer = ", answer);
    const name = await readLinePromise("Whats your name?");
    console.log("name = ", name);
    readLineInterface.close();
}

main();
