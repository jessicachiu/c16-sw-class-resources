import fs from "fs";

// Callback
fs.stat("./", (err, stats) => {
    if (err) {
        console.log(err);
        return;
    }
    console.log(stats);
});

// Promise

function fsStatPromise(filePath: string) {
    return new Promise((resolve, reject) => {
        fs.stat(filePath, (err, stats) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(stats);
        });
    });
}

// .then
fsStatPromise("./")
    .then((stats) => {
        console.log(stats);
    })
    .catch((error) => {
        console.log(error);
    });

// async await
async function main() {
    try {
        const stats = await fsStatPromise("package.json");
        console.log(stats);
    } catch (error) {
        console.log(error);
    }
}
main();
