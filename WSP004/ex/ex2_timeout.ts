import util from "util";

setTimeout(function () {
    // Logic here
}, 1000);

const wait = util.promisify(setTimeout);
console.log(wait);

function sleep(ms: number) {
    return new Promise<string>((resolve, reject) => {
        setTimeout(function () {
            resolve(`You have waited ${ms} ms`);
        }, ms);
    });
}

async function main() {
    console.log("Step 1");
    const msg1 = await sleep(2000);
    console.log(msg1);

    console.log("Step 2");
    const msg2 = await sleep(10);
    console.log(msg2);

    console.log("Step 3");
    const msg3 = await sleep(1000);
    console.log(msg3);

    console.log("Step 4");
}

main();
