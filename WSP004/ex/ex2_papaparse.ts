import fs from "fs";
import papa from "papaparse";

// papa.parse(file, {
//     worker: true, // Don't bog down the main thread if its a big file
//     complete: function (results, file) {
//         console.log(results);
//     },
// });

function papaParsePromise(inputFile: string) {
    return new Promise((resolve, reject) => {
        let file = fs.createReadStream(inputFile);
        papa.parse(file, {
            worker: true, // Don't bog down the main thread if its a big file
            header: true,
            error: function (error) {
                reject(error);
            },
            complete: function (results, file) {
                resolve(results);
            },
        });
    });
}

async function main() {
    try {
        const csvResult = await papaParsePromise("anycsv.csv");
        console.log("csvResult = ", csvResult);
    } catch (error) {
        console.log(error);
    }
}
main();
