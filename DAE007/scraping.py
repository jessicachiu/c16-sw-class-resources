
from playwright.async_api import async_playwright
import asyncio 

from pymongo import MongoClient

# Connect to locatlhost and port 27017
client = MongoClient('localhost',27017)
# Use the database wikipedia
db = client.openrice



async def main():
    async with async_playwright() as p:
        browser = await p.chromium.launch(headless=False)
        page = await browser.new_page()
        all_restaurants = []
        await page.set_viewport_size({"width":960,"height":768});
        for page_number in range(1,3):
            await page.goto("https://www.openrice.com/zh/hongkong/restaurants?where=上環&page={}".format(page_number))
            print("Accessing page number {}".format(page_number))
            restaurants_per_page = await page.evaluate("""()=>{
                    const addresses =  Array.from(document.querySelectorAll('.icon-info.address')).map(e=>e.innerText);
                    const prices = Array.from(document.querySelectorAll('.icon-info.icon-info-food-price')).map(e=>e.innerText);
                    const names= Array.from(document.querySelectorAll('.icon-info.icon-info-food-name')).map(e=>e.innerText);
                    const restaurants = [];
                    for(let index in addresses){
                        restaurants.push({
                                address: addresses[index],
                                price: prices[index],
                                name: names[index]
                        });
                    }
                    return restaurants
                }
            """)
            all_restaurants = all_restaurants + restaurants_per_page
        for restaurant in all_restaurants:
            print(restaurant)
            db.restaurants.insert_one(restaurant)
        await browser.close()


asyncio.run(main())