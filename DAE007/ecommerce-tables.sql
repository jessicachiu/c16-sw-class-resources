
-- Application database
CREATE TABLE users(
    id SERIAL PRIMARY KEY,
    email varchar(255),
    password varchar(255),
    created_at TIMESTAMP default now(),
    updated_at TIMESTAMP default now()
);


CREATE TABLE products(
    id SERIAL PRIMARY KEY,
    name varchar(255),
    price numeric(15,3),
    description text,
    created_at TIMESTAMP default now(),
    updated_at TIMESTAMP default now()
);

CREATE TABLE transactions(
    id SERIAL PRIMARY KEY,
    transaction_date timestamp,
    total numeric(15,3),
    user_id integer,
    created_at TIMESTAMP default now(),
    updated_at TIMESTAMP default now(),
    foreign key (user_id) references users(id)
);

CREATE TABLE transaction_details(
    id SERIAL PRIMARY KEY,
    quantity integer,
    amount numeric(15,3),
    transaction_id integer,
    foreign key (transaction_id) references transactions(id),
    product_id integer,
    foreign key (product_id) references products(id),
    created_at TIMESTAMP default now(),
    updated_at TIMESTAMP default now()
);