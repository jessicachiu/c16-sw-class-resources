#!/usr/bin/env python


def prepare_env():
    global spark
    from pyspark.sql import SparkSession
    import findspark
    findspark.init('/home/gordon/Codes/spark')
    # 你呢個位，需要啲咩drivers ，取決於你要讀寫啲咩system
    packages = [
        "com.amazonaws:aws-java-sdk-s3:1.12.79", # 寫上S3
        "org.apache.hadoop:hadoop-aws:3.2.0", # 寫上S3
        "org.apache.spark:spark-avro_2.12:2.4.4", # 要寫Avro format
        "org.postgresql:postgresql:42.2.24" # 要由postgresql application database 讀嘢
    ]

    # Spark 有大量drivers，可供使用
    spark = SparkSession.builder.appName("Read from postgresql")\
        .config("spark.jars.packages",",".join(packages)).getOrCreate()
    
    return spark
            

def read_dataframes():
    from dotenv import load_dotenv
    import os 
    load_dotenv()

    return spark.read.format('jdbc')\
         .option('url',"jdbc:postgresql://localhost:5432/"+os.getenv("DB_NAME"))\
         .option('dbtable',"(SELECT products.name,transaction_details.amount FROM products inner join transaction_details on products.id = transaction_details.product_id) product_transaction_details")\
         .option('user',os.getenv('DB_USERNAME'))\
         .option('password',os.getenv('DB_PASSWORD'))\
         .option('driver','org.postgresql.Driver').load()

def main():
    # 1. prepare environment
    prepare_env()

    # 2. Extract
    df = read_dataframes()

    df.show()

    # 3. Transform
    # Similar to the one before


if __name__ == "__main__":
    main()

