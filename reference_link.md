## Reference Link

### Tool


 | Type               | Description                            |
 | :----------------- | :------------------------------------- |
 | MD generator       | https://readme.so/                     |
 | Design             | https://www.figma.com/                 |
 | Design             | https://drawio-app.com/                |
 | Design \| ERD      | https://app.diagrams.net/              |
 | Design \| ERD      | https://drawsql.app/templates/koel     |
 | Design \| ERD      | https://app.quickdatabasediagrams.com/ |
 | Design \| ERD      | https://app.moqups.com/                |
 | Project Managemnet | https://project.tecky.io/              |



 ### SQL


 | Type     | Description                                            |
 | :------- | :----------------------------------------------------- |
 | Info     | https://db-engines.com/en/ranking                      |
 | Practice | https://sqlbolt.com/lesson/select_queries_introduction |


 ### More to Learn


 | Type                 | Description               |
 | :------------------- | :------------------------ |
 | Design Pattern       | https://refactoring.guru/ |
 | Semantic  Versioning | https://semver.org/       |
 
 ### Interesting


 | Description          | Description                 |
 | :------------------- | :-------------------------- |
 | Never SSL            | https://neverssl.com/       |
 | Semantic  Versioning | https://semver.org/         |
 | Have I been pwed     | https://haveibeenpwned.com/ |