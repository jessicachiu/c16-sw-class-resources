import { useEffect } from "react";
import { useDispatch } from "react-redux";
import useSWR from "swr";
import { fetcher, fetchProductsWithCategories } from "../fetcher";
import { Category, loadCategories, loadProducts } from "../redux/products/actions";

export function useProductsSWR() {
  const { data: categories, error } = useSWR<Category[]>(`${process.env.REACT_APP_BACKEND_URL}/categories`, fetcher)
  const dispatch = useDispatch();

  const { data: products, error: productError } = useSWR(categories?.map(c => c.id) ?? [], fetchProductsWithCategories)

  useEffect(() => {
    if (categories != null) {
      dispatch(loadCategories(categories))
    }
  }, [categories])

  // useEffect(() => {
  //   if (products != null) {
  //     dispatch(loadProducts(products))
  //   }
  // }, [products])
}