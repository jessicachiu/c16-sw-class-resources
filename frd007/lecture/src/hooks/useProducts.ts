import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { loadProducts, loadCategories, fetchCategories } from "../redux/products/actions";
import { RootState } from "../store";

export function useProducts() {
  const categories = useSelector((state: RootState) => Object.values(state.products.categories))

  const [isLoading, setIsLoading] = useState(true)
  const dispatch = useDispatch();

  useEffect(() => {
    async function load() {
      // const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/categories`)
      // const json = await res.json();

      // for (let category of json) {
      //   const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/categories/${category.id}/products`)
      //   const productJson = await res.json();
  
      //   dispatch(loadProducts(productJson, category.id));
      // }

      // dispatch(loadCategories(json));

      await dispatch(fetchCategories());
      setIsLoading(false);
    }

    if (Object.values(categories).length === 0) {
      load();
    } else {
      setIsLoading(false)
    }
  }, [dispatch, categories, setIsLoading])

  return isLoading;
}