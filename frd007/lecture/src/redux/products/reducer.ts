import produce from "immer";
import { Category, Product, ProductsActions } from "./actions"

export interface ProductsState {
  products: {
    [id: string]: Product
  };
  categories: {
    [id: string]: Category
  },
}

const initialState: ProductsState = {
  products: { },
  categories: { }
}

export function productsReducer(state: ProductsState = initialState, action: ProductsActions): ProductsState {
  return produce(state, state => {
    if (action.type === '@@products/LOAD_CATEGORIES') {
      for (let category of action.categories) {
        state.categories[category.id] = category
      }
    } else if (action.type === '@@products/LOAD_PRODUCTS') {
      for (let product of action.products) {
        state.products[product.id] = product
      }
      if (state.categories[action.categoryId] != null) {
        state.categories[action.categoryId].productIds = action.products.map(p => p.id)
      }
    }
  });
}