export interface Bid {
  productId: number;
  amount: number;
  finish: boolean;
  success: boolean;
}

export interface UserBidsState {
  bids: Bid[];
}

const initialState: UserBidsState = {
  bids: [
    {productId: 1, amount: 200, finish: true, success: false},
    {productId: 2, amount: 300, finish: false, success: false},
    {productId: 3, amount: 400, finish: true, success: true},
    {productId: 4, amount: 500, finish: false, success: true},
  ]
}

export function userBidsReducer(state: UserBidsState = initialState): UserBidsState {
  return initialState
}