export interface UserState {
  isAuthenticate: boolean;
  username: string | null;
}

const initialState: UserState = {
  isAuthenticate: true,
  username: 'alex'
}

export function userReducer(state: UserState = initialState): UserState {
  return initialState
}