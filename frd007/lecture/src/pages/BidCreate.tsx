import { useEffect } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { fetchCategories } from "../redux/products/actions";
import { RootState } from "../store";

type Inputs = {
  name: string,
  image: string,
  initialBid: string,
  minimumBid: string,
  eachBidAmount: string,
  deadline: string,
};

export function BidCreate() {
  const categories = useSelector((state: RootState) => Object.values(state.products.categories))

  const dispatch = useDispatch();
  
  useEffect(() => {
    dispatch(fetchCategories());
  }, [dispatch])

  const { register, handleSubmit, watch, formState: { errors } } = useForm<Inputs>();
  const onSubmit: SubmitHandler<Inputs> = data => {
    console.log(data);
    // ajax/fetch
  }

  return (
    <div>
      <h1>BidCreate</h1>
      
      <form onSubmit={handleSubmit(onSubmit)}>
        <p><label>Name: <input {...register('name')} /></label></p>
        <p><label>Image: <input {...register('image')} /></label></p>
        <p><label>Initial Bid: <input {...register('initialBid')} /></label></p>
        <p><label>底價: <input {...register('minimumBid')} /></label></p>
        <p><label>每口價: <input type="number" {...register('eachBidAmount')} /></label></p>
        <p><label>截止: <input type="datetime-local" {...register('deadline')} /></label></p>
        <p><label>分類: <select>
          {categories.map(category => (
            <option value={category.id} key={category.id}>{category.name}</option>
          ))}
          </select></label></p>
        <input type="submit" />
      </form>

    </div>
  )
}