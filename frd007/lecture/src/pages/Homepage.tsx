import { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { BidItem } from "../components/BidItem"
import { useProducts } from "../hooks/useProducts"
import { loadCategories, loadProducts } from "../redux/products/actions"
import { RootState } from "../store"

export function Homepage() {
  const categories = useSelector((state: RootState) => Object.values(state.products.categories))
  
  useProducts();

  categories.sort((a, b) => a.order - b.order)

  return (
    <div>
      <h1>Homepage</h1>
      
      {categories.map(category => (
        <div key={category.id}>
          <h2>{category.name}</h2>

          <div className="bid-list">
          {category.productIds.map(id => (
            <BidItem id={id} key={id} />
          ))}
          </div>
        </div>
      ))}
    </div>
  )
}