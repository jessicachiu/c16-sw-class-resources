import express from 'express'
import cors from 'cors'

const app = express()

app.use(cors({
  origin: ['http://localhost:3000']
}))

app.get('/categories/:id/products', (req, res) => {
  if (req.params.id === '1') {
    res.json([
      {
        id: 1,
        name: 'PS5',
        latestBid: 5,
        deadline: Date.now() + 120000,
        image: 'https://i.expansys.net/img/p/328407/playstation-5-gaming-console.jpg',
        minimumBid: 10,
        eachBidAmount: 10,
        username: 'Alex',
        categoryId: 1
      },
      {
        id: 2,
        name: 'Switch',
        latestBid: 5,
        deadline: Date.now() + 120000,
        image: 'https://i.expansys.net/img/p/328407/playstation-5-gaming-console.jpg',
        minimumBid: 10,
        eachBidAmount: 10,
        username: 'Alex',
        categoryId: 1
      }
    ]);
  } else {
    res.json([
      {
        id: 3,
        name: 'Lumia 1020',
        latestBid: 5000,
        deadline: Date.now() + 240000,
        image: 'https://i.expansys.net/img/p/328407/playstation-5-gaming-console.jpg',
        minimumBid: 15,
        eachBidAmount: 15,
        username: 'Alex',
        categoryId: 2
      },
      {
        id: 4,
        name: 'iPhone 13 Pro Max',
        latestBid: 3000,
        deadline: Date.now() + 120000,
        image: 'https://i.expansys.net/img/p/328407/playstation-5-gaming-console.jpg',
        minimumBid: 100,
        eachBidAmount: 100,
        username: 'Gordon',
        categoryId: 2
      },
    ])
  }
});

app.get('/categories', (req, res) => {
  res.json([
    {
      id: 1,
      name: 'Console',
      order: 99,
      productIds: [1, 2]
    },
    {
      id: 2,
      name: 'Cellphone',
      order: 1,
      productIds: [3, 4]
    }
  ])
});

app.get('/userBids', (req, res) => {
  res.json([
    {productId: 1, amount: 200, finish: true, success: false},
    {productId: 2, amount: 300, finish: false, success: false},
    {productId: 3, amount: 400, finish: true, success: true},
    {productId: 4, amount: 500, finish: false, success: true},
  ])
});

const port = process.env.PORT ?? 8080
app.listen(port, () => {
  console.log(`Listening on port ${port}`)
})