import fs from "fs";

console.log("Step 1");
const dijkstraQuote1 =
  "Computer science is no more about computers than astronomy is about telescopes.\n";
console.log("Step 2");
const dijkstraQuote2 = "Simplicity is prerequisite for reliability.\n";

console.log("Step 3");
// Flag w overwrites the original content and create the if it does not exist
fs.writeFile(
  "quotes-dijkstra.txt",
  dijkstraQuote1,
  { flag: "w" },
  function (err: Error) {
    console.log("Step 5");
    if (err) {
      console.log(err);
    }
    console.log("Step 6");
    // Flag a+ appends to the content and create the file if it does not exist
    fs.writeFile(
      "quotes-dijkstra.txt",
      dijkstraQuote2,
      { flag: "a+" },
      function (err: Error) {
        console.log("Step 7");
        if (err) {
          console.log(err);
        }
        console.log("Step 8");
      }
    );
  }
);
console.log("Step 4");
