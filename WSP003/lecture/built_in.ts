// import readline from "readline";
// import readlineSync from "readline-sync";

//////////////////  Async
// const readLineInterface = readline.createInterface({
//   input: process.stdin, // keyboard input
//   output: process.stdout, // output on terminal / screen
// });

// readLineInterface.question("What is your name?", (answer: string) => {
//   console.log(`Your name is ${answer}`);
//   readLineInterface.close();
// });

//////////////////  Sync
// let userName = readlineSync.question("May I have your name? ");
// console.log("Hi " + userName + "!");

// console.log("main content : ");
// console.log("main content");
// console.log("main content");

setImmediate(function () {
  console.log("setImmediate");
});

setTimeout(function () {
  console.log("setTimeout");
}, 0);

const intervalId = setInterval(function () {
  // setInterval Logic
  console.log("setInterval");
  clearInterval(intervalId);
}, 0);

import os from "os";

// Accessing all the logicals cpus in the computers
console.log(os.cpus());

// // Access the current home directory of the current user running the node
console.log(os.homedir());

// // Access the current machine's hostname
// os.hostname();

// // Access the free memory and the total memory respectively
// os.freemem();
// os.totalmem();

import path from "path";
const folder1 = "/folder1/";
const folder2 = "/folder2/";
const file1 = "/file1";

console.log(`${folder1}${folder2}${file1}`);

// Using path
console.log(path.join("folder1/", "/folder2/", "file1"));
