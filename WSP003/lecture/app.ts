import fs from "fs";

// Read File
let content = "";
const readFileCallBack = function (err: Error, data: Buffer) {
  if (err) {
    console.log(err);
    return;
  }
  console.log(data.toString("utf8"));
  content = data.toString("utf8");
  //   console.log(data.toString("utf8"));
};
fs.readFile("quotes.txt", readFileCallBack);

// empty string found
console.log("waiting"); // not work as you expect
console.log("waiting");
console.log("waiting");
console.log("waiting");
console.log(content);

// Write File

// Flag w overwrites the original content and create the if it does not exist
const dijkstraQuote1 =
  "1111111111111111111111\n1111111111111111111111\n1111111111111111111111\n1111111111111111111111\n1111111111111111111111\n1111111111111111111111\n1111111111111111111111\n1111111111111111111111\n1111111111111111111111\n1111111111111111111111\n1111111111111111111111\n1111111111111111111111\n1111111111111111111111\n1111111111111111111111\n1111111111111111111111\n1111111111111111111111\n1111111111111111111111\n1111111111111111111111\n1111111111111111111111\n1111111111111111111111\n1111111111111111111111\n1111111111111111111111\n1111111111111111111111\n1111111111111111111111\n";

const dijkstraQuote2 =
  "Simplicity is prerequisite for reliability.\nSimplicity is prerequisite for reliability.\nSimplicity is prerequisite for reliability.\nSimplicity is prerequisite for reliability.\n";

// Flag w overwrites the original content and create the if it does not exist
fs.writeFile(
  "quotes-dijkstra.txt",
  dijkstraQuote1,
  { flag: "w" },
  function (err: Error) {
    if (err) {
      console.log(err);
    }

    // Flag a+ appends to the content and create the file if it does not exist
    fs.writeFile(
      "quotes-dijkstra.txt",
      dijkstraQuote2,
      { flag: "a+" },
      function (err: Error) {
        if (err) {
          console.log(err);
        }
      }
    );
  }
);
