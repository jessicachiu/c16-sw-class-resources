import fs from "fs";
import path from "path";

function listAllJs(dir: string) {
  const fullFolderPath = path.resolve(dir);
  fs.readdir(fullFolderPath, function (err, files) {
    if (err) {
      console.log(err);
      return;
    }
    for (let file of files) {
      const fullFilePath = path.resolve(path.join(fullFolderPath, file));

      fs.stat(fullFilePath, function (err, stats) {
        if (err) {
          console.log(err);
          return;
        }
        if (path.extname(file) === ".js" && stats.isFile()) {
          console.log(fullFilePath);
        }
      });
    }
  });
}
// fs.readdir and fs.stat
// listAllJs(
//   "C:\\Users\\Dickson\\Documents\\Tecky\\hk-map-16-jul-21\\tecky-exercises-solutions-sw\\WSP003\\ex"
// );
listAllJs("./demoFolder");
