import fs from "fs";
import path from "path";

function listAllJsRecursive(dir: string, 收工: (fileList: string[]) => void) {
    const fullFolderPath = path.resolve(dir);
    fs.readdir(fullFolderPath, function (err, files) {
        if (err) {
            console.log(err);
            return;
        }
        let resultList: string[] = [];
        for (let file of files) {
            const fullFilePath = path.resolve(path.join(fullFolderPath, file));
            if (path.extname(file) === ".js") {
                resultList.push(fullFilePath);
            }
        }
        收工(resultList);
    });
}
// fs.readdir and fs.stat

listAllJsRecursive("./demoFolder", (fileList: string[]) => {
    console.log(fileList.length);
});
