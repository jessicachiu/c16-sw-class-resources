import fs from "fs";
import path from "path";

function listAllJsRecursive(dir: string, 收工: (fileList: string[]) => void) {
    const fullFolderPath = path.resolve(dir);
    fs.readdir(fullFolderPath, function (err, files) {
        if (err) {
            console.log(err);
            return;
        }
        let resultList: string[] = [];
        let targetFileCount = files.length;
        for (let file of files) {
            const fullFilePath = path.resolve(path.join(fullFolderPath, file));

            fs.stat(fullFilePath, function (err, stats) {
                targetFileCount--;
                if (err) {
                    console.log(err);
                    return;
                }
                if (path.extname(file) === ".js" && stats.isFile()) {
                    resultList.push(fullFilePath);
                }
                if (targetFileCount == 0) {
                    收工(resultList);
                }
            });
        }
        if (targetFileCount == 0) {
            收工(resultList);
        }
    });
}
// fs.readdir and fs.stat

listAllJsRecursive("./demoFolder", (fileList: string[]) => {
    console.log(fileList.length);
});
