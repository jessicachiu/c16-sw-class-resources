import fs from "fs";

const fileCount = 100;
for (let i = 0; i < fileCount; i++) {
    fs.writeFile(`${i}.js`, `//this is file ${i}`, (error) => {
        if (error) {
            console.log(error);
            return;
        }
        if (i % 10 == 0) {
            console.log(`整緊${i}`);
        }
    });
}
