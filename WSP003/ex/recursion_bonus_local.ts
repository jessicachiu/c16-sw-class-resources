import fs from "fs";
import path from "path";

// let results: string[] = [];
function listAllJsRecursive(
    dir: string,
    收工: (resultFiles: string[]) => void
) {
    const fullFolderPath = path.resolve(dir);
    fs.readdir(fullFolderPath, function (err, files) {
        if (err) {
            console.log(err);
            return;
        }
        let resultFiles: string[] = [];
        let filesToCheckCount = files.length;
        for (let file of files) {
            const fullFilePath = path.resolve(path.join(fullFolderPath, file));
            fs.stat(fullFilePath, function (err, stats) {
                filesToCheckCount--;
                if (err) {
                    console.log(err);
                    return;
                }
                if (path.extname(file) === ".js" && stats.isFile()) {
                    resultFiles.push(fullFilePath);
                } else if (stats.isDirectory()) {
                    filesToCheckCount++;
                    listAllJsRecursive(fullFilePath, (files) => {
                        filesToCheckCount--;
                        for (let file of files) {
                            resultFiles.push(file);
                        }
                        if (filesToCheckCount == 0) {
                            收工(resultFiles);
                        }
                    });
                }
                if (filesToCheckCount == 0) {
                    收工(resultFiles);
                }
            });
        }
        if (filesToCheckCount == 0) {
            收工(resultFiles);
        }
    });
}
listAllJsRecursive("./", (resultFiles: string[]) => {
    console.log("收工lu ./");
    console.log("resultFiles = ", resultFiles.length);
});
listAllJsRecursive("./demoFolder", (resultFiles: string[]) => {
    console.log("收工lu demoFolder");
    console.log("resultFiles = ", resultFiles.length);
});
// setTimeout(() => {
//   console.log("results = ", results.length);
// }, 10);
