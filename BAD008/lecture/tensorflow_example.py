# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %%
# Tensorflow
import tensorflow as tf 
print(f'tf version is {tf.__version__}')


# %%
import matplotlib.pyplot as plt

# Eager Execution
tf.executing_eagerly()


# %%
train_dataset_url = "https://storage.googleapis.com/download.tensorflow.org/data/iris_training.csv"
train_dataset_fp = tf.keras.utils.get_file(fname='iris_training.csv',origin=train_dataset_url)
print(train_dataset_fp)


# %%
test_dataset_url = "https://storage.googleapis.com/download.tensorflow.org/data/iris_test.csv"
test_dataset_fp = tf.keras.utils.get_file(fname='iris_test.csv',origin=test_dataset_url)
print(test_dataset_fp)


# %%
feature_names = ['sepal_length','sepal_width',
        'petal_length','petal_width']

label_name = 'species'
column_names = feature_names + [label_name]


# %%
batch_size = 32
raw_train_dataset = tf.data.experimental.make_csv_dataset(
    train_dataset_fp,
    batch_size,
    column_names= column_names,
    label_name= label_name,
    num_epochs=1
)

raw_test_dataset = tf.data.experimental.make_csv_dataset(
    test_dataset_fp,
    batch_size,
    column_names= column_names,
    label_name= label_name,
    num_epochs=1
)


# %%
next_batch_dataset = next(iter(raw_train_dataset))
features, labels = next_batch_dataset

plt.scatter(features['sepal_width'],
    features['petal_width'],
    c= labels
)

plt.xlabel('Sepal Width')
plt.ylabel('Petal Width')
plt.show()


# %%
def  pack_features_vector(features, labels):
    """Pack the features into a single array."""
    features = tf.stack(list(features.values()), axis=1)
    return features, labels


# %%
# train_dataset 同test_dataset會有後用
train_dataset = raw_train_dataset.map(pack_features_vector)
test_dataset = raw_test_dataset.map(pack_features_vector)

next_batch_dataset = next(iter(train_dataset))

next_batch_dataset


# %%
# Initialize 個model 

model = tf.keras.Sequential([
    tf.keras.layers.Dense(10,activation=tf.nn.relu,input_shape=(4,)), # 係 4,batch_size
    tf.keras.layers.Dense(10,activation=tf.nn.relu) ,
    tf.keras.layers.Dense(3)
])

model.summary()


# %%
features, labels = next_batch_dataset
# output = model(input)
predictions = model(features)
print(predictions)
tf.nn.softmax(predictions[:5])
# 佢現家答出嘅嘢係垃圾，因為未train

print("Prediction: {}".format(tf.argmax(predictions, axis=1)))


# %%
loss_object = tf.keras.losses.SparseCategoricalCrossentropy(
        from_logits=True)


# %%
def loss(model,x,y, training=True):
    y_ = model(x,training = training)
    # y_true係理想
    # y_pred 係現實
    return loss_object(y_true=y, y_pred=y_)

l = loss(model,features,labels,training= False)
print(l)


# %%
def grad(model, inputs, targets):
    with tf.GradientTape() as tape:
        loss_value = loss(model, inputs, targets, training=True)
    return loss_value, tape.gradient(loss_value, model.trainable_variables)

# learning_rate 要係呢度set，而你一開唔知係幾多嘅
optimizer = tf.keras.optimizers.SGD(learning_rate= 0.01)


# %%

# Single Optimization Step
loss_value, grads = grad(model,features,labels)
print(loss_value)
print(grads) # 對應每一粒 trainable variable(weight/bias)應該點改
optimizer.apply_gradients(zip(grads, model.trainable_variables))


print(model.trainable_variables)


# %%
# 呢個parameter 又係要試
num_epochs = 201

# 同理想差幾遠平均嘅list
train_loss_results = []
# percentage有幾多個中咗理想嘅label
train_accuracy_results = []

for epoch in range(num_epochs):
    epoch_loss_avg = tf.keras.metrics.Mean()
    epoch_accuracy = tf.keras.metrics.SparseCategoricalAccuracy()

    for x,y in train_dataset:             
        loss_value, grads = grad(model,x,y)
        optimizer.apply_gradients(zip(grads,
            model.trainable_variables))
        epoch_loss_avg.update_state(loss_value)
        # y 係理想，後面係model 估嘅data
        epoch_accuracy.update_state(y, model(x, training=True))
    train_loss_results.append(epoch_loss_avg.result())
    train_accuracy_results.append(epoch_accuracy.result())
    if epoch % 10 == 0:
        print(f'Epoch {epoch} loss:{epoch_loss_avg.result()} accuracy:{epoch_accuracy.result()}')


# %%
features, labels = next_batch_dataset
# output = model(input)
predictions = model(features)
tf.nn.softmax(predictions[:5])
# 佢現家答出嘅嘢係垃圾，因為未train

print("Prediction: {}".format(tf.argmax(predictions, axis=1)))


# %%
fig, axes = plt.subplots(2, sharex=True, figsize=(12, 8))
fig.suptitle('Training Metrics')

axes[0].set_ylabel("Loss", fontsize=14)
axes[0].plot(train_loss_results)

axes[1].set_ylabel("Accuracy", fontsize=14)
axes[1].set_xlabel("Epoch", fontsize=14)
axes[1].plot(train_accuracy_results)
plt.show()


# %%
model.trainable_variables


# %%
test_accuracy = tf.keras.metrics.Accuracy()

for x,y in test_dataset:
    logits = model(x,training=False)
    prediction = tf.argmax(logits, axis=1, output_type=tf.int32)
    test_accuracy(prediction, y)

print(f'Test Accuracy: {test_accuracy.result()}')


# %%
model.save('./',overwrite=True)


