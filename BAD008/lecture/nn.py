# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %% [markdown]
# # Neural Network

# %%
import numpy as np
# Derivative calculated already 
f = lambda x: 1.0/(1.0 + np.exp(-x))

# input 3 x1
x = np.array([0.01,0.02,0.03])
# 12 parameters Layer 1 weights
w1 = np.array([[1,2,3],[4,5,6,],[7,8,9],[1,2,3]])
b1 = np.array([0.02,0.04,0.03,0.02])

# Linear combination
out1 = f(np.dot(w1,x) + b1) # layer 1 
# activation function: Sigmoid

w2 = np.array([[1,2,3,4],[4,5,6,7],[7,8,9,10],[1,2,3,4]])
b2 = np.array([0.012,0.014,0.013,0.012])

out2 = f(np.dot(w2,out1) + b2) # layer 2


w3 = np.array([1,2,3,4])
b3 = np.array([3])
out3 = f(np.dot(w3,out2) + b3) # output layer

print(out3)


# %%
def cost_func(theta, X, y):
    m =  len(y)
    prediction = X.dot(theta)
    cost = (1/2*m) * np.sum(np.square(prediction-y))
    return cost

def gradient_descent(X, y, theta, learning_rate=0.01, iterations=2000):
    '''
    X: parameters of hypothesis function
    y: labelled result
    theta: trainable parameters, eg. theta_0, theta_1
    learning_rate user-defined learning rate
    iterations iterations for the training
    '''
    m =  len(X)
    costs = np.zeros(iterations)
    for it in range(iterations):
        prediction = np.dot(X, theta)
        theta = theta - (1  / m) * learning_rate * (X.T.dot((prediction-y)))
        costs[it] = cost_func(theta, X, y)
    return theta, costs

X = np.array([0.01,0.02,0.03])
y = np.array([0.5,0.6,0.2])
theta = np.array([0.1,0.2,0.3])
theta,costs = gradient_descent(X,y,theta,learning_rate=0.3)
print(costs)
print(theta)


# %%



# %%



