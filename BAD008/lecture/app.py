from sanic import Sanic
import tensorflow as tf
from sanic.response import json
import numpy as np

app = Sanic("My Hello, world app")

model = tf.saved_model.load('./')

class_names = ['Iris setosa', 'Iris versicolor', 'Iris virginica']


@app.post('/')
async def hello(request):
    input_json = request.json
    features = tf.convert_to_tensor(np.array(input_json),dtype=tf.float32)

    predictions =  model(features,training=False)

    results = []
    for prediction in predictions:
        probs = tf.nn.softmax(prediction)
        most_probable = tf.argmax(probs)
        index = most_probable.numpy() # Convert from tensor to numpy 之後先做到其他嘢
        name = class_names[index]
        results.append({
            'species': name,
            'probability': float(probs.numpy()[index])
        })
        print("Predicted class: {} with a probability: {:4.1f}%".format(
            name, 100*probs.numpy()[index]))
    return json(results)

if __name__ == '__main__':
    app.run()