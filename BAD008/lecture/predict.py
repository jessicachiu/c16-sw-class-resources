
import tensorflow as tf
import numpy as np 
model = tf.saved_model.load('./')

print(model)
class_names = ['Iris setosa', 'Iris versicolor', 'Iris virginica']

# 將一個numpy array  變成tensor
features = tf.convert_to_tensor(np.array([
    [1.0,3.0,2.0,1.0],
    [1.0,3.0,1.1,3.0],
]),dtype=tf.float32)

predictions =  model(features,training=False)

for prediction in predictions:
    probs = tf.nn.softmax(prediction)
    most_probable = tf.argmax(probs)
    index = most_probable.numpy() # Convert from tensor to numpy 之後先做到其他嘢
    name = class_names[index]
    print("Predicted class: {} with a probability: {:4.1f}%".format(
            name, 100*probs.numpy()[index]))