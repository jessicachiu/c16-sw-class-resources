console.log("Hello this is ts");

let color: string | number = "";
color = "blue";
console.log(color);
color = 255;

let numArray: number[] = [1, 2, 3, 4];

function getCellVal() {
  if (Math.random() > 0.5) {
    return "You got me";
  } else {
    return null;
  }
}

let value = getCellVal();
if (value) {
  value.toString();
}

// function
function add(inputA: number, inputB: number): number | null {
  let sum = inputA + inputB;
  if (sum > 10) {
    return sum;
  } else {
    return null;
  }
}

// let timer: NodeJS.Timeout = setTimeout(
//   (input1) => {
//     console.log("arg  =", input1);
//   },
//   0,
//   "A",
//   "B",
//   "C"
// );

type ClassStudentName = "Peter" | "Bob" | "Apple";

let myClassmate: ClassStudentName = "Apple";

// Object

type Student = {
  name: string;
  age: number;
  dateOfBirth: Date;
  gender?: string;
};

let student1: Student = {
  name: "Peter",
  age: 30,
  dateOfBirth: new Date("1999-11-11"),
  gender: "M",
};

let student2: Student = {
  name: "Betty",
  age: 30,
  dateOfBirth: new Date("1999-11-11"),
};

// Enum

enum Direction {
  East = 1,
  South = 2,
  West = 3,
  North = 4,
}

console.log(Direction.East);
console.log(Direction[1]);

function turnTo(direction: Direction) {
  if (direction == Direction.East) {
    console.log("This is the direction East!");
  }
  return direction != Direction.East;
}

turnTo(Direction.East);
console.log("This is the direction DEMO!");

// Generics

function sum(nums: number[]): number {
  //<--- Type error here
  let total = 0;
  for (let num of nums) {
    total += num;
  }
  return total;
}

let pattern: any[][];

// sum(["12","47983","12"]);  //<--- type error

sum([1, 2, 3, 4, 5]); // Correct

const axiosOld = require("axios");
import axios from "axios";
