
import pytest
from chispa.dataframe_comparer import *
from pyspark.sql import SparkSession
from pyspark.sql.types import StructType,StructField,LongType,StringType
from spark_pagecreate import aggregate_for_q2,aggregate_for_q3

# 可以幫你將spark_pagecreate 入面嘅spark mock 咗佢
@pytest.fixture(scope='session')
def spark():
    return SparkSession.builder \
      .master("local") \
      .appName("spark-test") \
      .getOrCreate()

pytestmark = pytest.mark.usefixtures("spark")


def test_aggregate_for_q2(spark):
    # 1. 準備功夫
    data = [
        (False,"en.wikipedia.org"),
        (True,"en.wikipedia.org"),
        (False,"zh.wikipedia.org"),
        (True,"zh.wikipedia.org"),
    ]

    df = spark.createDataFrame(data,["user_is_bot",'domain'])

    # 2. 真係test 果步
    q2_df = aggregate_for_q2(df)

    # 3. Verify下啱唔啱
    expected_data = [
        (False,"zh.wikipedia.org")
    ]
    expected_df = spark.createDataFrame(expected_data,['user_is_bot','domain'])
    assert_df_equality(q2_df,expected_df)


def test_aggregate_for_q3(spark):
    # 1. 準備功夫
    data = [
        (False,"peter"),
        (False,"peter"),
        (True,"peter"),
        (False,"john"),
        (True,"Susan"),
    ]

    df = spark.createDataFrame(data,["user_is_bot",'user_text'])

    # 2. 真係test 果步
    q3_df = aggregate_for_q3(df)

    # 3. Verify下啱唔啱
    q3_data = q3_df.rdd.map(tuple).collect() #將一個spark dataframe 變返做 List of Tuples
    expected_data = [
        ("peter",2)
    ]
    assert(expected_data == q3_data)