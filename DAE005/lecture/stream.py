import time
from sseclient import SSEClient as EventSource
import json

ROWS_PER_FILE = 50
url = 'https://stream.wikimedia.org/v2/stream/page-create';

def write_to_file(data):
    now = int(time.time())
    with open('/tmp/pagecreate-{}.txt'.format(now),'w') as f:
        for row in data:
            # 用JSON format 寫落file
            f.write(json.dumps(row)+"\n")
rows = []
for event in EventSource(url):
    if event.event == "message":
        try:
            event_data = json.loads(event.data)
            data = {
               "user_text": event_data['performer']['user_text'],
                "user_is_bot": event_data['performer']['user_is_bot'],
                "user_registration_dt": event_data['performer'].get('user_registration_dt',''),
                "user_edit_count": event_data['performer'].get('user_edit_count',''),
                "domain": event_data['meta']['domain'],
                "uri": event_data['meta']['uri'],
                "database": event_data['database'],
                "page_id": event_data['page_id'],
                "page_title": event_data['page_title'],
                "rev_timestamp": event_data['rev_timestamp'],
                "rev_minor_edit": event_data['rev_minor_edit'],
                "rev_len": event_data['rev_len'],
                "rev_content_model": event_data['rev_content_model'],
                "rev_content_format":event_data['rev_content_format'],
                "comment": event_data['comment'].replace("\n"," "),
                "parsedcomment": event_data['parsedcomment'].replace("\n"," ")
            }
            rows.append(data)
            if len(rows) >= ROWS_PER_FILE:
                write_to_file(rows)
                rows = []
        except ValueError as e:
            print(e)
        else:
            print('{page_title} created by {user}'.format(**event_data,user=event_data['performer']['user_text']))