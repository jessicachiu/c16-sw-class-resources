#!/usr/bin/env python


def prepare_env():
    global spark
    from pyspark.sql import SparkSession
    import findspark
    findspark.init('/home/gordon/Codes/spark')
    packages = [
        "com.amazonaws:aws-java-sdk:1.12.79",
        "org.apache.hadoop:hadoop-aws:3.2.0",
        "org.apache.spark:spark-avro_2.12:2.4.4",
        "org.mongodb.spark:mongo-spark-connector_2.12:3.0.1"
    ]
    spark = SparkSession.builder.appName("Read from mongo")\
        .config("spark.jars.packages",",".join(packages)).getOrCreate()
    return spark
            

def read_dataframes():
    return spark.read.format('mongo').option('spark.mongodb.input.uri',
        'mongodb://127.0.0.1/wikipedia.pagecreate').load()

def registerTable(df):
    df.registerTempTable('pagecreate');


def aggregate_for_q2(df):
    return df.where(df.user_is_bot == False ).where(df.domain == "zh.wikipedia.org")

def aggregate_for_q3(df):
    from pyspark.sql.functions import col
    q3_df = df.where(df.user_is_bot == False).groupBy('user_text').count()
    return q3_df.sort(col('count').desc()).limit(1)

def aggregate_for_q4(df):
    q4_df = df.where(df.user_is_bot == False).groupBy('user_text').sum("rev_len")
    q4_df = q4_df.sort(col('sum(rev_len)').desc()).limit(1)
    return q4_df.withColumnRenamed('sum(rev_len)','sum_rev_len')

def write_to_parquet_s3(df,path):
    df.write.format('parquet').save('s3a://dae-c16/{}'.format(path),mode="overwrite")

def main():
    # 1. prepare environment
    prepare_env()

    # 2. Extract
    df = read_dataframes()

    # 3. Transform
    # process data -> filter null
    registerTable(df)

    # 4. Aggregate
    q2_df = aggregate_for_q2(df)
    q3_df = aggregate_for_q3(df)
    q4_df = aggregate_for_q4(df)

    write_to_parquet_s3(q2_df,"q2.parquet")
    write_to_parquet_s3(q3_df,"q3.parquet")
    write_to_parquet_s3(q4_df,"q4.parquet")


if __name__ == "__main__":
    main()



# q5_df = df.groupBy('domain').count().sort(col('count').desc()).limit(3)
# q5_df = spark.createDataFrame(q5_df.collect()[0::2])




# df.registerTempTable('pagecreate');

# q6_df = spark.sql("SELECT * from pagecreate WHERE comment like '%edit%' ")
# q6_df.select('comment').show()




# q5_df.write.format('avro').save('s3a://dae-c16/q5.avro',mode="overwrite")
# q6_df.write.format('avro').save('s3a://dae-c16/q6.avro',mode="overwrite")

