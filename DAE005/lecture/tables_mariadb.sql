CREATE TABLE users(
    id SERIAL PRIMARY KEY,
    user_text text,
    user_is_bot boolean,
    user_registration_dt TIMESTAMP,
    user_edit_count integer,
    created_at  TIMESTAMP DEFAULT NOW()
);

CREATE UNIQUE INDEX users_unique_idx on users (user_text);


CREATE TABLE domains(
    id SERIAL PRIMARY KEY,
    domain text,
    database_text text,
    created_at TIMESTAMP DEFAULT NOW()
);

CREATE UNIQUE INDEX domains_unique_idx on domains(domain,database_text);


CREATE TABLE rev_content_models(
    id SERIAL PRIMARY KEY,
    rev_content_model text,
    rev_content_format text,
    created_at TIMESTAMP DEFAULT NOW()
);

CREATE UNIQUE INDEX rev_content_models_unique_idx on rev_content_models(rev_content_model,rev_content_format);


CREATE TABLE rev_timestamps(
    id SERIAL PRIMARY KEY,
    rev_timestamp timestamp,
    rev_year integer,
    rev_month integer,
    rev_day integer,
    rev_hour integer,
    created_at TIMESTAMP DEFAULT NOW()
);

CREATE UNIQUE INDEX rev_timestamps_unique_idx on 
rev_timestamps(rev_year,rev_month,rev_day,rev_hour);

CREATE TABLE pages(
    id SERIAL PRIMARY KEY,
    page_id  integer,
    page_title text,
    url        text,
    comment    text,
    parsedcomment text,
    rev_len      integer,
    user_id  bigint(20) unsigned,
    foreign key (user_id) references users(id),
    domain_id  bigint(20) unsigned ,
    foreign key (domain_id) references domains(id),
    rev_timestamp_id  bigint(20) unsigned ,
    foreign key (rev_timestamp_id) references rev_timestamps(id),
    rev_content_model_id  bigint(20) unsigned,
    foreign key (rev_content_model_id) references rev_content_models(id),
    created_at TIMESTAMP DEFAULT NOW()
);

CREATE UNIQUE INDEX pages_unique_idx on pages (page_id);