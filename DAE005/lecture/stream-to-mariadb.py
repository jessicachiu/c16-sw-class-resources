#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pip

# 懶人function: 用嚟裝嘢
def import_or_install(package):
    try:
        __import__(package)
    except ImportError:
        pip.main(['install','--user', package])

import_or_install('findspark')
import_or_install('python-dotenv')
import_or_install('psycopg2-binary')


# In[2]:


import findspark
findspark.init('/home/gordon/Codes/spark')


# In[3]:


from pyspark.sql import SparkSession

# 呢個可以取代  spark-defaults.conf 果個嘅configuration
packages = [
    "com.amazonaws:aws-java-sdk:1.12.79",
    "org.apache.hadoop:hadoop-aws:3.2.0",
    "org.apache.spark:spark-avro_2.12:2.4.4",
    "org.mongodb.spark:mongo-spark-connector_2.12:3.0.1"
]



spark = SparkSession.builder.appName("Read from mongo")\
        .config("spark.jars.packages",",".join(packages))\
        .getOrCreate()


# In[4]:


from pyspark.sql.types import StructType,StructField, StringType, BooleanType, LongType

pagecreate_schema = StructType([
    StructField('user_text',StringType(),True),
    StructField('user_is_bot',BooleanType(),True),
    StructField('user_registration_dt',StringType(),True),
    StructField('user_edit_count',LongType(),True),
    StructField('domain',StringType(),True),
    StructField('uri',StringType(),True),
    StructField('database',StringType(),True),
    StructField('page_id',LongType(),True),
    StructField('page_title',StringType(),True),
    StructField('rev_timestamp',StringType(),True),
    StructField('rev_minor_edit',BooleanType(),True),
    StructField('rev_len',LongType(),True),
    StructField('rev_content_model',StringType(),True),
    StructField('rev_content_format',StringType(),True),
    StructField('comment',StringType(),True),
    StructField('parsedcomment',StringType(),True),
]);

df_stream  = spark.readStream.schema(pagecreate_schema)\
        .option('cleanSource','delete')\
        .json('/tmp/pagecreate-*.txt');


# In[6]:


from dotenv import load_dotenv
import os 
load_dotenv()
DB_NAME=os.getenv('DB_NAME')
DB_USERNAME=os.getenv('DB_USERNAME')
DB_PASSWORD=os.getenv('DB_PASSWORD')
DB_HOST = os.getenv('DB_HOST') or 'localhost'# 如果無DB_HOST，就load localhost


# In[7]:
import mariadb
conn = mariadb.connect(
        user=DB_USERNAME,
        password=DB_PASSWORD,
        host=DB_HOST,
        port=3306,
        database="pagecreate"
    )


# In[8]:

import mariadb
from pyspark.sql.functions import col,when,substring
def insert_data_batch(df,id):
    import time
    # 每個 Stream 嘅minibatch，呢個function 入面，同之前做嘅嘢係一樣
    df = df.withColumn('user_edit_count', col('user_edit_count').cast('int'))
    df = df.withColumn('user_registration_dt',when(col('user_registration_dt') != '' ,col('user_registration_dt')).otherwise(None))
    df = df.withColumn('user_registration_dt',substring('user_registration_dt',1,19))
    df = df.withColumn('year',substring('rev_timestamp',1,4))        .withColumn('month',substring('rev_timestamp',6,2))        .withColumn('day',substring('rev_timestamp',9,2))        .withColumn('hour',substring('rev_timestamp',12,2))
    df.rdd.foreach(insert_data)
    now = int(time.time())
    df.write.parquet('s3a://dae-c16/pagecreate-{}.parquet'.format(now))
    return (True)

# Function 外
def insert_data(row):    
    # Function內 RDD -> 可以係其他機度行，所以你必須係入面先至connect
    conn = mariadb.connect(
        user=DB_USERNAME,
        password=DB_PASSWORD,
        host='dw-tecky-hk.ci9gmtwieyv5.ap-southeast-1.rds.amazonaws.com',
        port=3306,
        database="pagecreate"
    )
    conn.autocommit = True
    # Open a cursor to perform database operations
    cur = conn.cursor()
    
    row_dict = row.asDict()
    # 先insert dimension
    # users
    insert_user_sql = "insert into users (user_text,user_is_bot,user_registration_dt,user_edit_count) values(%(user_text)s,%(user_is_bot)s,%(user_registration_dt)s,%(user_edit_count)s) on duplicate key update id = id"
    cur.execute(insert_user_sql,row_dict)

    # domains
    insert_domain_sql = "insert into domains (domain,database_text) values(%(domain)s,%(database)s) on duplicate key update id = id"
    cur.execute(insert_domain_sql,row_dict)

    # rev_content_model
    insert_rev_content_model_sql = "insert into rev_content_models (rev_content_model,rev_content_format) values(%(rev_content_model)s,%(rev_content_format)s)       on duplicate key update id = id"
    cur.execute(insert_rev_content_model_sql,row_dict)

    # rev_timestamp
    insert_rev_timestamp_sql = "insert into rev_timestamps (rev_timestamp,rev_year,rev_month,rev_day,rev_hour) values(%(rev_timestamp)s,%(year)s,%(month)s, %(day)s,%(hour)s) on duplicate key update id = id"
    cur.execute(insert_rev_timestamp_sql,row_dict)

    # 再insert fact
    # 我地用subquery ，係因為唔想再重新select 啲foreign key 出嚟
    insert_page_sql = "insert into pages (page_id,page_title,url,comment,parsedcomment,rev_len,        user_id, domain_id,rev_content_model_id,rev_timestamp_id)        values(%(page_id)s,%(page_title)s, %(uri)s, %(comment)s, %(parsedcomment)s,%(rev_len)s,            (select id from users where user_text = %(user_text)s limit 1),            (select id from domains where domain = %(domain)s and database = %(database)s limit 1),            (select id from rev_content_models where rev_content_model = %(rev_content_model)s                and rev_content_format = %(rev_content_format)s  limit 1),            (select id from rev_timestamps where rev_year = %(year)s                and rev_month = %(month)s and rev_day = %(day)s and rev_hour = %(hour)s  limit 1)  ) on duplicate key update comment = %(comment)s"
    cur.execute(insert_page_sql,row_dict)
    return (True)       

query = df_stream.writeStream.foreachBatch(insert_data_batch).start()
query.awaitTermination() 


# In[ ]:




