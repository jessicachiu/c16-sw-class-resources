import React, { useEffect, useMemo, useState } from 'react';
import logo from './logo.svg';
import './App.css';

function timeFormatter(count: number) {
  return `${Math.floor(count/60000)}:${('0' + Math.floor(count/1000) % 60).substr(-2)}:${('00' + count % 1000).substr(-3)}`
}

function App() {
  const [count, setCount] = useState(0)
  const [timerId, setTimerId] = useState(0)
  const [laps, setLaps] = useState<number[]>([])

  useEffect(() => {
    return () => {
      clearInterval(timerId)
    }
  }, [timerId]);

  const slowestIndex = useMemo(() => {
    if (laps.length <= 2) {
      return null;
    }
    let current = 0;
    for (let i = 1; i < laps.length; i++) {
      if ((laps[i] - laps[i - 1]) > (laps[current] - (laps[current - 1] ?? 0))) {
        current = i;
      }
    }
    return current;
  }, [laps])

  const fastestIndex = useMemo(() => {
    if (laps.length <= 2) {
      return null;
    }
    let current = 0;
    for (let i = 1; i < laps.length; i++) {
      if ((laps[i] - laps[i - 1]) < (laps[current] - (laps[current - 1] ?? 0))) {
        current = i;
      }
    }
    return current;
  }, [laps])
 
  return (
    <div className="App">
      <div className="mono">
        {timeFormatter(count)}
      </div>
      <div>
      <button onClick={() => {
        if (timerId == 0) {
          setLaps([])
          setCount(0)
        } else {
          setLaps(l => [...l, count])
        }
      }}>{timerId == 0 ? 'Reset' : 'Lap'}</button>
      <button onClick={() => {
        if (timerId == 0) {
          setTimerId(window.setInterval(() => {
            setCount(c => c + 16)
          }, 16))
        } else {
          setTimerId(0)
        }
      }}>{timerId == 0 ? 'Start' : 'Stop'}</button>
      </div>
      <div>
        {laps.map((time, index) => (
          <div key={index} className={slowestIndex === index ? "slowest" : (fastestIndex === index ? "fastest" : "")}>
            <span>#{index + 1}. </span>
            <span className="mono">{timeFormatter(time - (laps[index - 1] ?? 0))}</span>
          </div>
        ))}
      </div>

    </div>
  );
}

export default App;
