import React, { useEffect, useMemo, useState } from 'react';
import logo from './logo.svg';
import './App.css';

function timeFormatter(count: number) {
  return `${Math.floor(count/60000)}:${('0' + Math.floor(count/1000) % 60).substr(-2)}:${('00' + count % 1000).substr(-3)}`
}

interface State {
  date: Date;
  accumulate: number;
  count: number;
  timerId: number;
  laps: number[]
}

class App extends React.Component<{}, State> {
  state: State = {
    date: new Date(),
    accumulate: 0,
    count: 0,
    timerId: 0,
    laps: []
  }

  protected slowestIndex: number | null = null;
  protected fastestIndex: number | null = null;

  public getSnapshotBeforeUpdate(prevProps: {}, prevState: State) {
    if (prevState.laps !== this.state.laps) {
      this.slowestIndex = this.calculateSlowestIndex();
      this.fastestIndex = this.calculateFastestIndex();
    }
    return null;
  }

  protected calculateSlowestIndex = () => {
    const { laps } = this.state
    if (laps.length <= 2) {
      return null;
    }
    let current = 0;
    for (let i = 1; i < laps.length; i++) {
      if ((laps[i] - laps[i - 1]) > (laps[current] - (laps[current - 1] ?? 0))) {
        current = i;
      }
    }
    return current;
  }

  protected calculateFastestIndex = () => {
    const { laps } = this.state
    if (laps.length <= 2) {
      return null;
    }
    let current = 0;
    for (let i = 1; i < laps.length; i++) {
      if ((laps[i] - laps[i - 1]) < (laps[current] - (laps[current - 1] ?? 0))) {
        current = i;
      }
    }
    return current;
  }
 
  public render() {
    const { date, accumulate, count, timerId, laps } = this.state
    
    return (
      <div className="App">
        <div className="mono">
          {timeFormatter(accumulate + count)}
        </div>
        <div>
        <button onClick={() => {
          if (timerId == 0) {
            this.setState({laps:[]})
            this.setState({count:0})
          } else {
            this.setState(state => ({laps: [...state.laps, accumulate + count]}))
          }
        }}>{timerId === 0 ? 'Reset' : 'Lap'}</button>
        <button onClick={() => {
          if (timerId === 0) {
            this.setState({date:new Date()})
            this.setState({timerId:window.setInterval(() => {
              this.setState(state => {
                return {count:(new Date()).getTime() - state.date.getTime()}
              })
            }, 16)})
          } else {
            clearInterval(timerId)
            this.setState(state => ({accumulate: state.accumulate + (new Date()).getTime() - date.getTime()}))
            this.setState({count:0})
            this.setState({timerId:0})
          }
        }}>{timerId === 0 ? 'Start' : 'Stop'}</button>
        </div>
        <div>
          {laps.map((time, index) => (
            <div key={index} className={this.slowestIndex === index ? "slowest" : (this.fastestIndex === index ? "fastest" : "")}>
              <span>#{index + 1}. </span>
              <span className="mono">{timeFormatter(time - (laps[index - 1] ?? 0))}</span>
            </div>
          ))}
        </div>
  
      </div>
    );
  }
}

export default App;
