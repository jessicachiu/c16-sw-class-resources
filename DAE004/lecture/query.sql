


SELECT rev_hour,count(*) as occurrence from pages 
        inner join rev_timestamps on pages.rev_timestamp_id = rev_timestamps.id
        group by rev_hour;


SELECT user_is_bot,count(*) as occurrence from pages
        inner join users on pages.user_id = users.id
        group by user_is_bot;


SELECT domain,count(*) as occurrence from pages
        inner join domains on pages.domain_id = domains.id
        group by domain  order by occurrence desc;