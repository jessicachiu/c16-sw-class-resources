import express from "express";
import { Request, Response } from "express";
import expressSession from "express-session";
import path from "path";

const app = express();

app.use(
    expressSession({
        secret: "lfsiudfg9q847(*&)(8",
        resave: true,
        saveUninitialized: true,
    })
);

app.get("/helloworld", (req: Request, res: Response) => {
    console.log("Before Session > name : ", req.session["name"]);

    req.session["name"] = "Tecky Student";
    console.log("After Session > name : ", req.session["name"]);

    let data = { name: "Peter" };
    res.json(data);
});

app.get("/", (req, res, next) => {
    if (Math.random() > 0.5) {
        next(); // Going to run the next middleware
        return;
    }
    // Skip all middlewares afterwards
    res.end("Random value smaller than 0.5");
});

app.get("/", (req, res, next) => {
    if (Math.random() > 0.5) {
        next();
        return;
    }
    res.end("Random value smaller than 0.5");
});

app.get("/personal/info", (req: Request, res: Response) => {
    res.end("I am Dickson1");
});
app.get("/personal/info", (req: Request, res: Response) => {
    res.end("I am Dickson2");
});

app.get("/test", (req, res) => {
    // extra logic
    res.sendFile(path.join(__dirname, "assets", "test.txt"));
});

app.use(express.static("assets"));
app.use(express.static("public"));

//包底
app.use((req, res, next) => {
    res.sendFile(path.resolve("./public/404.html"));
});

const PORT = 8080;
app.listen(PORT, () => {
    console.log(`Server 已經ready http://localhost:${PORT}`);
});
