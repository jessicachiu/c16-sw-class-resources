
from datetime import datetime
from datetime import timedelta
import math

new_year = datetime(2020,1,1,0,0,0,0)

hk_holidays = [
    datetime(2020,1,1),
    datetime(2020,2,12),
    datetime(2020,2,13),
    # Add the rest of the holidays
]

# 你試下做埋佢喇
with open('time_dimension.csv','a') as f:
    for num in range(365):
        date = new_year + timedelta(days=num)
        data_row = {
            "date": date.strftime('%Y-%m-%d'),
            "year":date.strftime('%Y'),
            "month_num": int(date.strftime('%m')),
            "month_end_short": date.strftime('%b'),
            "month_end_long": date.strftime('%B'),
            "day_of_month": date.strftime('%d'),
            "day_of_week_num": date.strftime('%w'),
            "day_of_week_eng_short": date.strftime('%a'),
            "day_of_week_eng_long": date.strftime('%A'),
            "quarter": math.ceil(date.month / 3),
            "is_work_day": date.strftime('%w') in [str(num) for num in range(1,6)] and date not in hk_holidays,
            "is_holiday":  date in hk_holidays
        }
        # columns = []
        # for key in data_row:
        #     columns.append('{'+key+'}')
        columns = ['{'+key+'}' for key in data_row]
        line = ','.join(columns) + '\n'
        f.write(line.format(**data_row))
        
        # f.write("{date},{year},{month_num},{month_end_short}"\
        #             ",{month_end_long},{day_of_month},{day_of_week_num},{day_of_week_eng_short},{day_of_week_eng_long},"\
        #             "{quarter},{is_work_day},{is_holiday}\n".format(**data_row))
        f.flush()

