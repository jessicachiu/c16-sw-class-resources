import json
from sseclient import SSEClient as EventSource

url = 'https://stream.wikimedia.org/v2/stream/page-create';

def new_wikipedia_csv():
    with open('wikipedia.csv','w') as f:
        f.truncate(0) # 清空成個口CSV
        # 加返CSV 嘅header
        f.write("user_text,user_is_bot,user_registration_dt,user_edit_count,domain,uri,database,page_id,page_title,rev_timestamp,rev_minor_edit,rev_len,rev_content_model,rev_content_format,comment,parsedcomment\n")

with open('wikipedia.csv','a') as f:
    
    # 呢個 for-loop ，其實係等你Ctrl+C 先會完
    for event in EventSource(url):
        if event.event == "message" and not event.data == "":
            # Extract
            event_data = json.loads(event.data)
            try:
                # Transform
                data_row = {
                    "user_text": event_data['performer']['user_text'],
                    "user_is_bot": event_data['performer']['user_is_bot'],
                    "user_registration_dt": event_data['performer'].get('user_registration_dt',''),
                    "user_edit_count": event_data['performer'].get('user_edit_count',''),
                    "domain": event_data['meta']['domain'],
                    "uri": event_data['meta']['uri'],
                    "database": event_data['database'],
                    "page_id": event_data['page_id'],
                    "page_title": event_data['page_title'],
                    "rev_timestamp": event_data['rev_timestamp'],
                    "rev_minor_edit": event_data['rev_minor_edit'],
                    "rev_len": event_data['rev_len'],
                    "rev_content_model": event_data['rev_content_model'],
                    "rev_content_format":event_data['rev_content_format'],
                    "comment": event_data['comment'].replace("\n"," "),
                    "parsedcomment": event_data['parsedcomment'].replace("\n"," ")
                }

                # Load
                f.write("{user_text},{user_is_bot},{user_registration_dt},{user_edit_count}"\
                    ",{domain},{uri},{database},{page_id},{page_title},{rev_timestamp},{rev_minor_edit},"\
                    "{rev_len},{rev_content_model},{rev_content_format},{comment},{parsedcomment}\n".format(**data_row))
                f.flush()
            except Exception as err:
                print(err)
            # Do what you need to do here


