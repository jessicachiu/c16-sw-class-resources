import { ChangeEvent, FormEvent, useEffect, useState } from "react"

export function Countdown() {
  const [seconds, setSeconds] = useState(0)
  const [counting, setCounting] = useState(0)
  const [timerId, setTimerId] = useState(0)

  const onSecondChange = (e: ChangeEvent<HTMLInputElement>) => setSeconds(parseInt(e.currentTarget.value))
  const onSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    setCounting(seconds)
    const intervalId = window.setInterval(() => {
      console.log('???')
      setCounting(counting => counting - 0.016)
    }, 16)
    setTimerId(intervalId)
  }

  useEffect(() => {
    // on mount

    return () => {
      // destructor
      clearInterval(timerId)
    }
  }, [timerId])

  return (
    <div>
      <form onSubmit={onSubmit}>
        <input type="text" value={seconds} onChange={onSecondChange} />seconds
        <input type="submit" value="Count!" />
      </form>

      {counting.toFixed(2)}
    </div>
  )
}

