import { useState } from "react"
import { Countdown } from "./Countdown"

export function CountdownList() {
  const [numberOfCountdown, setNumberOfCountdown] = useState(0)

  return (
    <div>
      <button onClick={() => setNumberOfCountdown(numberOfCountdown + 1)}>+</button>
      <button onClick={() => setNumberOfCountdown(numberOfCountdown - 1)}>-</button>

      {Array(numberOfCountdown).fill(null).map((_, i) => {
        return <Countdown key={i} />
      })}
    </div>
  )
}
