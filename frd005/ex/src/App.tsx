import React, { useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import './App.css';
import { addName } from './redux/leaderboard/actions';
import { nextStep, resetGame } from './redux/tictactoe/actions';
import { Square } from './Square';
import { RootState } from './store';

const winningConditions = [
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
  [0, 4, 8],
  [2, 4, 6],
];

function App() {
  const dispatch = useDispatch();

  const boardState = useSelector((state: RootState) => state.tictactoe.board);
  const turn = useSelector((state: RootState) => state.tictactoe.turn)
  const [mousePosition, setMousePosition] = useState(0)

  // FRD003 useEffect()
  const leaderboard = useSelector((state: RootState) => state.leaderboard.names)

  const [leaderboardLoaded, setLeaderboardLoaded] = useState(false)

  useEffect(() => {
    const oldNames = JSON.parse(localStorage.getItem('leaderboard') ?? '[]')
    for (let name of oldNames) {
      dispatch(addName(name))
    }
    setLeaderboardLoaded(true)
  }, [dispatch, setLeaderboardLoaded])

  useEffect(() => {
    if (leaderboardLoaded) {
      localStorage.setItem('leaderboard', JSON.stringify(leaderboard))
    }
  }, [leaderboardLoaded, leaderboard])

  // check winner
  let [winner, winningRow] = useMemo(() => {
    let winner: 'X' | 'O' | null = null;
    let winningRow: number[] | null = null;
    for (let winningCondition of winningConditions) {
      if (boardState[winningCondition[0]] &&
        (boardState[winningCondition[0]] === boardState[winningCondition[1]] &&
          boardState[winningCondition[1]] === boardState[winningCondition[2]])
      ) {
        winner = boardState[winningCondition[0]]
        winningRow = winningCondition
      }
    }
    console.log('run jo 一次')
    return [winner, winningRow];
  }, [boardState]);

  useEffect(() => {
    console.log('[useEffect] 計 jo winner 一次')
    if (winner != null) {
      dispatch(addName(winner))
    }
    return () => {
      console.log('[useEffect] destruct 咗一次')
    }
  }, [winner] /* dependency list */)

  return (
    <section>
      <main>
        <div className='top-row'>
          <div className='level-dropdwon'>
            <i className="fas fa-chevron-down"></i>
            <select className='label' defaultValue="Medium">
              <option>Easy</option>
              <option>Medium</option>
              <option>Hard</option>
            </select>
          </div>
          <i className="fas fa-share-alt"></i>
        </div>
        {leaderboard}
        <div className='scoring-row'>
          <div className='scoring-container'>
            <div className='scoring-container-inner'>
              <div className='player'>X</div>
              <div className='x-counter'>0</div>
            </div>
          </div>
          <div className='scoring-container'>
            <div className='scoring-container-inner'>
              <div className='player'>O</div>
              <div className='o-counter'>0</div>
            </div>
          </div>
        </div>
        <div className='turn-row'>
          { winner != null && `${winner} is the winner \u{1F439}` }
          { winner == null && turn % 2 === 1 ? <><i className="fas fa-times"></i> Turn</> : <><i className="far fa-circle"></i> Turn</> }
        </div>
        <div className='board-row'>
          <div className='board' onMouseMove={e => setMousePosition(e.clientX)} style={{opacity: mousePosition/1000}}>
            {Array(9).fill(null).map((_, i) => {
              return <Square
              key={i}
              value={boardState[i]}
              highligthed={winningRow != null && winningRow.indexOf(i) > -1}
              onClick={() => {
                if (boardState[i] !== null || winner != null) {
                  return;
                }
                dispatch(nextStep(i))
              }} />
            })}
          </div>
        </div>
        <div className='restart-container' onClick={() => {
          dispatch(resetGame())
        }}>
          restart game
        </div>

      </main>
      <footer>
        <i>feedback</i>
      </footer>
    </section>
  );
}

export default App;
