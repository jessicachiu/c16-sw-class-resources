export function nextStep(position: number) {
  return {
    type: '@@tictactoe/NEXT_STEP' as const,
    position
  }
}

export function resetGame() {
  return {
    type: '@@tictactoe/RESET_GAME' as const
  }
}

export type TictactoeActions = ReturnType<typeof nextStep> | ReturnType<typeof resetGame>;