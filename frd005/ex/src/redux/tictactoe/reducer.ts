import produce from "immer"
import { TictactoeActions } from "./actions";

export interface TictactoeState {
  board: ('X' | 'O' | null)[],
  turn: number
}

const initialState: TictactoeState = {
  board: [null, null, null, null, null, null, null, null, null],
  turn: 0
}

export function tictactoeReducer(state: TictactoeState = initialState, action: TictactoeActions): TictactoeState {
  return produce(state, state => {
    if (action.type === '@@tictactoe/NEXT_STEP') {
      state.board[action.position] = state.turn % 2 == 0 ? 'O' : 'X'
      state.turn += 1
    } else if (action.type === '@@tictactoe/RESET_GAME') {
      return initialState
    }
  });
}