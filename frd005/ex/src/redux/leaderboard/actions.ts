export function addName(name: string) {
  return {
    type: '@@leaderboard/ADD_NAME' as const,
    name
  }
}

export type LeaderboardActions = ReturnType<typeof addName>;