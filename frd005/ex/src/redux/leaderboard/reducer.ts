import produce from "immer"
import { LeaderboardActions } from "./actions"

export interface LeaderboardState {
  names: string[]
};

const initialState: LeaderboardState = { 
  names: []
}

export function leaderboardReducer(state: LeaderboardState = initialState, action: LeaderboardActions): LeaderboardState {
  return produce(state, state => {
    if (action.type === '@@leaderboard/ADD_NAME') {
      state.names.push(action.name)
    }
  })
}