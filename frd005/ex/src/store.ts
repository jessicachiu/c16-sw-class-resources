import { combineReducers, createStore } from 'redux'
import { leaderboardReducer, LeaderboardState } from './redux/leaderboard/reducer';
import { tictactoeReducer, TictactoeState } from './redux/tictactoe/reducer';

export interface RootState {
  tictactoe: TictactoeState;
  leaderboard: LeaderboardState;
}
declare global {
    /* tslint:disable:interface-name */
    interface Window {
       __REDUX_DEVTOOLS_EXTENSION__: any
    }
}

const reducer = combineReducers<RootState>({
  tictactoe: tictactoeReducer,
  leaderboard: leaderboardReducer
})

export const store = createStore(reducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)