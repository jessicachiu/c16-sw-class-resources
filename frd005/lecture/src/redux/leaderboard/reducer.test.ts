import { addLeaderboard } from "./actions";
import { leaderboardReducer, LeaderboardState } from "./reducer"

describe('Test leaderboard reducer', () => {
  it('can add leader', () => {
    const initialState: LeaderboardState = {
      leaders: []
    };

    const action = addLeaderboard('Alex', '2020-01-01')

    const result = leaderboardReducer(initialState, action);

    expect(result).toEqual({
      leaders: [
        {name: 'Alex', date: '2020-01-01'}
      ]
    })
  })
})