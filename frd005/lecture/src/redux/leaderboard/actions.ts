export function addLeaderboard(name: string, date: string) {
  return {
    type: '@@leaderboard/ADD_LEADER' as const,
    name,
    date
  }
}

export type LeaderboardActions = ReturnType<typeof addLeaderboard>;