import { playerMove } from "./actions"
import { gameReducer, GameState } from "./reducer"

describe('Game reducer', () => {
  it('should add new progress', () => {
    const initialState: GameState = {
      players: [],
      progress: {
        '1': null as any,
        '2': null as any
      },
      losers: [],
      canMove: true
    }
    const action = playerMove(3);
  
    const result = gameReducer(initialState, action);

    expect(result).toEqual({
      players: [],
      progress: {
        '1': null as any,
        '2': null as any,
        '3': 0
      },
      losers: [],
      canMove: true
    })
  })

  it('should add new progress when start moving', () => {
    const initialState: GameState = {
      players: [],
      progress: {
        '1': null as any,
        '2': null as any
      },
      losers: [],
      canMove: true
    }
    const action = playerMove(3);
  
    let result = gameReducer(initialState, action);
    result = gameReducer(result, action);

    expect(result).toEqual({
      players: [],
      progress: {
        '1': null as any,
        '2': null as any,
        '3': 1
      },
      losers: [],
      canMove: true
    })
  })
})