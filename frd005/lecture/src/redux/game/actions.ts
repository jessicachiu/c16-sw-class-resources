// action creator 
export function addPlayer(id: number, name: string, color: string) {
  return {
    type: '@@game/ADD_PLAYER' as const,
    id,
    name,
    color
  }
}

export function playerMove(id: number) {
  return {
    type: '@@game/PLAYER_MOVE' as const,
    id
  }
}

export function switchCanMove() {
  return {
    type: '@@game/SWITCH_CAN_MOVE' as const
  }
}

export type GameActions = ReturnType<typeof addPlayer> |
                          ReturnType<typeof playerMove> |
                          ReturnType<typeof switchCanMove>