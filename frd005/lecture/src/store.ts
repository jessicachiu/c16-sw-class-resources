import { combineReducers, createStore } from "redux";
import { gameReducer, GameState } from "./redux/game/reducer";
import { leaderboardReducer, LeaderboardState } from "./redux/leaderboard/reducer";

export interface RootState {
  game: GameState
  leaderboard: LeaderboardState
}

declare global {
  /* tslint:disable:interface-name */
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION__: any
  }
}

const reducer = combineReducers<RootState>({
  game: gameReducer,
  leaderboard: leaderboardReducer
})

export const store = createStore(reducer, 
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);