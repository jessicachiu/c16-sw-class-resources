const array1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]; // A memory block enough to hold the reference of ten numbers is created
array1[0]; // Accessing the first index of the memory block
array1[5]; // Accessing the sixth index of the memory block
array1.push(11); // Adding the eleventh elements and JS runtime reallocates a longer memory block for you automatically.
array1.splice(3, 1); // Remove the third element

// Linked list
function cons(value, next) {
  return {
    value: value,
    next: next,
  };
}

let linkListHead = cons(
  1,
  cons(2, cons(3, cons(4, cons(4, cons(24, cons(14, null))))))
);

function getIndexAt(head, index) {
  let i = 0;
  let item = head;
  while (i < index) {
    if (!item.next) {
      throw new Error("No items at index " + index);
    }
    item = item.next;
    i++;
  }
  return item;
}

console.log(getIndexAt(linkListHead, 2));
linkListHead = cons(0, linkListHead); // The new head item becomes newHead instead of head.
getIndexAt(linkListHead, 2).next = null;

// Object

let obj = {};
obj.name = "Peter";
obj["age"] = 26;
let key = "gender";
obj[key] = "M";
obj.age = 99;
delete obj.age;
// Map

let map = new Map();
map.set("key1", "value1");
map.set("key2", "value2");
map.set("key1", "value3"); // value1 is overwritten after this line.

console.log(map.get("key2"));
console.log(map.get("key3"));
map.delete("key1");
map.set("key4", "value1");

// Set

const set = new Set();
set.add(1);
set.add(1);
set.add(10);
set.add(20);
set.add(2);
set.add(1);
var s = new Set([2, 4, 6, 8]);
let arr = Array.from(s);

// Stack
const stack = [];
stack.push(1);
stack.push(2);
stack.push(3);
stack.push(4);
stack.push(5);
stack.pop(); // 5
stack.pop(); // 4

function func1() {
  console.log("I am func 1");
  func2();
}
function func2() {
  console.log("I am func 2");
  func3();
}
function func3() {
  console.log("I am func 3");
  func4();
}
function func4() {
  console.log("I am func 4");
}
func1();

// Queue
// const queue = [];
// queue.push(1);
// queue.push(2);
// queue.push(3);
// queue.push(4);
// queue.push(5);
// queue.shift(); //1
// queue.shift(); //2

function queue() {
  const store = [];
  return {
    enqueue(item) {
      store.push(item);
    },
    dequeue() {
      return store.shift();
    },
    getQueue() {
      return store;
    },
  };
}

let myQueue = queue();
myQueue.enqueue(1233);
myQueue.enqueue(5);
myQueue.enqueue(44);
myQueue.enqueue(0);

// tree

function tree(value, left, right) {
  return {
    value,
    left,
    right,
  };
}

let myTree = tree(
  2,
  tree(7, tree(2), tree(6, tree(5), tree(11))),
  tree(5, tree(9, tree(4)))
);

// Graph
function node(value, nodes = []) {
  return {
    value,
    nodes,
  };
}

const ten = node(10);
const nine = node(9);
const two = node(2);

const eleven = node(11, [two, nine, ten]);
const eight = node(8, [nine]);
const five = node(5, [eleven]);
const seven = node(7, [eleven, eight]);
const three = node(3, [eight, ten]);

// Time complexity
// O(n)
const arr2 = [1, 2, 3, 4, 5, 6];
let sum = 0;
for (let elem of arr2) {
  sum += elem;
}

// O(n^2).
const arr3 = [1, 2, 3, 4, 5, 6];
const products = [];
for (let elem1 of arr2) {
  for (let elem2 of arr3) {
    products.push(elem1 * elem2);
  }
}

// Sort
[7, 6, 5, 5, 3, 1].sort(function (a, b) {
  return a - b;

  // a - b  -----> -ve    (keep position)
  // a - b  -----> ==0    (keep position)
  // a - b  -----> +ve    (swap)
  // effect : accending order
});
// a - b  -----> -ve    (keep position)
// a - b  -----> ==0    (keep position)
// a - b  -----> +ve    (swap)
// effect : accending order

//   [(7, 6, 5, 5, 3, 1)].sort(function (a, b) {
//     return a - b;

//   });

let student1 = { name: "Peter", age: 10 };
let student2 = { name: "Betty", age: 11 };
let student3 = { name: "Charlie", age: 22 };
let student4 = { name: "Kate", age: 40 };

let students = [student4, student2, student3, student1];

students.sort(function (studentA, studentB) {
  if (studentA.age < studentB.age) {
    return 1;
  }
  if (studentA.age > studentB.age) {
    return -1;
  }
});

// Bubble sort

function bubbleSort(arr) {
  const sortedArr = arr.slice(0);
  let swapped = true;
  while (swapped) {
    swapped = false;
    for (let i = 1; i < sortedArr.length; ++i) {
      if (sortedArr[i - 1] > sortedArr[i]) {
        [sortedArr[i], sortedArr[i - 1]] = [sortedArr[i - 1], sortedArr[i]];
        swapped = true;
      }
    }
  }
  return sortedArr;
}

// Quick sort

function quicksortBasic(array) {
  if (array.length < 2) {
    return array;
  }

  const pivot = array[0];
  const lesser = [];
  const greater = [];

  for (let i = 1; i < array.length; i++) {
    if (array[i] < pivot) {
      lesser.push(array[i]);
    } else {
      greater.push(array[i]);
    }
  }

  return quicksortBasic(lesser).concat(pivot, quicksortBasic(greater));
}

// Linear Search
[4, 3, 5, 23, 4, 3, 2, 1, 56].find((elem) => {
  return elem == 56;
});

// Binary search

function tree(v, l, r) {
  let value = v;
  let left = l;
  let right = r;
  return {
    getValue() {
      return value;
    },
    toString(tabs = "\t") {
      return (
        `Value:${value}\n` +
        `${tabs}|=== ${left && left.toString(tabs + "\t")}\n` +
        `${tabs}|=== ${right && right.toString(tabs + "\t")}\n`
      );
    },
    search(num) {
      if (num === value) {
        return num;
      } else if (num > value && right) {
        return right.search(num);
      } else if (num < value && left) {
        return left.search(num);
      } else {
        return null;
      }
    },
    insert(num) {
      if (num === value) {
        return;
      } else if (num > value) {
        if (right) {
          right.insert(num);
        } else {
          right = tree(num, null, null);
        }
      } else if (num < value) {
        if (left) {
          left.insert(num);
        } else {
          left = tree(num, null, null);
        }
      }
    },
  };
}

let root = tree(
  5,
  tree(4, tree(2, tree(1), tree(3))),
  tree(8, tree(7, tree(6)), tree(9, null, tree(10)))
);

root.insert(13);
console.log(root.toString());
root.search(13);
