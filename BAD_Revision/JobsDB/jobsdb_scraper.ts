import { Page, chromium, Browser } from "playwright";
let page: Page;
let browser: Browser;

async function main() {
    browser = await chromium.launch({
        headless: false,
    });
    page = await browser.newPage();

    await page.goto("https://hk.jobsdb.com/hk");

    await page.fill("input[data-automation=searchKeywordsField]", "java");

    await page.evaluate(() => {
        // Inisde fake Browser

        const jobTitle = document.querySelector("input[data-automation=searchKeywordsField]");

        const jobFunction = document.querySelector("[data-automation=jobFunctionChecklistField] > div >div > button");
        const submitButton = document.querySelector("button[data-automation=searchSubmitButton]");

        if (!jobTitle) {
            throw new Error("No username input found");
        }
        if (!jobFunction) {
            throw new Error("No password input found");
        }
        if (jobTitle && jobFunction) {
            console.log("before jobTitle  =", (jobTitle as HTMLInputElement).value);

            // (jobTitle as HTMLInputElement).value = "teckyyyyyyyy";
            console.log("after jobTitle  =", (jobTitle as HTMLInputElement).value);

            (jobFunction as HTMLInputElement).click();
            const ITRow = [].slice.call(document.querySelectorAll("span")).filter(function (item: any) {
                return item.innerText.includes("IT");
            })[0];
            (ITRow as HTMLInputElement).click();
            (submitButton as HTMLElement).click();
        }
    });

    // await page.screenshot({ path: "./login_typed.png" });
    // await page.evaluate(() => {
    //     const submit = document.querySelector("[type=submit]");
    //     if (!submit) {
    //         throw new Error("No submit button found");
    //     }

    //     if (submit) {
    //         (submit as HTMLInputElement).click();
    //     }
    // });
    await delay(2000);
    await page.screenshot({ path: "./step2.jpg" });
    // const results = await page.$$("input");
    // const results = await page.$$eval('[data-automation="backdrop"]', (el) => el);
    // console.log("results = ", results);

    // console.log(await page.content());

    // const results2 = await page.waitForSelector('[data-automation="job-card-2"]');

    let result = await page.$$eval('[data-automation="job-card-2"]', (els) => {
        let arr = new Array();
        for (let index = 0; index < els.length; index++) {
            arr.push(els[index].innerHTML);
        }
        return arr;
    });
    console.log(result);

    // await page.screenshot({ path: "./home.png" });
    // const studentMain = await page.evaluate(() => document.querySelector("#student-main"));
    // const title = await page.title();
    // done();
}

export async function done() {
    await page.close();
    await browser.close();
}

function delay(time: number) {
    return new Promise(function (resolve) {
        setTimeout(resolve, time);
    });
}
main();
