import express from "express";
import expressSession from "express-session";
import { format } from "fecha";
import { Request, Response } from "express";
import multer from "multer";
import jsonfile from 'jsonfile';
import {isLoggedIn} from './guard';
import bodyParser from 'body-parser';

const storage = multer.diskStorage({
    destination:function(req,file,cb){/* cb = callback */
        cb(null,"uploads")
    },
    filename:function(req,file,cb){
        cb(null,`memo-image-${file.fieldname}.${file.mimetype.split('/')[1]}`)
    }
})
const upload = multer({storage});

/////////////////////  Set up
const app = express();
app.use(bodyParser.urlencoded())
app.use(bodyParser.json())

app.use(
    expressSession({
        secret: "Tecky Academy teaches typescript",
        resave: true,
        saveUninitialized: true,
    })
);

//////////////////////  Main Route / Logic
app.use(express.static("public"));
app.use((req, res, next) => {
    // [2018-11-12 09:21:31] Request /index.js
    let dateTimeString = format(new Date(), "YYYY-MM-DD HH:mm:ss");
    console.log(`[${dateTimeString}] ${req.method} ${req.path}`);
    next();
});
app.use((req, res, next) => {
    let counter = req.session["counter"] || 0;
    counter++;
    req.session["counter"] = counter;
    console.log(` 你上左黎 : ${req.session["counter"]}`);
    next();
});

interface Memo{
    content:string,
    image?:string
}

app.post("/memo", upload.single("image"), async (req: Request, res: Response) => {
    try {
        const {content} = req.body;
        const image = req.file?.filename;
        const memos: Memo[] = await jsonfile.readFileSync("memos.json");
        if (image) {
            memos.push({
                content,
                image
            })
        } else {
            memos.push({
                content
            })
        }
        jsonfile.writeFileSync("memos.json", memos);
        res.json({success:true});
    } catch (err) {
        console.log(err)
        res.json({success:false,error:err});
    }
});

app.get("/memos",async(req:Request,res:Response)=>{
    const memos = await jsonfile.readFileSync("memos.json");
    res.json(memos);
})

interface User{
    username:string,
    password:string
}

app.post("/login", async (req: Request, res: Response) => {
    try {
        const { username, password } = req.body;
        const users: User[] = await jsonfile.readFileSync("users.json");
        const usernameCheck = users.filter(user => user.username === username);
        if (usernameCheck.length == 0) {
            return res.json({ success: false, error: "User doesn't exist!" });
        }
        if (usernameCheck[0].password != password) {
            return res.json({ success: false, error: "Wrong Password!" });
        }
        req.session["user"] = users[0];
        return res.json({ user: usernameCheck[0] });
    } catch (err) {
        console.log(err);
        return res.json({ success: false, error: err });
    }
})

app.get("/user", (req: Request, res: Response) => {
    res.json({user:req.session["user"]});
});

app.put("/memo/:id",upload.single("image"),async (req: Request, res: Response) => {
    try {
        const {id/*string!*/} = req.params;
        const {content} = req.body;
        const image = req.file?.filename;
        const memos: Memo[] = await jsonfile.readFileSync("memos.json");
        const memoId = parseInt(id);
        memos[memoId].content = content;
        if (image){
            memos[memoId].image = req.file?.filename;
        }
        jsonfile.writeFileSync("memos.json", memos);
        res.json({success:true});
    } catch (err) {
        console.log(err)
        res.json({success:false,error:err});
    }
});

app.delete("/memo/:id", async (req: Request, res: Response) => {
    try {
        const { id } = req.params;
        const memoId = parseInt(id);
        const memos: Memo[] = await jsonfile.readFileSync("memos.json");
        memos.splice(memoId, 1);
        await jsonfile.writeFileSync("memos.json", memos);
        res.json({ success: true });
    } catch (err) {
        console.log(err);
        res.json({ success: false });
    }
});


app.use(express.static("uploads"));
app.use(isLoggedIn,express.static("protected"));
// app.use((req, res, next) => {
//     res.redirect("/404.html");
// });

const PORT = 8080;

app.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}/`);
});