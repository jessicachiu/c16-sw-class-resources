import express from "express";
import multer from "multer";
import path from "path";
const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.resolve("./uploads"));
    },
    filename: function (req, file, cb) {
        cb(
            null,
            `${file.fieldname}-${Date.now()}.${file.mimetype.split("/")[1]}`
        );
    },
});
const upload = multer({ storage });

const images: string[] = [
    "https://wikipedia.org/static/images/project-logos/frwiki-1.5x.png",
    "https://upload.wikimedia.org/wikipedia/commons/7/77/Delete_key1.jpg",
];

app.use((req, res, next) => {
    console.log(req.method, req.path, req.body);
    next();
});

app.post("/application", (req, res, next) => {
    const { name, age } = req.body;
    console.table({ name, age });
    res.status(202).json({ msg: "json post application OK" });
    // res.end("POst application OK");
});

app.post("/application-file", upload.single("icon"), (req, res, next) => {
    const file = req.file;

    const { name, age } = req.body;
    console.table({ name, age, file: file!["filename"] });
    res.status(201).json({ msg: "post application file OK" });
    // res.end("POst application OK");
});

app.post("/img", (req, res, next) => {
    if (Math.random() < 0.5) {
        res.end(images[0]);
    } else {
        res.end(images[1]);
    }
});

app.use(express.static("public"));

const PORT = "8181";
app.listen(PORT, () => {
    console.log(`Listening on http://localhost:${PORT}`);
});
