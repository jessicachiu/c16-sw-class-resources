console.log("Js loaded");

// For ex1
// document.querySelector('#box-1').addEventListener('click',function(event){
//     // 1. Get the element itself by event.target or event.currentTarget
//     // console.log('event.target', event.target);
//     // console.log('event.currentTarget', event.currentTarget);

//     const squarElem  = event.currentTarget
//     const iconElem = squarElem.querySelector('i')
//     // 2. Check if the box is occupied
//     if(!iconElem){
//     // 3, Put the X in if the box is not occupied.
//         squarElem.innerHTML = 'X'
//     }

// });

// document.querySelector('#box-1').addEventListener('click',function(event){

//     const squarElem  = event.currentTarget
//     const iconElem = squarElem.querySelector('i')
//     if(!iconElem){
//         squarElem.innerHTML = 'X'
//     }

// });

let turn = 1; // and increment it for every move.
const squareElements = document.querySelectorAll(".square");
const turnElem = document.querySelector(".turn-row");
const resetElem = document.querySelector(".restart-container");
const xCounterElem = document.querySelector('.x-counter');
const oCounterElem = document.querySelector('.o-counter');

const circleIcon = '<i class="far fa-circle"></i>';
const crossIcon = '<i class="fas fa-times"></i>';
let winner = ''
const winningConditions = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ]
  // [(null, null, null, "X", "O", "O", "O", "X", "X")];



  resetElem.addEventListener('click',function(){
    for (let squareElem of squareElements) {
        squareElem.innerHTML = ''
    }
    winner =''
    turn = 1
    xCounterElem.innerHTML = '0'
    xCounterElem.innerHTML = '0'
    turnElem.innerHTML = '<i class="fas fa-times"></i> Turn'
  })

for (let squareElem of squareElements) {
  squareElem.addEventListener("click", function (event) {
    const squarElem = event.currentTarget;
    const iconElem = squarElem.querySelector("i");
    if (!iconElem && !winner) {
      if (turn % 2 != 0) {
        squarElem.innerHTML = crossIcon;
        turnElem.innerHTML = `${circleIcon} Turn`
      } else {
        squarElem.innerHTML = circleIcon;
        turnElem.innerHTML = `${crossIcon} Turn`
      }
      turn ++;
      checkWin();
    }
    console.log("turn = ", turn);
  });
}

function getCurrentBoard() {
  let currentBoard = [];
  for (let squareElem of squareElements) {
    if (squareElem.querySelector(".fa-circle")) {
      currentBoard.push("O");
    } else if (squareElem.querySelector(".fa-times")) {
      currentBoard.push("X");
    } else {
      currentBoard.push(null);
    }
  }
  return currentBoard;
}

function checkWin() {
  let currentBoard = getCurrentBoard();
  // [(null, null, null, "X", "O", "O", "O", "X", "X")];

  console.log('turn = ', turn);

    if(turn < 10){
   //   console.log(currentBoard);
        for (let winningCondition of winningConditions){
            console.log("winningCondition = ", winningCondition);
            if(currentBoard[winningCondition[0]] && 
                (currentBoard[winningCondition[0]] == currentBoard[winningCondition[1]] &&
                currentBoard[winningCondition[1]] == currentBoard[winningCondition[2]])
            ){
                console.log('Found winner : ', currentBoard[winningCondition[0]])
                winner = currentBoard[winningCondition[0]]
                turnElem.innerHTML = `${winner} is the winner \u{1F439}`
                // Wrok around
                setTimeout(()=>{continueFunction(turnElem.innerHTML)},0) 
                if (winner == 'X'){
                    xCounterElem.innerHTML = parseInt(xCounterElem.innerHTML) + 1
                }else{
                    oCounterElem.innerHTML = parseInt(oCounterElem.innerHTML) + 1
                }
            }
        }
  }else{
      winner = 'draw'
      turnElem.innerHTML = `Game is draw`
      // Wrok around
      setTimeout(()=>{continueFunction(turnElem.innerHTML)},0) 
  }
   

}

function continueFunction(msg) {
    let answer = confirm(`${msg}. \nContinue?`);
    if (answer){
        for (let squareElem of squareElements) {
            squareElem.innerHTML = ''
        }
        winner = ''
        turn = 1
    }
  }

