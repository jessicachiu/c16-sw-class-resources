window.onload = function(){

    console.log('You can see me')

    const canvas = document.getElementById('canvas');
    const ctx = canvas.getContext('2d');
    ctx.fillStyle = 'green';
    ctx.fillRect(10, 10, 150, 100);
    
    // document.querySelector('#header1').onclick = function(event){ 
    //     // The event object contains the information about the event
    //     console.log("header1 is clicked! AAAA");
    // }
    
    // document.querySelector('#header1').onclick = function(event){ 
    //     // The event object contains the information about the event
    //     console.log("header1 is clicked! BBBBB");
    // }
    
    const headerElem = document.querySelector('#header1')
    headerElem.addEventListener('click',function(event){
        // The event object contains the information about the event
        // console.log("header1 is clicked! CCCCC");
        console.log('event.target = ', event.target)
        console.log('event.currentTarget = ', event.currentTarget)
        // headerElem.innerHTML = 'CCCC'
        // event.currentTarget.innerHTML = "CCCCC"
        // event.target.innerHTML = parseInt(event.target.innerHTML)+1
    });
    
    
    // Loop add eventListener
    // let listItems = document.querySelectorAll('#my-list .list-item');
    // for(let listItem of listItems){
    //     listItem.addEventListener('click',function(event){
    //         console.log(event.target , "has been clicked!")
    //         // console.log(`${event.target} has been clicked!`);
    //     })
    // }
    
    // Add only on parent
    
    document.querySelector('#my-list').addEventListener('click',function(event){
        if(event.target && event.target.matches('.list-item')){
            console.log(event.target , "has been clicked!")
        }
    });


}
