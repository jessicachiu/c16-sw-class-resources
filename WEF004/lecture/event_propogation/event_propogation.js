window.onload=function(){
    console.log('Window is onloaded')
    document.querySelector('.head').addEventListener('click', function(event){
        console.log("-----頭-----")
        console.log("event.target = " , event.target)
        console.log("event.currentTarget = " , event.currentTarget)
    })
    document.querySelector('.face').addEventListener('click', function(event){
        console.log("-----面-----")
        console.log("event.target = " , event.target)
        console.log("event.currentTarget = " , event.currentTarget)
        event.stopPropagation() // Stop propagate event to parent level

    })
    document.querySelector('.nose').addEventListener('click', function(event){
        console.log("-----鼻-----")
        console.log("event.target = " , event.target)
        console.log("event.currentTarget = " , event.currentTarget)
        event.stopPropagation()  // Stop propagate event to parent level
    })
}