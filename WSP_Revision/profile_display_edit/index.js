let editMode = false;
let profileDataFromDB = {
    edu: {
        university: "科大",
        secondary: "救世軍",
    },
    eduNotHC: [{ institution: "university", name: "UST" }],
    career: [{ company: "Tecky ltd" }, { company: "BBC" }, { company: "13BBC" }, { company: "BB2211C" }, { company: "BBdfC" }, { company: "BBC" }, { company: "BBC" }, { company: "BBC" }],
};
function initData() {
    document.querySelector("#display #university").innerHTML = profileDataFromDB.edu.university;
    document.querySelector("#display #secondary").innerHTML = profileDataFromDB.edu.secondary;
    document.querySelector("#edit input[name=university]").value = profileDataFromDB.edu.university;
    document.querySelector("#edit input[name=secondary]").value = profileDataFromDB.edu.secondary;
    document.querySelector("#display").classList.remove("hide");
    document.querySelector("#edit").classList.remove("show");

    // Dynamic field handling
    document.querySelector("#display-company").innerHTML = "";
    document.querySelector("#edit-company").innerHTML = "";
    for (let careerItem of profileDataFromDB.career) {
        document.querySelector("#display-company").innerHTML += `<div>${careerItem.company}</div>`;
        document.querySelector("#edit-company").innerHTML += `<input value='${careerItem.company}'><br><br>`;
    }
}
document.querySelector("#editBtn").addEventListener("click", () => {
    console.log("Edit now!");
    editMode = true;
    document.querySelector("#display").classList.add("hide");
    document.querySelector("#edit").classList.add("show");
});
document.querySelector("#cancelBtn").addEventListener("click", () => {
    editMode = false;
    document.querySelector("#display").classList.remove("hide");
    document.querySelector("#edit").classList.remove("show");
});
document.querySelector("#restoreBtn").addEventListener("click", () => {
    editMode = false;
    initData();
});
document.querySelector("#confirmBtn").addEventListener("click", async () => {
    let updateObject = {
        edu: {
            university: document.querySelector("#edit input[name=university]").value,
            secondary: document.querySelector("#edit input[name=secondary]").value,
        },
    };

    // dynamic handling

    let careerInputElems = document.querySelectorAll("#edit-company input");
    let career = [];
    for (let careerInputElem of careerInputElems) {
        career.push({ company: careerInputElem.value });
    }
    updateObject.career = career;
    console.log(updateObject);
    await updateProfile(updateObject);
    initData();
});

async function updateProfile(updateObject) {
    console.log("Connecting to db");
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            profileDataFromDB = updateObject;
            console.log("updateProfile done");

            resolve();
        }, 500);
    });
}
initData();
