import express from "express";
import http from "http";
import socketIO from "socket.io";
import MessageStore from "./messageStore";
import expressSession from "express-session";
// import path from "path";

let version = 25;

let answer = Math.floor(Math.random() * 100) + 1;
let left = 1;
let right = 100;
let round = 1;

let app = express();
let server = new http.Server(app);
let io = new socketIO.Server(server);
app.use(express.json());
let players: any = [];

const sessionMiddleware = expressSession({
    secret: "Tecky Academy teaches typescript",
    resave: true,
    saveUninitialized: true,
    cookie: { secure: false },
});

app.use(sessionMiddleware);

app.post("/login", (req, res, next) => {
    const { playerName } = req.query;
    if (playerName) {
        req.session["user"] = playerName;
        res.end(`Success`);
    } else {
        res.status(400).end("Invalid Name");
    }
});

app.post("/logout", (req, res, next) => {
    req.session["user"] = null;
    res.redirect("/login.html");
});

app.post("/private_message", (req, res) => {
    let toPlayer = req.body.toPlayer;
    let sender = req.body.sender;
    console.log("toplayer = ", toPlayer);
    console.log("sender = ", sender);

    let initPrivateMessage = messageStore.findPrivateMessages(toPlayer.id, sender.id);
    console.log("initPrivateMessage = ", initPrivateMessage);
    res.json(initPrivateMessage);
});

// type Player= {
//   userName:string
//   id:string
// }
let messageStore = new MessageStore();
io.on("connection", (socket) => {
    console.log("client connected");
    socket.onAny((event, ...args) => {
        console.log(`socket_event :[${event}]`, args);
    });
    socket.emit("version", version);
    socket.emit("left", left);
    socket.emit("right", right);
    socket.on("disconnectAll", () => {
        console.log("disconnectAll");
        io.emit("disconnectAll");
    });

    function getUserBySocketID(socketID: string) {
        return players.find((player: any) => player.id == socketID);
    }

    socket.on("disconnect", () => {
        const presentUser = getUserBySocketID(socket.id);
        players = players.filter((player: any) => player != presentUser);
        io.emit("player_left", presentUser);
        console.log(`${presentUser.username}:${presentUser.id} has left the game`);
    });

    socket.on("private_message", ({ content, to }) => {
        socket.to(to).emit("private_message", {
            content,
            from: getUserBySocketID(socket.id),
        });
        let message = {
            from: getUserBySocketID(socket.id),
            to: getUserBySocketID(to),
            content,
        };
        messageStore.saveMessage(message);
        let testFindResult = messageStore.findMessagesForUser(to);
        console.log("testFindResult = ", testFindResult);
    });

    socket.on("client_connect", (player) => {
        let playerObj = {
            id: socket.id,
            username: player,
        };
        players.push(playerObj);
        console.log(`${player}:${socket.id} has joined the room`);
        socket.emit("init_room_list", players);
        io.emit("new_player", playerObj);
    });
    socket.on("guess", (guess) => {
        console.log(socket.id);
        console.log(players);
        if (guess === answer) {
            let guessingPlayer = getUserBySocketID(socket.id);
            answer = Math.floor(Math.random() * 10000);
            left = 1;
            (right = Math.max(Math.floor(Math.random() * 10000) + 1)), right + 1;
            console.table({ left, answer, right });
            io.emit("congrats", { guessingPlayer, round });
            io.emit("left", left);
            io.emit("right", right);
            round++;
            return;
        }
        if (guess < answer) {
            if (guess <= left) {
                return;
            }
            left = guess + 1;
            io.emit("left", left);
            return;
        }
        if (guess > answer) {
            if (guess >= right) {
                return;
            }
            right = guess - 1;
            io.emit("right", right);
            return;
        }
    });
});

app.use(express.static("public"));
app.use(isLogin, express.static("protected"));
let PORT = 8888;
server.listen(PORT, () => {
    console.log(`http://localhost:${PORT}`);
});
function isLogin(req: express.Request, res: express.Response, next: express.NextFunction) {
    if (req.session["user"]) {
        next();
    } else {
        res.redirect("/login.html");
    }
}
