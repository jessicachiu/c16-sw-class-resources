import express, { Request } from "express";
import expressSession from "express-session";
import path from "path";
import { format } from "fecha";
import jsonfile from "jsonfile";
import multer from "multer";

const app = express();
const Files = {
    APPLICATIONS: path.resolve("applications.json"),
};
interface Application {
    name: string;
    age: number;
    email: string;
    birthday: string;
    createDate: string;
    profileImage?: string;
}

// // 等 express 識得睇urlencoded 既request body
app.use(express.urlencoded({ extended: true }));

// // 等 express 識得睇 json 既request body
app.use(express.json());

// Session 既config
app.use(
    expressSession({
        secret: "Tecky Academy teaches typescript",
        resave: true,
        saveUninitialized: true,
    })
);

//  Multer 既Config
//  Multer 係用黎拆  multipart form data (張form有file 果d)
//  係 app.post() 既input param ，用番佢

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        //揀你想放upload file 既位置
        cb(null, path.resolve("./uploads"));
    },
    filename: function (req, file, cb) {
        // set你upload file 既file名
        // 呢到做緊既係：file名 + 時間 + extension
        cb(
            null,
            `${file.originalname.split(".")[0]}-${Date.now()}.${
                file.originalname.split(".")[1]
            }`
        );
    },
});
const upload = multer({ storage });

/*
 ***************************************************************
 ***************************** Routes **************************
 ***************************************************************
 */

// Checking 用嘅middleware
app.use((req, res, next) => {
    // [2018-11-12 09:21:31] Request /index.js
    let dateTimeString = format(new Date(), "YYYY-MM-DD HH:mm:ss");
    let requestCounter = getRequestCounter(req);
    console.log(
        `[${requestCounter}]:[${dateTimeString}] ${req.method} ${req.path}`
    );
    console.log("\t\t[Request query : ", req.query);
    console.log("\t\t[Request params :", req.params);
    if (req.method === "POST") {
        console.log("\t\t[Request body :", req.body);
    }
    next();
});

app.post("/application", async (req, res, next) => {
    try {
        const { name, age, email, birthday } = req.body;
        const users: Application[] = await jsonfile.readFile(
            Files.APPLICATIONS
        );
        if (!name) {
            res.redirect("/?msg=名都唔寫，點幫你？");
            return;
        }
        users.push({
            name,
            age,
            email,
            birthday,
            createDate: format(new Date(), "YYYY-MM-DD HH:mm:ss"),
        });
        await jsonfile.writeFile(Files.APPLICATIONS, users, { spaces: 4 });
        console.log("張Form 無問題，我redirect番你去主頁。");
        res.redirect("/?msg=" + name + "，OK架喇，有消息再通知你");
    } catch (error) {
        console.log(error);
        res.redirect("/?msg=Post apllication有問題");
    }
});

app.post(
    "/application-multer",
    upload.single("profileImage"),
    async (req, res, next) => {
        console.log("\t\t[Request body :", req.body);
        try {
            const { name, age, email, birthday } = req.body;
            const profileImage = req.file?.filename || "";
            console.log("profileImage = ", profileImage);
            const users: Application[] = await jsonfile.readFile(
                Files.APPLICATIONS
            );
            if (!name) {
                res.redirect("/?msg=名都唔寫，點幫你？");
                return;
            }
            users.push({
                name,
                age,
                email,
                birthday,
                profileImage,
                createDate: format(new Date(), "YYYY-MM-DD HH:mm:ss"),
            });
            await jsonfile.writeFile(Files.APPLICATIONS, users, { spaces: 4 });

            res.redirect("/?msg=" + name + "，OK架喇，有消息再通知你");
        } catch (error) {
            console.log(error);
            res.redirect("/?msg=Post apllication multer 有問題");
        }
    }
);

app.post("/application/ajax/simple", async (req, res, next) => {
    try {
        const { name, age, email, birthday, profileImage } = req.body;
        const users: Application[] = await jsonfile.readFile(
            Files.APPLICATIONS
        );
        if (!name) {
            res.status(400).json({
                msg: `名都唔寫，點幫你？`,
                code: "SYS0019",
            });
            return;
        }
        users.push({
            name,
            age,
            email,
            birthday,
            profileImage,
            createDate: format(new Date(), "YYYY-MM-DD HH:mm:ss"),
        });
        await jsonfile.writeFile(Files.APPLICATIONS, users, { spaces: 4 });

        let msg: string;
        if (!profileImage) {
            msg = `${name}，OK架喇，有消息再通知你，可以試下upload埋相`;
        } else {
            msg = `${name}，OK架喇，有消息再通知你，不過記得我只save到你張相嘅名`;
        }
        res.json({
            msg,
            code: "SYS0000",
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: `post application/ajax 有問題`,
            code: "SYS0099",
        });
    }
});

app.post(
    "/application/ajax/multer",
    upload.single("profileImage"),
    async (req, res, next) => {
        console.log("\t\t[Request body :", req.body);
        try {
            const { name, age, email, birthday } = req.body;
            const profileImage = req.file?.filename || "";
            console.log("profileImage = ", profileImage);
            if (!name) {
                res.status(400).json({
                    msg: `名都唔寫，點幫你？`,
                    code: "SYS0019",
                });
                return;
            }
            const users: Application[] = await jsonfile.readFile(
                Files.APPLICATIONS
            );

            users.push({
                name,
                age,
                email,
                birthday,
                profileImage,
                createDate: format(new Date(), "YYYY-MM-DD HH:mm:ss"),
            });
            await jsonfile.writeFile(Files.APPLICATIONS, users, { spaces: 4 });

            let msg: string;
            if (!profileImage) {
                msg = `${name}，OK架喇，有消息再通知你，可以試下upload埋相`;
            } else {
                msg = `${name}，OK架喇，有消息再通知你，所有資料齊全！`;
            }
            res.json({
                msg,
                code: "SYS0000",
            });
        } catch (error) {
            console.log(error);
            res.status(500).json({
                msg: `post application/ajax 有問題`,
                code: "SYS0099",
            });
        }
    }
);

app.get("/application", async (req, res, next) => {
    const users: Application[] = await jsonfile.readFile(Files.APPLICATIONS);
    res.json(users);
});

app.delete("/application", async (req, res, next) => {
    await jsonfile.writeFile(Files.APPLICATIONS, []);
    res.json({ msg: "cleared all application" });
});

/*
 ***********************************************************
 ********************** Static Folder **********************
 ***********************************************************
 */
// 放public folder 入面既file 比入黎既request用
// uploads都可以放埋入黎 (optional)
app.use(express.static("public"));
app.use(express.static("uploads"));

/*
 ***************************************************************
 *************************** 開Server **************************
 ***************************************************************
 */

const PORT = 8888;
app.listen(PORT, () => {
    console.log(`Server 已上線 ： http://localhost:${PORT}`);
});

/*
 ***************************************************************
 ******************** Helper Function **************************
 ***************************************************************
 */

function getRequestCounter(req: Request) {
    let counter = req.session["counter"] || 0;
    counter++;
    req.session["counter"] = counter;
    return counter;
}
