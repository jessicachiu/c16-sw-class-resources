-------------------------------------- Simple Aggregation

select 
--distinct (c.name)  as cust_name,
c.name as cust_name,
ct."type" as cust_type,
count(*) as total_account_count,
sum(deposit) as deposit_total,
avg(deposit) as deposit_average,
min(deposit) as deposit_min,
max(deposit) as deposit_max,
max(account_num) as max
from customers as c
join bank_accounts ba on c.id = ba.customer_id
join customer_types ct on ct.id = c.customer_type_id
where c."name" like '%e%' 
group by (c.id, ct.type)
having sum(deposit) > 6500;

-----------------------------------------  Hard Code table
select 
--distinct (c.name)  as cust_name,
'Dickson Chan' as cust_name,
'vip' as cust_type,
10 as total_account_count
from customers as c limit 1;


------------------------------------------- UNION
select 
--distinct (c.name)  as cust_name,
c.name as cust_name,
ct."type" as cust_type,
count(*) as total_account_count,
sum(deposit) as deposit_total,
avg(deposit) as deposit_average,
min(deposit) as deposit_min,
max(deposit) as deposit_max,
max(account_num) as max
from customers as c
join bank_accounts ba on c.id = ba.customer_id
join customer_types ct on ct.id = c.customer_type_id
where c."name" like '%e%' 
group by (c.id, ct.type)
having sum(deposit) > 6500;

UNION

select 
--distinct (c.name)  as cust_name,
'Dickson Chan' as cust_name,
'vip' as cust_type,
10 as total_account_count
from customers as c limit 1;

------------------------------------------------ WITH
with real_data_set as (
select 
--distinct (c.name)  as cust_name,
c.name as cust_name,
ct."type" as cust_type,
count(*) as total_account_count
--sum(deposit) as deposit_total,
--avg(deposit) as deposit_average,
--min(deposit) as deposit_min,
--max(deposit) as deposit_max,
--max(account_num) as max
from customers as c
join bank_accounts ba on c.id = ba.customer_id
join customer_types ct on ct.id = c.customer_type_id
where c."name" like '%e%' 
group by (c.id, ct.type)
--having sum(deposit) > 6500
),
hard_code_data_set as (
select 
--distinct (c.name)  as cust_name,
'Dickson Chan' as cust_name,
'vip' as cust_type,
10 as total_account_count
from customers as c limit 1
)

select * from real_data_set 
union 
select * from hard_code_data_set;

----------------------------------------------------- Transaction
begin;

 SET TRANSACTION ISOLATION LEVEL Repeatable Read;
 
 select * from bank_accounts ba where id = 1;
 update bank_accounts set deposit = deposit - 100  where id = 1;

commit;