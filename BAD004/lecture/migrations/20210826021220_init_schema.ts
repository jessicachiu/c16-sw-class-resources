import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable("customer_types", (table) => {
        table.increments();
        table.string("type");
        table.string("description");
        // table.timestamps(false, true);
    });
    await knex.schema.createTable("customers", (table) => {
        table.increments();
        table.string("name");
        table.integer("customer_type_id").unsigned();
        table.foreign("customer_type_id").references("customer_types.id");
    });
    await knex.schema.createTable("bank_accounts", (table) => {
        table.increments();
        table.string("account_num");
        table.decimal("deposit");
        table.integer('customer_id')
        table.foreign('customer_id').references('customers_id')
    });
    
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable("customers");
    await knex.schema.dropTable("bank_accounts");
    await knex.schema.dropTable("customer_types");
}
