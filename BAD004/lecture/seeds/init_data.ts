import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("customers").del();
    await knex("bank_accounts").del();
    await knex("customer_types").del();

    // Inserts seed entries
    
    const customerTypesIDs = await knex("customer_types").insert([
        { type: "normal", description:'AAAAA' },
        { type: "new", description:'BBBB' },
        { type: "vip", description:'CCCC' },
        { type: "rich", description:'DDDD' },
    ]).returning('id');

    const customerIDs = await knex("customers").insert([
        { name: "Ben Chan", customer_type_id:customerTypesIDs[0] },
        { name: "Ricky Cheung", customer_type_id:customerTypesIDs[0] },
        { name: "Tommy Leung", customer_type_id:customerTypesIDs[1] },
        { name: "May Fung", customer_type_id:customerTypesIDs[2] },
    ]).returning('id');

    await knex("bank_accounts").insert([
        { account_num: "8745827465827", deposit:3200.00, customer_id:customerIDs[0] },
        { account_num: "2435432532777", deposit:3200.00, customer_id:customerIDs[1] },
        { account_num: "56353t2415435", deposit:3200.00, customer_id:customerIDs[1] },
        { account_num: "5635655324243", deposit:23200.00, customer_id:customerIDs[2] },
        { account_num: "6664453443324", deposit:3000.00, customer_id:customerIDs[2] },
        { account_num: "4536562535555", deposit:33200.00, customer_id:customerIDs[2] },
        { account_num: "1232131244444", deposit:23200.00, customer_id:customerIDs[3] },
        { account_num: "4443352555551", deposit:23200.00, customer_id:customerIDs[3] },
    ]);

}
