-- Exported from QuickDBD: https://www.quickdatabasediagrams.com/
-- NOTE! If you have used non-SQL datatypes in your design, you will have to change these here.

-- Modify this code to update the DB schema diagram.
-- To reset the sample schema, replace everything with
-- two dots ('..' - without quotes).

CREATE TABLE "customers" (
    "id" int   NOT NULL,
    "name" string   NOT NULL,
    "customer_type_id" int   NOT NULL,
    CONSTRAINT "pk_customers" PRIMARY KEY (
        "id"
     )
);

CREATE TABLE "bank_accounts" (
    "id" int   NOT NULL,
    "account_num" string   NOT NULL,
    "deposit" decimal   NOT NULL,
    "customer_id" int   NOT NULL,
    CONSTRAINT "pk_bank_accounts" PRIMARY KEY (
        "id"
     )
);

CREATE TABLE "customer_types" (
    "id" int   NOT NULL,
    "type" string   NOT NULL,
    "description" string   NOT NULL,
    CONSTRAINT "pk_customer_types" PRIMARY KEY (
        "id"
     )
);

ALTER TABLE "customers" ADD CONSTRAINT "fk_customers_customer_type_id" FOREIGN KEY("customer_type_id")
REFERENCES "customer_types" ("id");

ALTER TABLE "bank_accounts" ADD CONSTRAINT "fk_bank_accounts_customer_id" FOREIGN KEY("customer_id")
REFERENCES "customers" ("id");

CREATE INDEX "idx_customers_name"
ON "customers" ("name");

