//spread operator
let jsonObj={
    name:"Tecky",
    year:3
}
console.log(jsonObj);
let newJSONObj ={
    ...jsonObj,
    name:"Ann",
    month:1
}
console.log(newJSONObj);

//[keyName]
let keyName= "name";
let newJSONObj2 = {
    ...newJSONObj,
    [keyName]:'Peter'
}
console.log(newJSONObj2);