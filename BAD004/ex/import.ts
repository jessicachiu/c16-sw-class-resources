import Knex from 'knex';
const KnexConfig = require("./knexfile");
const knex = Knex(KnexConfig[process.env.NODE_ENV||"development"]);

import XLSX from 'xlsx';

const main = async()=>{
    //Get Data from Excel file
    const workbook = XLSX.readFile("BAD004-exercise.xlsx");
    
    const userWS = workbook.Sheets["user"];
    const userJSON = XLSX.utils.sheet_to_json(userWS);

    const categoryWS = workbook.Sheets["category"];
    const categoryJSON = XLSX.utils.sheet_to_json(categoryWS);

    const fileWS = workbook.Sheets["file"];
    const fileJSON = XLSX.utils.sheet_to_json(fileWS);

    //Update Owner if wrong
    const correctFileJSON = fileJSON.map((file:any)=>{
        if (file.owner === "alexs"){
            file.owner = "alex";
        }else if (file.owner === "admiin"){
            file.owner = "admin";
        }else if (file.owner === "ales"){
            file.owner = "alex";
        }else if (file.owner === "micheal"){
            file.owner = "michael";
        }
        return file;
    });

    //Add to Database
    const userTable = await knex("user").insert(userJSON).returning("*");
    const categoryTable = await knex("category").insert(categoryJSON).returning("*");
    
    const userMap = userTable.reduce((acc,user)=>{
        return {
            ...acc,
            [user.username]:user.id
        }
    },{});
    const categoryMap = categoryTable.reduce((acc,category)=>{
        return {
            ...acc,
            [category.name]:category.id
        }
    },{});
    const updatedFileJSON = correctFileJSON.map(file=>{
        file.owner = userMap[file.owner]
        file.category = categoryMap[file.category];
        return file;
    });
    const fileTable = await knex("file").insert(updatedFileJSON).returning("*");
    console.log(fileTable.length)

    /*
    [
  { id: 1, username: 'admin', password: 'admin', level: 'admin' },
  { id: 2, username: 'alex', password: '123456', level: 'staff' },
  { id: 3, username: 'gordon', password: '456789', level: 'staff' },
  { id: 4, username: 'michael', password: '098776', level: 'staff' }
]
=> {
    admin:1,
    alex:2,
    gordon:3,
    michael:4
}
[
  { id: 1, name: 'Important' },
  { id: 2, name: 'Urgent' },
  { id: 3, name: 'Useful' },
  { id: 4, name: 'Not Urgent' },
  { id: 5, name: 'Not Important' }
]
=>
{
    Important:1,
    Urgent:2,
    Useful:3,
    'Not Urgent':4,
    'Not Important':5
}
    */

    await knex.destroy();
}
main();