import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable("user",(table)=>{
        table.increments();
        table.string("username").notNullable();
        table.string("password").notNullable();
        table.string("level").notNullable();
    });

    await knex.schema.createTable("category",(table)=>{
        table.increments();
        table.string("name").notNullable();
    });
    
    await knex.schema.createTable("file",(table)=>{
        table.increments();
        table.string("name").notNullable();
        table.text("Content");
        table.integer("is_file").defaultTo(0);
        table.integer("category").unsigned();
        table.foreign("category").references("category.id");
        table.integer("owner").unsigned();
        table.foreign("owner").references("user.id");
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists("file");
    await knex.schema.dropTableIfExists("category");
    await knex.schema.dropTableIfExists("user");
}

