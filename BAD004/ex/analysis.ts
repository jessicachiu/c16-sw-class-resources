/*
select owner,count(name) from file where is_file=1 group by owner;
select category,count(name) from file where is_file=1 group by category
select owner, count(name) from files where owner=2 and category=1 and is_file=1 group by owner
select owner,count(name) from file where is_file=1 group by owner having count(name) > 800;

select * 
from (elect owner,count(name) as total from file where is_file=1 group by owner)
where total > 800
*/
import Knex from 'knex';
const KnexConfig = require("./knexfile");
const knex = Knex(KnexConfig[process.env.NODE_ENV||"development"]);


const main = async()=>{
    const q1 = await knex("file").select("owner").count("name").where("is_file",1).groupBy("owner");
    console.table(q1);
    
    const q2 = await knex("file").select("category").count("name").where("is_file",1).groupBy("category");
    console.table(q2);

    const q3 = await knex("file").select("owner").count("name").where("is_file",1).andWhere("owner","2").andWhere("category",1).groupBy("owner");
    console.table(q3);

    const q4 = await knex("file")
        .select("owner")
        .count("name")
        .where("is_file",1)
        .groupBy("owner")
        .having(knex.raw(`count(name) > 530`));
    console.table(q4);
    await knex.destroy();
}
main();