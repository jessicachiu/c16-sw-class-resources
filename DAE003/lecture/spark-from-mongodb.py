#%%
# Find out where the spark installation is
import findspark
# 唔好抄我，係要改返你自己部機嘅果個spark folder所在
findspark.init('/home/gordon/Codes/spark')

# %%
# Initialize the session

from pyspark.sql import SparkSession

spark = SparkSession.builder.appName("Read from mongo").getOrCreate()



# %%
# Extract data from MongoDB
df = spark.read.format('mongo').option('spark.mongodb.input.uri','mongodb://127.0.0.1/wikipedia.pagecreate').load();
# %%
# Count how many users are in zh.wikipedia.org
# Transform
from pyspark.sql.functions import col
# Lazy Execution ，即係係呢一剎那，乜都未做
count_df = df.where(col('domain') == 'zh.wikipedia.org').groupBy('user_text').count()

# %%
# 去到讀嘅時候，先會做嘢
# 你只要setup 咗個Spark Cluster，你可以處理比你部機個Ram更大嘅data
count_df.show()


#%%
# Dataframe可以做嘅嘢

df.select('domain','comment').where(col('domain') == "ru.wikipedia.org").sort('comment').show()
# %%
from pyspark.sql.functions import *
afinn_df = spark.read.format('csv').option('delimiter','\t').load('/home/gordon/Codes/spark/data/streaming/AFINN-111.txt')
afinn_df.sort(desc('_c0')).show()

# %%
# Dataframe 變成Memory中的temp table 
df.registerTempTable('pagecreate')

# %%
spark.sql("SELECT * from pagecreate").show()

# %%
# Add a column called processed date
import pyspark.sql.functions as F;
import time

df = df.withColumn("processed_date",F.to_timestamp(lit(time.time())))


# %%
df = df.drop('_id')


# Load into output format
df.where(col("domain")== 'zh.wikipedia.org').write.format('parquet').save('./pagecreate.parquet')


# %%

zh_only_df = spark.read.format('parquet').load('./pagecreate.parquet')
# %%
# Map operation

zh_only_df.rdd.map( lambda row: {"new_user_text": row.user_text + "123" }).toDF().show()


# %%
# Dataframe is immutable 
zh_only_df.show()
# %%
