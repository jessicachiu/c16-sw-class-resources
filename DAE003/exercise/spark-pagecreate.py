#!/usr/bin/env python
# coding: utf-8

# In[1]:


# Find out where the spark installation is
import findspark
# 唔好抄我，係要改返你自己部機嘅果個spark folder所在
findspark.init('/home/gordon/Codes/spark')


# In[2]:


from pyspark.sql import SparkSession

# 呢個可以取代  spark-defaults.conf 果個嘅configuration
packages = [
    "org.apache.hadoop:hadoop-aws:3.2.0",
    "org.apache.spark:spark-avro_2.12:2.4.4",
    "org.mongodb.spark:mongo-spark-connector_2.12:3.0.1"
]

spark = SparkSession.builder.appName("Read from mongo").config("spark.jar.packages",",".join(packages))            .getOrCreate()


# # Extract (Spark Read from MongoDB)
# spark.read.format("mongo")

# In[ ]:



df = spark.read.format('mongo').option('spark.mongodb.input.uri','mongodb://127.0.0.1/wikipedia.pagecreate').load()
df.show()


# In[13]:


df.printSchema()


# # Transform (Aggregation)

# In[36]:


q2_df = df.where(df.user_is_bot == False ).where(df.domain == "zh.wikipedia.org")
q2_df.show()

# Another version
# from pyspark.sql.functions import col
# df.where(df.user_is_bot == False ).where(col('domain') == "zh.wikipedia.org").show()


# In[21]:


from pyspark.sql.functions import desc,col
q3_df = df.where(df.user_is_bot == False).groupBy('user_text').count()
q3_df = q3_df.sort(col('count').desc()).limit(1)


# In[38]:


q4_df = df.where(df.user_is_bot == False).groupBy('user_text').sum("rev_len")
q4_df = q4_df.sort(col('sum(rev_len)').desc()).limit(1)
q4_df = q4_df.withColumnRenamed('sum(rev_len)','sum_rev_len')


# In[31]:


q5_df = df.groupBy('domain').count().sort(col('count').desc()).limit(3)
q5_df = spark.createDataFrame(q5_df.collect()[0::2])


# In[34]:


df.registerTempTable('pagecreate');

q6_df = spark.sql("SELECT * from pagecreate WHERE comment like '%edit%' ")
q6_df.select('comment').show()


# In[40]:


q2_df.write.format('parquet').save('./q2.parquet',mode="overwrite")
q3_df.write.format('parquet').save('./q3.parquet',mode="overwrite")
q4_df.write.format('parquet').save('./q4.parquet',mode="overwrite")
q5_df.write.format('avro').save('./q5.avro',mode="overwrite")
q6_df.write.format('avro').save('./q6.avro',mode="overwrite")

