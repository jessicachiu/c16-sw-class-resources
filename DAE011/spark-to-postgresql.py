
import os 
from dotenv import load_dotenv

load_dotenv()
DB_NAME=os.getenv('DB_NAME')
DB_USERNAME=os.getenv('DB_USERNAME')
DB_PASSWORD=os.getenv('DB_PASSWORD')
DB_HOST = os.getenv('DB_HOST') or 'localhost'

AWS_ACCESS_KEY=os.getenv('AWS_ACCESS_KEY')
AWS_SECRET_KEY=os.getenv('AWS_SECRET_KEY')

#%% 
# 你部laptop 要裝返pyspark，令我唔洗用findspark，都可以自己開個spark session
from pyspark.sql import SparkSession

packages = [
    "com.amazonaws:aws-java-sdk-s3:1.12.79",
    "org.apache.hadoop:hadoop-aws:3.2.0",
    "org.apache.spark:spark-avro_2.12:2.4.4",
    "org.mongodb.spark:mongo-spark-connector_2.12:3.0.1"
]

spark = SparkSession.builder.appName("Read from mongo")\
        .master('spark://localhost:7077')\
        .config("spark.jars.packages",",".join(packages))\
        .config("spark.hadoop.fs.s3a.access.key",AWS_ACCESS_KEY)\
        .config("spark.hadoop.fs.s3a.secret.key",AWS_SECRET_KEY)\
        .config("spark.hadoop.fs.s3a.impl","org.apache.hadoop.fs.s3a.S3AFileSystem")\
        .config("spark.hadoop.fs.s3a.multipart.size",104857600)\
        .getOrCreate()


# # Read data from mongo
# 呢個位你要小心用返MongoDB 個ip
df = spark.read.format('mongo').option('spark.mongodb.input.uri','mongodb://172.1.0.10:27017/wikipedia.pagecreate').load();
df.show()

from pyspark.sql.functions import col,when,substring
df = df.withColumn('user_edit_count', col('user_edit_count').cast('int'))
df = df.withColumn('user_registration_dt',when(col('user_registration_dt') != '' ,col('user_registration_dt')).otherwise(None))
df = df.withColumn('year',substring('rev_timestamp',1,4))\
            .withColumn('month',substring('rev_timestamp',6,2))\
            .withColumn('day',substring('rev_timestamp',9,2))\
            .withColumn('hour',substring('rev_timestamp',12,2))

# # Insert users data 

def insert_data(row):
    # 係worker 入面考，無嘅話就會炒
    import psycopg2 as psycopg
    # 直上Amazon RDS 嘅Data Warehouse
    conn = psycopg.connect(dbname=DB_NAME,user=DB_USERNAME,password=DB_PASSWORD,host=DB_HOST)
    conn.autocommit = True
    # Open a cursor to perform database operations

    cur = conn.cursor()
    insert_user_sql = "insert into users (user_text,user_is_bot,user_registration_dt,user_edit_count)\
        values(%(user_text)s,%(user_is_bot)s,%(user_registration_dt)s,%(user_edit_count)s)\
        on conflict(user_text) do update SET user_edit_count = %(user_edit_count)s returning id"
    cur.execute(insert_user_sql,row)
    insert_page_sql = "insert into pages (page_id,page_title,url,comment,parsedcomment,rev_len,user_id)\
        values(%(page_id)s,%(page_title)s, %(uri)s, %(comment)s, %(parsedcomment)s,%(rev_len)s,\
        (select id from users where user_text = %(user_text)s limit 1))\
        on conflict(page_id) do update set comment = %(comment)s returning id"
    cur.execute(insert_page_sql,row)
    return (True)       
         

df.rdd.foreach(insert_data)

