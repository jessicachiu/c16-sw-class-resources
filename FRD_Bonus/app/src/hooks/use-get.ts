import { useState, useEffect } from 'react'
import { useStorageState } from 'react-use-storage-state'

export function useGet<T>(url: string, defaultValue: T) {
  // const [cache, setCache] = useStorageState(url, defaultValue)
  const [data, setData] = useStorageState<T | { error: string }>(
    url,
    defaultValue,
  )

  async function loadData() {
    try {
      let res = await fetch('http://127.0.0.1:8100' + url, {
        headers: {
          Authorization: 'Bearer ' + localStorage.getItem('token'),
        },
      })
      let json = await res.json()
      setData(json)
    } catch (error) {
      // let json = {
      //   error: (error as Error).toString(),
      // }
      // setData(json)
    }
  }

  useEffect(() => {
    loadData()
  }, [url])

  return [data, loadData] as const
}
