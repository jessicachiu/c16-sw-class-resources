import {
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonIcon,
  IonItem,
  IonLabel,
  IonList,
  IonPage,
  IonText,
  IonTitle,
  IonToolbar,
} from '@ionic/react'

function GetWrapper<T>(props: {
  data: T | { error: string }
  render: (data: T) => JSX.Element
}) {
  const data = props.data
  if ('error' in data) {
    return <IonText color="danger">{data.error}</IonText>
  } else {
    return props.render(data)
  }
}

export default GetWrapper
