import {
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonIcon,
  IonPage,
  IonTitle,
  IonToolbar,
} from '@ionic/react'
import { refresh } from 'ionicons/icons'
import ExploreContainer from '../components/ExploreContainer'
import { useGet } from '../hooks/use-get'
import './Tab2.css'
import { Data } from './types'

const Tab2: React.FC = () => {
  const [data, reload] = useGet<Data>('/icons/outline', { iconList: [] })
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Tab 2</IonTitle>
          <IonButtons slot="end">
            <IonButton onClick={reload}>
              <IonIcon icon={refresh}></IonIcon>
            </IonButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Tab 2</IonTitle>
          </IonToolbar>
        </IonHeader>
        {JSON.stringify(data)}
      </IonContent>
    </IonPage>
  )
}

export default Tab2
