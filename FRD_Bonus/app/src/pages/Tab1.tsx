import {
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonIcon,
  IonItem,
  IonLabel,
  IonList,
  IonPage,
  IonText,
  IonTitle,
  IonToolbar,
} from '@ionic/react'
import { refresh } from 'ionicons/icons'
import { useEffect, useState } from 'react'
import ExploreContainer from '../components/ExploreContainer'
import './Tab1.css'
import { useGet } from '../hooks/use-get'
import GetWrapper from '../components/GetWrapper'
import { Data } from './types'

const Tab1: React.FC = () => {
  const [data, reload] = useGet<Data>('/icons/normal', { iconList: [] })

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Tab 1</IonTitle>
          <IonButtons slot="end">
            <IonButton onClick={reload}>
              <IonIcon icon={refresh}></IonIcon>
            </IonButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding">
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Tab 1</IonTitle>
          </IonToolbar>
        </IonHeader>

        <GetWrapper
          data={data}
          render={data => (
            <>
              <p>Total: {data.iconList.length}</p>

              <IonList>
                {data.iconList.map(icon => (
                  <IonItem key={icon.name}>
                    <IonIcon icon={icon.icon} slot="start"></IonIcon>
                    <IonLabel>{icon.name}</IonLabel>
                  </IonItem>
                ))}
              </IonList>
            </>
          )}
        />
      </IonContent>
    </IonPage>
  )
}

export default Tab1
