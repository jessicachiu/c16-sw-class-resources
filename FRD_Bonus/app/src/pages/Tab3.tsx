import {
  IonAvatar,
  IonButton,
  IonButtons,
  IonCard,
  IonContent,
  IonHeader,
  IonIcon,
  IonPage,
  IonTitle,
  IonToolbar,
} from '@ionic/react'
import { refresh } from 'ionicons/icons'
import ExploreContainer from '../components/ExploreContainer'
import GetWrapper from '../components/GetWrapper'
import { useGet } from '../hooks/use-get'
import './Tab3.css'
import { Data } from './types'

const Tab3: React.FC = () => {
  const [data, reload] = useGet<Data>('/icons/sharp', { iconList: [] })
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Tab 3</IonTitle>
          <IonButtons slot="end">
            <IonButton onClick={reload}>
              <IonIcon icon={refresh}></IonIcon>
            </IonButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Tab 3</IonTitle>
          </IonToolbar>
        </IonHeader>

        <GetWrapper
          data={data}
          render={data => (
            <>
              {data.iconList.map(icon => (
                <IonCard key={icon.name}>
                  <IonAvatar>
                    <img src={icon.icon} />
                  </IonAvatar>
                  <p>{icon.name}</p>
                </IonCard>
              ))}
            </>
          )}
        />
      </IonContent>
    </IonPage>
  )
}

export default Tab3
