export type Icon = {
  name: string
  icon: string
}

export type Data =
  | {
      iconList: Icon[]
    }
  | {
      error: string
    }
