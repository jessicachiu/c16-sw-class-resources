import express from 'express'
import icons from 'ionicons/icons'

export let router = express.Router()

let iconList: Array<{
  name: string
  icon: string
}> = []
for (let name in icons) {
  let icon = (icons as any)[name]
  iconList.push({ name, icon })
}

router.get('/icons/normal', (req, res) => {
  if (Math.random() < 0.5) {
    res.status(400).json({ error: 'No luck, try again' })
  } else {
    res.json({
      iconList: iconList.filter(
        icon => !(icon.name.endsWith('Outline') || icon.name.endsWith('Sharp')),
      ),
    })
  }
})

router.get('/icons/outline', (req, res) => {
  if (Math.random() < 0.5) {
    res.status(400).json({ error: 'No luck, try again' })
  } else {
    res.json({
      iconList: iconList.filter(icon => icon.name.endsWith('Outline')),
    })
  }
})

router.get('/icons/sharp', (req, res) => {
  if (Math.random() < 0.5) {
    res.status(400).json({ error: 'No luck, try again' })
  } else {
    res.json({ iconList: iconList.filter(icon => icon.name.endsWith('Sharp')) })
  }
})

router.use((req, res) => {
  res
    .status(404)
    .json({ error: `Route not found, Method: ${req.method}, Url: ${req.url}` })
})
