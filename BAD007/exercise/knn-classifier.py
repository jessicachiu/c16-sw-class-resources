#%%
# import
import numpy as np
from scipy.stats  import mode
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report


#%%
# Load data
iris = load_iris()
X = iris.data
y = iris.target

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)


print(X_train.shape)    # (120, 4)
print(X_test.shape)     # (30, 4)
print(y_train.shape)    # (120,)
print(y_test.shape)     # (30,)

#%%
# KNNClassifier class
class KNNClassifier:

    def __init__(self, X_train, y_train, n_neighbors = 3):
        self.n_neighbors = n_neighbors
        self._X_train = X_train
        self._y_train = y_train

    """calculate the euclidean distance here"""
    def euclidean_dist(self, point_1, point_2):
        return np.sqrt( 
                np.sum((point_1 - point_2)**2)
            )

    """accept multiple inputs here and predict one by one with predict_single()"""
    def predict(self, X_test_array):
        result = []
        for x_test in X_test_array:
            result.append(self.predict_single(x_test))
        return result

    """predict single input here"""
    def predict_single(self, input_data_single):
        distances = []
        for index,x_train in enumerate(X_train):
            distances.append((self.euclidean_dist(
                x_train,input_data_single),index))
        closest_n = sorted(distances)[0:self.n_neighbors]
        classes = []
        for x_data in closest_n:
            y_data = self._y_train[x_data[1]]
            classes.append(y_data)
        return mode(classes).mode[0]

classifier = KNNClassifier(X_train,y_train,10)

predict_result =classifier.predict(np.array([
    [1,2,3,4],
    [3,3,3,3],
    [2,1,2,1],
])
)

print(predict_result)


#%%
# Classification
# Written by ourselves
iris_knn_classifier = KNNClassifier(X_train, y_train)
y_pred = iris_knn_classifier.predict(X_test)
print(classification_report(y_test, y_pred, target_names=[
      'Iris-Setosa', 'Iris-Versicolour', 'Iris-Virginica']))
# %%
# Use built-in KNN Classifier
from sklearn.neighbors import KNeighborsClassifier
iris_knn_classifier = KNeighborsClassifier(n_neighbors=3)
iris_knn_classifier.fit(X_train,y_train)
y_pred = iris_knn_classifier.predict(X_test)
print(classification_report(y_test, y_pred, target_names=[
      'Iris-Setosa', 'Iris-Versicolour', 'Iris-Virginica']))
# %%
