const peter = {
    name: "Peter",
    age: 50,
    students:[
       { name:"Andy", age:20,exercises:[
          {score: 60, date: new Date("2018-10-01")}
       ]},
       { name:"Bob", age:23,exercises:[
          {score: 76 ,date: new Date("2019-01-05")},
          {score: 55 ,date: new Date("2018-11-05")}
       ]},
       {name: "Charlie", age:25 , exercises:[
           { score: 60 , date: new Date("2019-01-05") }
       ]}
    ]
}
const penny = {
    name: "Peter",
    age: 50,
    students: [
      { name: "Alex", age: 20 },
      {
        name: "Ben",
        age: 25,
        exercises: [{ score: 60, date: new Date("2019-01-05") }],
      },
    ],
  };

// Case I : Traversing nested object and arrays
for(const student of peter.students){
    console.log(`Student ${student.name} is ${student.age} years old`)
}

for(const student of peter.students){
    if (student.exercises){
        for (const exercise of student.exercises){
            console.log(`Student ${student.name} has a exercise of  score ${exercise.score} submitted on ${exercise.date}`);
        }
    }
    
}


//Case II: Find something by a condition from an array

let studentFound;
let isStudentFound = false
for(let student of peter.students){
    if(student.name === "Andy"){
       studentFound = student;
       isStudentFound = true
    }
}

if(isStudentFound){
    //some more logic
}

// Case III: Factoring repetitive code

function findByStudentName(teacher, name){
    for(let student of teacher.students){
        if(student.name === name){
           return student
        }
    }
}
findByStudentName(peter,'Charlie')

//Case IV: Use an object to "collect" values from an array

const exercises = [];
for(let student of peter.students){
    for(let exercise of student.exercises){
       exercises.push(exercise);
    }
}