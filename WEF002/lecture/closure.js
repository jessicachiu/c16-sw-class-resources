function getNumber(){
    return 5
}
let myNum = getNumber()


// ---------------------------- //

function getNumberFunction(){
    return function(){
        return 5
    }
}
let myNumFunction = getNumberFunction()
//  myNumFunction()  ->> get 5

// ---------------------------- //

function getNumberFunctionV2(){
    let num = 15
    return function(){
        num = num + input
        return num
    }
}
let myNumFunctionV2a = getNumberFunctionV2()
let myNumFunctionV2b = getNumberFunctionV2()

// ---------------------------- //

function getCalculator(){
    let num = 0
    return {
        getNumber:() => num,
        add:function(input){
            num = num + input
            return num
        },
        minus:function(input){
            num = num - input
            return num
        },
        times:function(input){
            num = num * input
            return num
        },
        divide:input=>{
            num = num / input
            return num
        },
        reset:()=>{
            num = 0
            return num
        }
    }
}


let calculator = getCalculator()