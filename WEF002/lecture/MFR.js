// Join
let joinResult = ["John","Peter","Ben","Andy"].join(",")

// Split
let splitResult1 = "ABCDEFG".split('')
let splitResult2 = "A,B,C,D,E,F,G".split(',')

// Reverse
let reverseResult = ["John","Peter","Ben","Andy"].reverse()



const numbers = [1,2,3,4,5,6,7,8,9,10]
// Map  ->> return loopItem
let result = []
for(let number of numbers){
    result.push(number **2);
}

let mapResult = numbers.map(function(number){
    if (number > 5){
        number = number + 1 
    }
    return number
})

// Filter  -->> reutn true / false

result = []
for (const number of numbers){
    if (number % 2 === 0 ){
        result.push(number)
    }
}

let filterResult = numbers.filter(function(number){
    if (number % 2 === 0 ){
        return true
    }else{
        return false
    }
})

// Reduce  ---> return acc

let reduceResult = numbers.reduce(function(acc,number ){
    if (number < 5 ){
        acc = acc + number
    }    
    return acc
},0)
