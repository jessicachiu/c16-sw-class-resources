const data = [
    {
        "name": "Hong Kong",
        "topLevelDomain": [
            ".hk"
        ],
        "alpha2Code": "HK",
        "alpha3Code": "HKG",
        "callingCodes": [
            "852"
        ],
        "capital": "City of Victoria",
        "altSpellings": [
            "HK",
            "香港"
        ],
        "region": "Asia",
        "subregion": "Eastern Asia",
        "population": 7324300,
        "latlng": [
            22.25,
            114.16666666
        ],
        "demonym": "Chinese",
        "area": 1104.0,
        "gini": 53.3,
        "timezones": [
            "UTC+08:00"
        ],
        "borders": [
            "CHN"
        ],
        "nativeName": "香港",
        "numericCode": "344",
        "currencies": [
            {
                "code": "HKD",
                "name": "Hong Kong dollar",
                "symbol": "$"
            }
        ],
        "languages": [
            {
                "iso639_1": "en",
                "iso639_2": "eng",
                "name": "English",
                "nativeName": "English"
            },
            {
                "iso639_1": "zh",
                "iso639_2": "zho",
                "name": "Chinese",
                "nativeName": "中文 (Zhōngwén)"
            }
        ],
        "translations": {
            "de": "Hong Kong",
            "es": "Hong Kong",
            "fr": "Hong Kong",
            "ja": "香港",
            "it": "Hong Kong",
            "br": "Hong Kong",
            "pt": "Hong Kong",
            "nl": "Hongkong",
            "hr": "Hong Kong",
            "fa": "هنگ‌کنگ"
        },
        "flag": "https://restcountries.eu/data/hkg.svg",
        "regionalBlocs": [],
        "cioc": "HKG"
    }
]

const hkData = data[0]


// Method 1
// console.log('Name :' + hkData.name)


// Method 2


// for (const hkDataKey in hkData){
//     const preffix = hkDataKey.charAt(0).toUpperCase() + hkDataKey.slice(1);
//     if (hkDataKey == 'currencies'){
//         for (const currency of hkData[hkDataKey]){
//             for (const currencyKey in currency){
//                 console.log(`${preffix}_${currencyKey}: ${currency[currencyKey]}`);
//             }
//         }

//     }else if (hkDataKey == 'languages'){
//         for (const language of hkData[hkDataKey]){
//             for (const languageKey in language){
//                 console.log(`${preffix}_${languageKey}: ${language[languageKey]}`);
//             }
//         }

//     }else if (hkDataKey == 'translations'){
//         for (const translationKey in hkData[hkDataKey]){
//             console.log(`${preffix}_${translationKey}: ${hkData[hkDataKey][translationKey]}`);
//         }
//     }else {
//         console.log(preffix +": "+ hkData[hkDataKey] );
//     }
// }

// Method 3


for (const hkDataKey in hkData){
    const preffix = hkDataKey.charAt(0).toUpperCase() + hkDataKey.slice(1);
    const hkDataVal = hkData[hkDataKey]
    if(hkDataVal instanceof Array){
        const arrData = hkDataVal
        if(!(arrData[0] instanceof Object)){
            console.log(preffix +": "+ hkDataVal );
            continue
        }

        // Object handling
        for (const arrItem of arrData){
                for (const objectKey in arrItem){
                    console.log(`${preffix}_${objectKey}: ${arrItem[objectKey]}`);
                }

        }
        
    }else if (hkDataVal instanceof Object){
        for (const objectKey in hkDataVal){
            console.log(`${preffix}_${objectKey}: ${hkDataVal[objectKey]}`);
        }
    }else {
        console.log(preffix +": "+ hkDataVal );
    }
}