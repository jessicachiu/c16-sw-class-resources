const cards = [
    ['Spade','A'],
    ['Diamond','J'],
    ['Club','3'],
    ['Heart','6'],
    ['Spade','K'],
    ['Club','2'],
    ['Heart','Q'],
    ['Spade','6'],
    ['Heart','J'],
    ['Spade','10'],
    ['Club','4'],
    ['Diamond','Q'],
    ['Diamond','3'],
    ['Heart','4'],
    ['Club','7']
];

// Count the number of card which is of suit Spade.(Hints:using reduce)
    let countOfSpad = 0 
    for (const card of cards){
        if(card[0] === 'Spade' ){
            countOfSpad = countOfSpad +1
        }
    }
    
    countOfSpad = cards.reduce(function(acc, card){
        if(card[0] === 'Spade' ){
            acc = acc + 1
        }
        return acc
    },0)

    console.log('countOfSpad = ', countOfSpad)

// Remove all the card that is smaller than ['Club','3'] .
    const rankOrder = ["2","3","4","5","6","7","8","9","10","J","Q","K","A"];
    const suitOrder = ["Diamond","Club","Heart","Spade"];

    function getRankIndex(rank){
        return rankOrder.indexOf(rank) 
    }
    function getSuitIndex(suit){
        return suitOrder.indexOf(suit) 
    }

    let greaterThanClub3 = []
    for (const card of cards){
        const [cardSuit,cardRank] = card;
        if(  getRankIndex(cardRank) >= getRankIndex("3")   ||  
             (getRankIndex(cardRank) === getRankIndex("3") && getSuitIndex(cardSuit) >  getSuitIndex('Club'))
             ){
            greaterThanClub3.push(card)
        }
    }

    greaterThanClub3 = cards.filter((card)=>{
        const [cardSuit,cardRank] = card;
        if(  getRankIndex(cardRank) >= getRankIndex("3")   ||  
             (getRankIndex(cardRank) === getRankIndex("3") && getSuitIndex(cardSuit) >  getSuitIndex('Club'))
             ){
            return true
        }else{
            return false
        }
        // return true/ false
    })

    console.log('greaterThanClub3 = ', greaterThanClub3);
    // Count the number of card which is of suit Diamond or Heart and with the rank larger than or equal to J.

    let countSpecialFiltered = 0 

    for(const card of cards){
        const [cardSuit,cardRank] = card;
        if((cardSuit === 'Diamond' || cardSuit === 'Heart') && getRankIndex(cardRank) >= getRankIndex("J")){
            countSpecialFiltered = countSpecialFiltered + 1
        }
    }

    countSpecialFiltered = cards.reduce(function(acc, card){
        const [cardSuit,cardRank] = card;
        if((cardSuit === 'Diamond' || cardSuit === 'Heart') && getRankIndex(cardRank) >= getRankIndex("J")){
            acc = acc + 1
        }
        return acc
    },0)


    console.log('countSpecialFiltered = ', countSpecialFiltered)
// Replace all of the cards with suit Club to suit Diamond, keeping the same rank.
    let  replacedByDimondCards = cards

    /*
        Will update original stack
    */
    // for(const card of replacedByDimondCards){
    //     const [cardSuit,cardRank] = card;
    //     if(cardSuit === 'Club'){
    //         card[0] = 'Diamond'
    //     }
    // }
    
    replacedByDimondCards = cards.map(function(card){
        const [cardSuit,cardRank] = card;
        if(cardSuit === 'Club'){
            // card[0] = 'Diamond'
            return ['Diamond',cardRank]
        }
        return card
    })

    console.log('replacedByDimondCards = ', replacedByDimondCards)
    
// Replace all of the cards with rank A to rank 2. Keeping the same suit.
    let  replacedByRank2Cards = cards

    /*
        Will update original stack
    */

    // for(const card of replacedByRank2Cards){
    //     const [cardSuit,cardRank] = card;
    //     if(cardRank === 'A'){
    //         card[1] = '2'
    //     }
    // }

    replacedByRank2Cards = cards.map(function(card){
        const [cardSuit,cardRank] = card;
        if(cardRank === 'A'){
            // card[0] = 'Diamond'   !!!!!! dangerous
            return [cardSuit,'2']
        }
        return card
    })

    console.log('replacedByRank2Cards = ', replacedByRank2Cards)

    console.log('original cards = ', cards)
