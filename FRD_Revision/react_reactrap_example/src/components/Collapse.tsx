import React, { useState } from 'react';
import { Collapse, Button, CardBody, Card } from 'reactstrap';

const Example = (props :any) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div>
      <Button color="primary" onClick={toggle} style={{ marginBottom: '1rem' }}>{props.label}</Button>
      <Collapse isOpen={isOpen}>
        <Card>
          <CardBody>
           <div className='packageJson'><pre>{props.content}</pre></div>
          </CardBody>
        </Card>
      </Collapse>
    </div>
  );
}

export default Example;