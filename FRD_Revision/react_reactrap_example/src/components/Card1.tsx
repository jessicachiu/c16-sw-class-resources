import React from 'react';
import {
  Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle, Button
} from 'reactstrap';

const Ccard = (props :any) => {
  return (
    <div >
      <Card style={{width: "400px"}}>
        <CardImg top width="20px" style={{width: "200px"}} src="https://wallpapercave.com/wp/wp2646303.jpg" alt="Card image cap" />
        <CardBody>
          <CardTitle tag="h5"  style={{color: "black"}}>Card title</CardTitle>
          <CardSubtitle tag="h6" className="mb-2 text-muted">Card subtitle</CardSubtitle>
          <CardText  style={{color: "red"}}>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
          <Button>Button</Button>
        </CardBody>
      </Card>
    </div>
  );
};

export default Ccard;