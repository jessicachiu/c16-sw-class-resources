import React from "react";
import logo from "./logo.svg";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import ModalExample from "./components/Modal";
import DropDown from "./components/DropDown";
import { Button, Table } from "reactstrap";
import CPopup from "./components/Popup";
import CTooltip from "./components/Tooltip";
import packageJson from '../package.json';
import CCollapse from "./components/Collapse";
import Ccard1 from "./components/Card1"
import Ccard2 from "./components/Card2"
const REACT_VERSION = React.version;


function App() {
  const componentsDemo = [
    {
      name: "Button",
      html: <span><Button color="danger">Danger!</Button><Button color="success">Success</Button></span>,
    },
    {
      name: "DropDwon",
      html: <DropDown />,
    },
    {
      name: "Modal",
      html: <ModalExample buttonLabel="Click Me" className="my-modal" />,
    },
    {
      name: "Popup",
      html: <CPopup buttonLabel="Click Me" className="my-modal" />,
    },
    {
      name: "Tooltip",
      html: <CTooltip buttonLabel="Click Me" className="my-modal" />,
    },
    {
      name: "Card",
      html: <Ccard1 buttonLabel="Click Me" className="my-modal" />,
    },
    {
      name: "Card2",
      html: <Ccard2 buttonLabel="Click Me" className="my-modal" />,
    },
  ] as any;
  return (
    <div className="App">
      <header className="App-header">
        {/* <img src={logo} className="App-logo" alt="logo" /> */}
        <h1>Reactstrap demo</h1>
        <div>React version: {REACT_VERSION}</div>

        <CCollapse  className='collapse' content={JSON.stringify(packageJson, null, 2)} label='Show package.json'/>

      </header>
      <section className='content-container' >
      <Table dark className="my-table" borderless>
          <thead>
            <tr>
              <th>#</th>
              <th>Component</th>
              <th>Example</th>
            </tr>
          </thead>
          <tbody>
            {componentsDemo.map((component: any, index :number) => {
              return (
                <tr>
                  <th>{index + 1}</th>
                  <th>{component.name}</th>
                  <th>{component.html}</th>
                </tr>
              );
            })}
          </tbody>
        </Table>
        </section>
    </div>
  );
}

export default App;
