/*
https://medium.com/coding-blocks/creating-user-database-and-adding-access-on-postgresql-8bfcd2f4a91e
user : memo_wall_admin
password : '123456'
*/
create database "memo-wall";
create table memos (id serial,content text, image varchar(255), created_at timestamp, updated_at timestamp);
create table staff (id serial, username varchar(255), password varchar(255),created_at timestamp, updated_at timestamp);
