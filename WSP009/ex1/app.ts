import {Client} from 'pg';
import dotenv from 'dotenv';
dotenv.config();

const client = new Client({
    user:process.env.DB_USERNAME,
    password:process.env.DB_PASSWORD,
    database:process.env.DB_NAME,
    host:process.env.DB_HOST
});
client.connect();