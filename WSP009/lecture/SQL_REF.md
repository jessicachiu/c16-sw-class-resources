# access DB in terminal

```Bash
psql

```

remark: same db name with username

```Bash
psql -U <username> -W

```

```Bash
psql -U <username> -W -h <host> <dbname>

```

# exit DB

```SQL
\q

ctrl + c
```

# list out all db

```SQL
\l

```

# SQL to create db

```SQL
CREATE DATABASE <dbname>;
CREATE DATABASE tecky;

```
# change db

```SQL
\c <dbname>
\c memo-wall
\c tecky

```
# list all tables/relation in current DB

```SQL
\d
\d+
```
# view table details

```SQL
\d <table_name>
\d+ <table_name>

\d tecky
\d+ tecky
```
  # CREATE table

```SQL
CREATE TABLE students(
    <column_name> <data_type> <rules>
    ...
);

CREATE TABLE students(
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    level INTEGER NOT NULL,
    date_of_birth date
);

```

# INSERT 

```SQL
INSERT INTO <table_name> (columns...) VALUES (values...);

INSERT INTO students (name, level, date_of_birth) VALUES ('Peter', 25, '1995-05-15');

INSERT INTO students (name,level,date_of_birth) VALUES ('John',10,'1985-06-16'), ('Simon',20,'1987-07-17');

insert into Students (name,level,date_of_birth) VALUES ('Peter',25,'1995-05-15'),
                                                     ('John',10,'1985-06-16'),
                                                     ('Simon',20,'1987-07-17');
```

# SELECT

```SQL
# all columns
SELECT * FROM <table_name>;

# multi columns
SELECT col1, col2... FROM <table_name>;

```

# UPDATE

```SQL
UPDATE <table_name> SET <column_name> =  <Value>;
UPDATE <table_name> SET <column_name> =  <Value> WHERE <condition>;

UPDATE students SET level = level+5 ;
UPDATE students SET name = 'Jason' WHERE id = 1;

```
#  DELETE
```sql
DELETE FROM  <table_name>
DELETE FROM  <table_name> WHERE <condition> ;

DELETE FROM students;
DELETE FROM students WHERE level < 15;
```

# LIMIT / OFFSET

```sql
 SELECT * FROM students ORDER BY level ,date_of_birth LIMIT 2 OFFSET 1;
 SELECT * FROM students ORDER BY level ,date_of_birth LIMIT 2;
```
# ORDER
```sql
SELECT * FROM students ORDER BY <column_name> <order>; 

SELECT * FROM students ORDER BY level; 
SELECT * FROM students ORDER BY level DESC;
```
# ALTER
```sql
## rename
ALTER TABLE Students RENAME TO learners;

## Modify column

### update column definition
ALTER TABLE Students ALTER COLUMN date_of_birth TYPE timestamp;
### rename column
ALTER TABLE Students RENAME COLUMN date_of_birth TO birthday;
### remove column
ALTER TABLE Students DROP COLUMN date_of_birth;
### add column
ALTER TABLE Students ADD COLUMN date_of_birth date;
```
# TRUNCATE
```sql
TRUNCATE TABLE Students;
```
# DROP
```sql
## Drop table
DROP TABLE Students;

## Drop database
DROP DATABASE tecky;
```