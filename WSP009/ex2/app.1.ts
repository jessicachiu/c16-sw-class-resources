import {Client} from 'pg';
import XLSX from 'xlsx';
import dotenv from 'dotenv';
dotenv.config();

interface User{
    username:string,
    password:string
}
interface Memo{
    content:string,
    image?:string
}

let workbook = XLSX.readFile("WSP009-exercise.xlsx");
const usersWS = workbook.Sheets["user"];
const users:User[] = XLSX.utils.sheet_to_json(usersWS);

const memoWS = workbook.Sheets["memo"];
const memos:Memo[] = XLSX.utils.sheet_to_json(memoWS);

const client = new Client({
    user:process.env.DB_USERNAME,
    password:process.env.DB_PASSWORD,
    database:process.env.DB_NAME,
    host:process.env.DB_HOST
});
client.connect();

/*()=>{
    return "a"
}*/

/*()=>"a" */

const insertStaffTemplate = (user:User)=> 
    `insert into staff (username,password,created_at,updated_at) values ('${user.username}','${user.password}',NOW(),NOW())`;

const insertMemoTemplate = (memo:Memo)=>memo.image?
        `insert into memos (content,image,created_at,updated_at) values ('${memo.content}','${memo.image}',NOW(),NOW())`
    :`insert into memos (content,created_at,updated_at) values ('${memo.content}',NOW(),NOW())`;

async function main() {
    for(let user of users){
        await client.query(insertStaffTemplate(user));
    }
    for(let memo of memos){
        await client.query(insertMemoTemplate(memo));
    }
    const userCount = await client.query(`select * from staff`);
    const memoCount = await client.query(`select * from memos`);
    console.log(`user: ${userCount.rows.length}`);
    console.log(`memos: ${memoCount.rows.length}`);
    await client.end();
}
main();
