import { useSelector } from "react-redux"
import { RootState } from "./store"

export function Piglet() {
  const bonus = useSelector((state: RootState) => state.bonus)

  return (
    <div>
      🐷 {bonus}
    </div>
  )
}