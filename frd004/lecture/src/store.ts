import { createStore } from 'redux'

export interface RootState {
  players: number[],
  bonus: number
}

export const store = createStore(/* reducer */(previousState: RootState = {
  players: [],
  bonus: 0
}, action: any) => {
  if (action.type === '淘汰') {
    return {
      players: previousState.players.concat([action.number]),
      bonus: (previousState.players.indexOf(action.number) > -1) ? previousState.bonus : previousState.bonus + 100000000
    }
  }
  return previousState;
})

store.subscribe(() => {
  console.log('called when updated')
})
// store.dispatch()
// store.getState()