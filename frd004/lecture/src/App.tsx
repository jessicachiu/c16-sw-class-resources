import React, { useEffect } from 'react';
import logo from './logo.svg';
import './App.css';
import { Piglet } from './Piglet';
import { store } from './store';
import { useDispatch } from 'react-redux';

function App() {
  const dispatch = useDispatch();

  // useEffect(() => {
  //   async function load() {
  //     const res = await fetch("http://localhost/")
  //     const json = await res.json();

  //     setUsers(json)
  //   }
  //   load();
  // }, [])

  return (
    <div className="App">
      <header className="App-header">
        001 <button onClick={() => {
          dispatch(/* action */{
            type: '淘汰', // mandatory
            number: 1
          })
        }}>淘汰</button>
        123 <button onClick={() => {
          dispatch(/* action */{
            type: '淘汰', // mandatory
            number: 123
          })
        }}>淘汰</button>
        465 <button onClick={() => {
          dispatch(/* action */{
            type: '淘汰', // mandatory
            number: 465
          })
        }}>淘汰</button>
        467 <button onClick={() => {
          dispatch(/* action */{
            type: '淘汰!@@@!!!!', // mandatory
            number: 467
          })
        }}>淘汰</button>
        <Piglet />
      </header>
    </div>
  );
}

export default App;
