import React from 'react';
import logo from './logo.svg';
import AppStyles from './App.module.scss';
import { Friend } from './components/Friend';

const matchedFriends = [
  { id: 1, name: 'Alex', gender: 'F', age: 18 },
  { id: 2, name: 'Gordon', gender: 'F', age: 19 },
  { id: 3, name: 'Michael', gender: 'M', age: 20 },
  { id: 4, name: 'Jason', gender: 'F', age: 21 },
];

function App() {
  let i = 0
  return (
    <div className={AppStyles.App}>
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        { i % 2 == 0 ? "yes" : "no" }
        { i }
        { i == 0 && "it is zero" }
        { i != 0 && "it is not zero" } 
        { true }
        { false }
        { null }
        { undefined }
        { matchedFriends.length == 0 && <p>哎 慘慘豬 無人match毒L</p> }
        { matchedFriends.map(friend => (
            <Friend
              name={friend.name}
              gender={friend.gender}
              age={friend.age}
              key={friend.id} />
          )) }
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React 
        </a>
      </header>
    </div>
  );
}

export default App;
