import React from 'react';

export function Friend(props: {
  name: string, 
  gender: string,
  age: number
}) {
  return <div className={"friend " + (props.gender === 'M' ? 'male' : 'female')}>
    <p>Name: {props.name}</p>
    <p>Age: {props.age}</p>
  </div>
}