import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';
import { Friend } from './components/Friend';
import { FriendCls } from './components/FriendCls';

test('renders learn react link', () => {
  render(<App />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});

test('add andrew', () => {
  render(<App />);
  const buttonElement = screen.getByText(/Find new friend/i);
  buttonElement.click();
  const andrewElement = screen.getByText(/andrew/i);
  expect(andrewElement).toBeInTheDocument();
})

test('check duplicate andrew', async () => {
  render(<App />);
  const buttonElement = screen.getByText(/Find new friend/i);
  buttonElement.click();
  buttonElement.click();
  const deleteElement = screen.getByText(/Delete Alex/i);
  deleteElement.click();
  const andrewElement = await screen.findAllByText(/andrew/i);
  expect(andrewElement.length).toBe(2);
})

test('render friend',  () => {
  const deleteFn = jest.fn();
  const increaseFn = jest.fn();
  render(<Friend name="" age={0} gender="" onDelete={deleteFn} onIncreaseAge={increaseFn} />);
  const deleteElement = screen.getByText(/Delete/i);
  deleteElement.click();
  expect(deleteFn).toBeCalledTimes(1)

})

test('render friend class component',  () => {
  const deleteFn = jest.fn();
  const increaseFn = jest.fn();
  render(<FriendCls name="Peter" age={0} gender="" onDelete={deleteFn} onIncreaseAge={increaseFn} />);
  const deleteElement = screen.getByText(/Delete/i);
  deleteElement.click();
  expect(deleteFn).toBeCalledTimes(1)
  const peterElement = screen.getByText(/Peter/i);
  expect(peterElement).toBeInTheDocument()
})