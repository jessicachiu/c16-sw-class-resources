import React from 'react';

interface Props {
  name: string, 
  gender: string,
  age: number,
  onIncreaseAge: (increments: number) => void,
  onDelete: () => void
}

export class FriendCls extends React.Component<Props> {
  public constructor(props: Props) {
    super(props)

    // your todo things here
  }

  public render() {
    return <div className={"friend " + (this.props.gender === 'M' ? 'male' : 'female')}>
      <p>Name: {this.props.name}</p>
      <p>Age: {this.props.age}</p>
      <button className="btn btn-primary" onClick={() => {
        if (this.props.gender == 'F') {
          this.props.onIncreaseAge(0.5)
        } else {
          this.props.onIncreaseAge(this.props.age / 2)
        }
      }}>增齡</button>
      <button onClick={() => {
        this.props.onDelete()
      }}>Delete</button>
    </div>
  }
}