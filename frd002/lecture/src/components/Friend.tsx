import React from 'react';

export function Friend(props: {
  name: string, 
  gender: string,
  age: number,
  onIncreaseAge: (increments: number) => void,
  onDelete: () => void
}) {
  return <div className={"friend " + (props.gender === 'M' ? 'male' : 'female')}>
    <p>Name: {props.name}</p>
    <p>Age: {props.age}</p>
    <button className="btn btn-primary" onClick={() => {
      if (props.gender == 'F') {
        props.onIncreaseAge(0.5)
      } else {
        props.onIncreaseAge(props.age / 2)
      }
    }}>增齡</button>
    <button onClick={() => {
      props.onDelete()
    }}>Delete</button>
  </div>
}