import React, { useState } from 'react';
import logo from './logo.svg';
import AppStyles from './App.module.scss';
import { Friend } from './components/Friend';
import { Carousel } from 'react-bootstrap';
import { FriendCls } from './components/FriendCls';

function App() {
  const [matchedFriends, setMatchFriends] = useState(/* default value */ [
    { id: 1, name: 'Alex', gender: 'F', age: 18 },
    { id: 2, name: 'Gordon', gender: 'F', age: 19 },
    { id: 3, name: 'Michael', gender: 'M', age: 20 },
    { id: 4, name: 'Jason', gender: 'F', age: 21 },
  ]);
  console.log(matchedFriends)

  let i = 0

  const [index, setIndex] = useState(0);

  const handleSelect = (selectedIndex: number) => {
    setIndex(selectedIndex);
  };

  return (
    <div className={AppStyles.App}>
      <header className="App-header">
      <Carousel activeIndex={index} onSelect={handleSelect}>
        <Carousel.Item>
          <img className="d-block w-100 App-logo" src={logo} alt="logo" />

          <Carousel.Caption>
            <h3>First slide label</h3>
            <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img className="d-block w-100 App-logo" src={logo} alt="logo" />

          <Carousel.Caption>
            <h3>Second slide label</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img className="d-block w-100 App-logo" src={logo} alt="logo" />

          <Carousel.Caption>
            <h3>Third slide label</h3>
            <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>

      <div onMouseOver={() => setIndex(0)}>
        <h2>第一格</h2>
      </div>

      <div onMouseOver={() => setIndex(1)}>
        <h2>第2格</h2>
      </div>

      <div onMouseOver={() => setIndex(2)}>
        <h2>第3️⃣格</h2>
      </div>

        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>

        <button onClick={() => {
          // 方法一，用 destruction operator
          const newMatchedFriends = [...matchedFriends, {
            id: (matchedFriends[matchedFriends.length - 1]?.id ?? 0) + 1, name: 'Andrew', gender: 'M', age: 18
          }]
          // 方法二，用 concat
          const newMatchedFriends2 = matchedFriends.concat([{
            id: 5, name: 'Andrew', gender: 'M', age: 18
          }])
          // 方法三，先 slice 後改
          const newMatchedFriends3 = matchedFriends.slice()
          newMatchedFriends3.push({
            id: 5, name: 'Andrew', gender: 'M', age: 18
          })
          setMatchFriends(newMatchedFriends)
        }}>Find new friends</button>

        <button onClick={() => {
          // 方法一 : matchedFriends.filter
          // 方法二 : 先 slice 後 shift
          // 方法三 :
          const [_, ...newMatchFriends] = matchedFriends
          setMatchFriends(newMatchFriends)
          console.log(matchedFriends)
        }}>Delete Alex</button>

        { i % 2 == 0 ? "yes" : "no" }
        { i }
        { i == 0 && "it is zero" }
        { i != 0 && "it is not zero" } 
        { true }
        { false }
        { null }
        { undefined }
        { matchedFriends.length == 0 && <p>哎 慘慘豬 無人match毒L</p> }
        { matchedFriends.map(friend => (
            <FriendCls
              name={friend.name}
              gender={friend.gender}
              age={friend.age}
              onIncreaseAge={(increments: number) => {
                friend.age += increments // 呢句無尊重到 Immutability
                setMatchFriends([...matchedFriends])
              }}
              onDelete={() => {
                const newMatchFriends = matchedFriends.filter(aFriend => aFriend != friend)
                setMatchFriends(newMatchFriends)
              }}
              key={friend.id} />
          )) }
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React 
        </a>
      </header>
    </div>
  );
}

export default App;
