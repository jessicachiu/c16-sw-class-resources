export function Square(props: {
  value: 'X' | 'O' | null,
  highligthed: boolean,
  onClick: () => void
}) {
  return <div className={props.highligthed ? 'square highlight' : 'square'} onClick={props.onClick}>
    {props.value === 'X' && <i className="fas fa-times"></i>}
    {props.value === 'O' && <i className="far fa-circle"></i>}
  </div>;
}