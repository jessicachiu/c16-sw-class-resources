import React, { useState } from 'react';
import './App.css';
import { Square } from './Square';

const winningConditions = [
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
  [0, 4, 8],
  [2, 4, 6],
];

function App() {
  const [boardState, setBoardState] = useState<('X' | 'O' | null)[]>([
    null, null, null,
    null, null, null,
    null, null, null,
  ])
  const [turn, setTurn] = useState(1)

  // FRD003 useEffect()
  const [leaderboard, setLeaderBoard] = useState<('X' | 'O')[]>([])

  // check winner
  let winner: 'X' | 'O' | null = null;
  let winningRow: number[] | null = null;
  for (let winningCondition of winningConditions) {
    if (boardState[winningCondition[0]] &&
      (boardState[winningCondition[0]] === boardState[winningCondition[1]] &&
        boardState[winningCondition[1]] === boardState[winningCondition[2]])
    ) {
      winner = boardState[winningCondition[0]]
      winningRow = winningCondition
      
      // Will incur infinit-loop here
      // if (winner != null) {
      //   setLeaderBoard([...leaderboard, winner])
      // }
    }
  }

  return (
    <section>
      <main>
        <div className='top-row'>
          <div className='level-dropdwon'>
            <i className="fas fa-chevron-down"></i>
            <select className='label' defaultValue="Medium">
              <option>Easy</option>
              <option>Medium</option>
              <option>Hard</option>
            </select>
          </div>
          <i className="fas fa-share-alt"></i>
        </div>
        <div className='scoring-row'>
          <div className='scoring-container'>
            <div className='scoring-container-inner'>
              <div className='player'>X</div>
              <div className='x-counter'>0</div>
            </div>
          </div>
          <div className='scoring-container'>
            <div className='scoring-container-inner'>
              <div className='player'>O</div>
              <div className='o-counter'>0</div>
            </div>
          </div>
        </div>
        <div className='turn-row'>
          { winner != null && `${winner} is the winner \u{1F439}` }
          { winner == null && turn % 2 === 1 ? <><i className="fas fa-times"></i> Turn</> : <><i className="far fa-circle"></i> Turn</> }
        </div>
        <div className='board-row'>
          <div className='board'>
            {Array(9).fill(null).map((_, i) => {
              return <Square
              key={i}
              value={boardState[i]}
              highligthed={winningRow != null && winningRow.indexOf(i) > -1}
              onClick={() => {
                if (boardState[i] !== null || winner != null) {
                  return;
                }
                let newTurn = turn
                const newBoardState = boardState.slice()
                if (newTurn % 2 === 0) {
                  newBoardState[i] = 'O'
                } else {
                  newBoardState[i] = 'X'
                }
                newTurn += 1

                // const computerMove = Math.floor(Math.random() * 9)
                // if (newTurn % 2 === 0) {
                //   newBoardState[computerMove] = 'O'
                // } else {
                //   newBoardState[computerMove] = 'X'
                // }
                // newTurn += 1

                setTurn(newTurn)
                setBoardState(newBoardState)
              }} />
            })}
          </div>
        </div>
        <div className='restart-container' onClick={() => {
          setTurn(1)
          setBoardState([
            null, null, null,
            null, null, null,
            null, null, null,
          ])
        }}>
          restart game
        </div>

      </main>
      <footer>
        <i>feedback</i>
      </footer>
    </section>
  );
}

export default App;
