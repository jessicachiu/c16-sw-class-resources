import { knex } from "../db";
import { logger } from "../logger";

export class MemoService{
    constructor(){}
    createMemo = async(content:string,image:string|undefined)=>{
        try {
            if (image) {
                await knex.raw(
                    `insert into memos(content,image,created_at,updated_at) values ('${content}','${image}',NOW(),NOW())
                `
                );

            } else {
                await knex.raw(
                    `insert into memos(content,created_at,updated_at) values ('${content}',NOW(),NOW())
                `
                );
            }
            return { success: true };
        } catch (err) {
            logger.error("error:", err);
            return { success: false, error: err };
        }
    }
    updateMemoById = async(id:string,content:string,image:string|undefined)=>{}
    getAllMemos = async()=>{
        const results = await knex.raw(`select * from memos order by id DESC`);
        return results.rows;
    }
    deleteMemoById=async(id:string)=>{
        try {
            await knex("memos").delete().where("id",id);
            return { success: true };
        } catch (err) {
            logger.error("error:", err);
            return { success: false };
        }
    }
    deleteAllMemo = async () => {
        try {
            await knex.raw(/*sql*/ `DELETE FROM memos`);
            return { msg: "Cleared All memo" };
        } catch (error) {
            logger.error("Error :" + JSON.stringify(error));
            return { err: "System error" };
        }
    }
    likeMemoById = async (id: string, userId: string) => {
        try {
            await knex.raw(/*sql*/ `
        insert into likes(user_id,memo_id,created_at,updated_at) values ('${userId}','${id}',NOW(),NOW())
        `);
            return { success: true };
        } catch (error) {
            logger.error("Error :" + JSON.stringify(error));
            return { err: "System error" };
        }
    }
    getLikedMemosById = async(userId:any)=>{
        try {
            const results = await knex.raw(
                /*sql*/
                `
            SELECT memos.*  from staff 
                INNER JOIN likes on likes.user_id = staff.id
                INNER JOIN memos on likes.memo_id = memos.id
                WHERE staff.id = $1;
            `,
                [userId]
            );
            return { success: true, data: results.rows };
        } catch (error) {
            logger.error("Error :" + JSON.stringify(error));
            return { err: "System error" };
        }
    }
}