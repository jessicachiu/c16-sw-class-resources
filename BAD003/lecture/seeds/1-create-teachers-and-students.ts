import { Knex } from "knex";
import Chance from "chance";
// import { hashPassword } from "../hash";
const chance = new Chance();

export async function seed(knex: Knex): Promise<void> {
    await knex("students").del();
    await knex("teachers").del();

    let teachers: any = [];
    let students: any = [];
    // 準備一個 teacher array
    for (let i = 0; i < 10; i++) {
        teachers.push({
            teacher_name: chance.name(),
            date_of_birth: chance.date(),
        });
    }

    // INSERT teachers
    const teacherResult = await knex.insert(teachers).into("teachers").returning("id");
    console.log("teacherResult =", teacherResult);
    // teacherResult
    // [
    // 95,  96,  97,  98,
    // 99, 100, 101, 102,
    // 103, 104
    //]


    // 準備一個 student array, 用番 上面insert teacher 既 id
    for (let i = 0; i < 50; i++) {
        students.push({
            name: chance.name(),
            // name: await hashPassword("123456"),
            level: chance.integer({ min: 10, max: 60 }),
            date_of_birth: chance.date(),
            // teacherResult[0] 係當陣時，上面insert 果十隻teacher 既第一隻 既id
            teacher_id: chance.integer({ min: (teacherResult[0] as any) , max: (teacherResult[0] as any) + 9 }),
        });
    }

    // INSERT students
    await knex.insert(students).into("students");
    return;
}
