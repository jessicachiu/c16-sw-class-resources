//config should put in server.ts or db.ts
import Knex from "knex";
import dotenv from "dotenv";

dotenv.config();

const knexConfigs = require("./knexfile");
const configMode = process.env.NODE_ENV || "development";
const knexConfig = knexConfigs[configMode];
// export the db client connection for services use
export const knex = Knex(knexConfig);

async function main() {

    await knex.raw(`select 'name', 'level', 'date_of_birth' from 'students'`);
    const result = knex.select("name", "level", "date_of_birth").from("students");

    // toQuery()  去睇番query builder 幫你build左條咩 SQL
    console.log(result.toQuery());

    const students = await knex.select("name", "level", "date_of_birth")
                                .from("students")
                                .where("date_of_birth", "1980-01-01")
                                .orWhere("level", ">", 123)
                                .andWhere("name", "like", "%e%");
    console.log(students);

    await knex.select('name', 'level', 'date_of_birth')
                    .from('students')
                    .where('date_of_birth', '1970-01-01')
                    .whereIn('teacher_id', function(){
                        this.select('id').from('teachers');
                    });

    await knex('students')
        .update({
            level:9999,
            date_of_birth: '1965-05-01'
        })
        .where('name', 'Ken');
    await knex('teachers').where('name','Ben').del();
}
main();
