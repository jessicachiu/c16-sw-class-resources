import { Knex } from "knex";

export async function up(knex: Knex) {
    if (await knex.schema.hasTable("teachers")) {
        await knex.schema.alterTable("teachers", (table) => {
            table.renameColumn("name", "teacher_name");
            // table.decimal("level", 3).alter();
        });
    }
}

export async function down(knex: Knex) {
    if (await knex.schema.hasTable("teachers")) {
        await knex.schema.alterTable("teachers", (table) => {
            table.renameColumn("teacher_name", "name");
            // table.string("level").alter();
        });
    }
}
