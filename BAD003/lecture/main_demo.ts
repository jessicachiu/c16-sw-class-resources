// server.ts
import Knex from "knex";
import dotenv from "dotenv";

dotenv.config();

const knexConfigs = require("./knexfile");
const configMode = process.env.NODE_ENV || "development";
const knexConfig = knexConfigs[configMode];
const knex = Knex(knexConfig);

async function main() {
    `
    select * from students where name = 'Ken';
    `;
    const selectResult = await knex("students").where("name", "Ken");
    const selectResult2 = await knex.from("students").where("name", "Ken");

    console.log({ selectResult });
    console.log({ selectResult2 });

    // let whereString= ' where xxxx = yyy'
    // if (whereString){
    //     whereString += ' AND cccc = eeee '
    // }
    `
    select * from students where name = 'Ken';
    `;
    const result = await knex("students")
        .update({
            level: 3,
            date_of_birth: "1965-05-01",
        })
        .where({
            name: "Ken",
            age: undefined,
        });

    console.log(result);

    let updateQuery = knex("students")
        .update({
            level: 3,
            date_of_birth: "1965-05-01",
        })
        .where({
            name: "Ken",
        });

    if (applicationForm.age) {
        updateQuery = updateQuery.where({
            age: 13,
        });
    }
    await updateQuery;
}
main();
