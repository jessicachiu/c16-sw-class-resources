interface FlyingAnimal {
  fly(height: number): void;
  //   fastFly(height: number): void;
  flyAlone(height: number): void;
  //   flyWithGf(height: number): void;
}

interface Mammal {
  breastFeed(): void;
}

interface Bird {
  layEggs(numberOfEggs: number): void;
}

// Class

class Eagle implements FlyingAnimal, Bird {
  flyAlone(height: number): void {
    throw new Error("Method not implemented.");
  }
  layEggs(numberOfEggs: number): void {
    throw new Error("Method not implemented.");
  }
  fly(height: number): void {
    console.log(`Bat flies to ${height} using ultrasonic`);
  }
}

class Bat implements FlyingAnimal, Mammal {
  flyAlone(height: number): void {
    throw new Error("Method not implemented.");
  }
  breastFeed(): void {
    throw new Error("Method not implemented.");
  }
  fly(height: number): void {
    console.log(`Bat flies to ${height} using ultrasonic`);
  }
}

class Dragonfly implements FlyingAnimal {
  // @ts-ignore
  private hp: number;
  constructor() {
    this.hp = 1000;
    console.log("this.hp");
  }
  flyAlone(height: number): void {
    throw new Error("Method not implemented.");
  }
  fly(height: number): void {
    this.hp *= 1.5;
    console.log(`Dragonfly flies to ${height} using its insect wings`);
  }
}

const eagle = new Eagle();
const bat = new Bat();
const dragonfly = new Dragonfly();

function flyingContest(flyingAnimals: FlyingAnimal[]) {
  for (let flyingAnimal of flyingAnimals) {
    flyingAnimal.fly(100);
  }
}
flyingContest([eagle, bat, dragonfly]);
