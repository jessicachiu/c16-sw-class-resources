class Person {
  private name: string;
  constructor(name: string) {
    this.name = name;
  }
  rename(newName: string) {
    console.log(this.name);
    if (!newName.includes("pk")) {
      this.name = newName;
    } else {
      this.name = "";
    }
  }

  // Not good
  // setName(newName : string){
  //   this.name = newName
  // }
}

const person1 = new Person("John");
person1.rename("Peter");
console.log(person1); // Print Peter / John?

/// Protected

class Person2 {
  protected name: string;
  constructor(name: string) {
    this.name = name;
  }
}

class Student2 extends Person2 {
  private currentClass: string;

  constructor(name: string, currentClass: string) {
    super(name);
    this.name = `Student ${this.name}`; // Accessible here
    this.currentClass = currentClass;
  }

  check() {
    console.log(this.currentClass);
  }
}

const s = new Student2("John", "F.1");
// s.name = "Peter"; // ERROR Here

// Short form

class Person3 {
  constructor(private name: string, public age: number, private email: string) {
    console.log(this.name, this.age, this.email);
  }
}

let person3Instance = new Person3("Name", 29, "wqew.com");
console.log("person3Instance  =", person3Instance);
