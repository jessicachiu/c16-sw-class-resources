class BankAccount {
    // Bank Account Balance
    private balance: number;
    private readonly accountSID :number
  
    constructor() {
      this.balance = 0;
      this.accountSID = 45678923
    }
  
    deposit(amount: number) {
      this.balance += amount;
    }
  
    withdraw(amount: number) {
      if (this.balance < amount) {
        throw new Error("Not enough balance!");
      }
      this.balance -= amount;
    }
    getAccountSID(){
      return this.accountSID
    }
    getBalance() {
      return this.balance;
    }
    luckyDraw(investAmt: number) {
      this.balance -= investAmt;
      let prob = investAmt / 1000000000000000;
      if (Math.random() > prob) {
        this.vip999();
      } else {
        this.balance = 0;
      }
    }
    private vip999() {
      this.balance += 999999999;
    }
  }
  
  class PrivateBankAccount extends BankAccount{
  
  }
  
  const HSBCAccount = new BankAccount();
  HSBCAccount.deposit(100);
  // HSBCAccount.withdraw(110);
  // HSBCAccount.balance = HSBCAccount.balance - 110;
  HSBCAccount.luckyDraw(100);
  console.log(HSBCAccount.getBalance());
  // This line assign the balance to a negative number!!!
  
  
  
  const BALAMAAccount = new PrivateBankAccount()
  BALAMAAccount.accountSID