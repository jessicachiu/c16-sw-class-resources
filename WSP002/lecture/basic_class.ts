console.log("This is basic_class.ts");

class Student {
  name: string;
  age: number;
  learningLevel: number;

  // init
  constructor(name: string, age: number) {
    this.name = name;
    this.age = age;
    this.learningLevel = 0;
  }

  learn(hourSpent: number) {
    console.log(`learning ${hourSpent}`);
    this.learningLevel += hourSpent * 0.3;
    this.slack(hourSpent / 5);
  }
  slack(hourSpent: number) {
    console.log(`slacking ${hourSpent}`);
    this.learningLevel -= hourSpent * 0.1;
  }
  reName(newName: string) {
    this.name = newName;
  }
}

let student1 = new Student("Bob", 20);
let student2 = new Student("May", 20);
let student3 = new Student("Ken", 20);
console.log("student1 = ", student1);
console.log("learningLevel", student1.learningLevel);
student1.learn(20);
student1.slack(20);
