console.log("This is basic_class.ts");
export {};
class Student {
  name: string;
  age: number;
  learningLevel: number;

  // init
  constructor(name: string, age: number) {
    this.name = name;
    this.age = age;
    this.learningLevel = 0;
  }

  learn(hourSpent: number) {
    console.log(`Student learning ${hourSpent}`);
    this.learningLevel += hourSpent * 0.3;
    this.slack(hourSpent / 5);
  }
  slack(hourSpent: number) {
    console.log(`Student slacking ${hourSpent}`);
    this.learningLevel -= hourSpent * 0.1;
  }
  reName(newName: string) {
    this.name = newName;
  }
}

let student1 = new Student("Bob", 20);
let student2 = new Student("May", 20);
let student3 = new Student("Ken", 20);
if (5 > 10) {
  console.log("student1 = ", student1, student2, student3);
  console.log("learningLevel", student1.learningLevel);
  student1.learn(20);
  student1.slack(20);
}

////////////////////////////////////////  1st  Sub Class  ///////////////////////////////////
class CodingStudent extends Student {
  codingExp: number;
  favLang: string[];

  constructor(name: string, age: number, codingExp: number) {
    super(name, age);
    this.codingExp = codingExp;
    // this.favLang = ["JS"];
  }

  learn(hourSpent: number) {
    console.log(`CodingStudent learning ${hourSpent}`);
    this.learningLevel += hourSpent * 0.5;
  }
  slack(hourSpent: number) {
    console.log(`CodingStudent learning ${hourSpent}`);
    /*Calling slack(hourSpent) in Student*/
    super.slack(hourSpent);
    this.learningLevel -= hourSpent * 0.3;
  }
  /*It is also beneficial for CodingStudent to read Reddit*/
  readReddit(hourSpent: number) {
    this.learningLevel += hourSpent * 0.2;
  }
}

let codingStudent1 = new CodingStudent("Derek", 30, 0.5);
if (10 > 100) {
  console.log("codingStudent1 = ", codingStudent1);
  codingStudent1.learn(20);
  codingStudent1.slack(10);
}

////////////////////////////////////////  2nd  Sub Class  ///////////////////////////////////
class TeckyStudent extends CodingStudent {
  // Optional
  // constructor(name: string, age: number, codingExp: number) {
  //   super(name, age, codingExp);
  // }

  slack(hourSpent: number) {
    console.log(`TeckyStudent slacking ${hourSpent}`);
    this.learningLevel -= hourSpent * 0.8;
    super.slack(22);
  }
}

let teckyStudent = new TeckyStudent("Clever", 30, 0.5);
console.log("teckyStudent = ", teckyStudent);
teckyStudent.slack(10);
