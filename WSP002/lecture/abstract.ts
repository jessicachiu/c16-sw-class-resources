abstract class Animal {
  //
  constructor(public name: string) {}
  abstract eat(): void;
  abstract sleep(): void;
  play() {
    console.log(" Animal Playing");
  }
}
interface AnimalBehavior {
  interfaceEat(): void;
  interfaceSleep(): void;
}
class Human extends Animal implements AnimalBehavior {
  eat(): void {
    throw new Error("Method not implemented.");
  }
  sleep(): void {
    throw new Error("Method not implemented.");
  }
  constructor(public name: string) {
    super(name);
  }
  interfaceEat(): void {
    throw new Error("Method not implemented.");
  }
  interfaceSleep(): void {
    throw new Error("Method not implemented.");
  }
}

// const animal = new Animal(); //ERROR Here

const human = new Human("John"); // No Problem here.
human.play();
