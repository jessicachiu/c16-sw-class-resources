function deposit(this: any, amount: number) {
  console.log(this);
  this.total += amount;
  console.log(`Amount ${amount} is deposited to the account of ${this.name}`);
  return this.total;
}
let total = deposit(1000);
console.log("total =  ", total);
const gordonAcc = { total: 3000, name: "gordon" };
const jackyAcc = { total: 3000, name: "jacky" };

const depositToGordonAcc = deposit.bind(gordonAcc);
depositToGordonAcc(100);
console.log(gordonAcc);

const deposit200ToGordonAcc = deposit.bind(gordonAcc, 200);
deposit200ToGordonAcc();
// const depositToJackyAcc = deposit.bind(jackyAcc);
// depositToJackyAcc(120000);
// console.log(jackyAcc);

// Exmaple 2

function flyToAnywhere(this: any) {
  console.log(`${this.winner} can fly to ${this.destination}`);
}

const winningTicket = { winner: "John", destination: "TW" };
const winnerResultFun = flyToAnywhere.bind(winningTicket);
winnerResultFun();

function luckyDraw(this: any) {
  console.log(`${this.winner} can fly to ${this.destination}`);
}
const ITStaff = ["", "", ""];
const ITLuckyDraw = flyToAnywhere.bind(ITStaff);
