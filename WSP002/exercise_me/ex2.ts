interface Attack{
    damage:number
 }
 
 class BowAndArrow implements Attack{
            damage:number;
      constructor(damage:number){
          this.damage = damage;
      }

 }
 
 class ThrowingSpear implements Attack{
            damage:number;
      constructor(damage:number){
          this.damage = damage;
 }
}
 
 
 interface Player{
     attack(monster:Monster): void;
     switchAttack(): void;
     gainExperience(exp:number): void;
 }
 
 class Amazon implements Player{
     private primary: Attack;
     private secondary: Attack;
     private usePrimaryAttack: boolean;
     constructor(){
         this.primary = new BowAndArrow(30);
         this.secondary = new ThrowingSpear(40);
         // TODO: set the default value of usePrimaryAttack
      }
 
      attack(monster:Monster2):void{
          if(this.usePrimaryAttack){
             // TODO: use primary attack
          }else{
             // TODO: use secondary attack
          }
      }
 
      switchAttack(){
          // TODO: Change the attack mode for this player
      }
 
      //.. The remaining methods.
 }
 
 class Monster2{
 // You can use the `Monster` before
 }