

// Declaration of Class and its methods
class Player{
    strength:number;
    private name:string;

    constructor(strength:number,name:string){
        this.strength  = strength;
        this.name = name;
        console.log ("player name: " + this.name)
    }



    attack(monster:Monster){
        //1 ~ 10
        let prop = (Math.random()*10)+1;
        if (prop>8) {
            monster.injure(this.strength*2)
            console.log('Player Peter attacks a monster HP: ' + monster.getHP() + '[CRITICAL]')
        }else{
            monster.injure(this.strength);
            console.log('Player Peter attacks a monster HP: ' + monster.getHP())
        }
        
       
        
    }



    gainExperience(exp:number){
        this.strength += exp*0.1
    }

}


class Monster{
   name:string;
   private hp:number;
   constructor(name:string,hp:number){
        this.name = name;
        this.hp = hp;
        console.log("monster hp: " + this.hp)
   }
   getHP() {
    return this.hp;
}
    injure(strength:number) {
        this.hp -= strength 
    }
}


// Invocations of the class and its methods
const player = new Player(10,'Peter');
const monster = new Monster('monster', 100);
while(monster.getHP()>0){
    player.attack(monster);
}


// English counterpart: Player attacks monster