import { Monster } from "./main";

interface Attack {
  getDamage: () => number;
}

export class BowAndArrow implements Attack {
  private damage: number;
  constructor(damage: number) {
    this.damage = damage;
  }
  getDamage() {
    return this.damage;
  }
}

class ThrowingSpear implements Attack {
  private damage: number;
  constructor(damage: number) {
    this.damage = damage;
  }
  getDamage() {
    return this.damage;
  }
}
class Swords implements Attack {
  private damage: number;
  constructor(damage: number) {
    this.damage = damage;
  }
  getDamage() {
    return this.damage;
  }
}
class MagicSpells implements Attack {
  private damage: number;
  constructor(damage: number) {
    this.damage = damage;
  }
  getDamage() {
    return this.damage;
  }
}
interface Player {
  attack(monster: Monster): void;
  switchAttack(): void;
  gainExperience(exp: number): void;
}

export class GamePlayer implements Player {
  private primary: Attack;
  private secondary: Attack;
  private usePrimaryAttack: boolean;
  private exp: number;
  private role: Role;
  //@ts-ignore
  private level: number;
  constructor(role: Role) {
    this.role = role;
    this.primary = Characters[role].primary;
    this.secondary = Characters[role].secondary;
    this.usePrimaryAttack = true;
    this.exp = 0;
    this.level = 1;
  }
  gainExperience(exp: number): void {
    this.exp += exp;
    this.level = Math.floor(25 + Math.sqrt(625 + 100 * this.exp)) / 50;
  }

  attack(monster: Monster): void {
    // Prepare to attack
    let multiplier: number = Math.random() > 0.9 ? 2 : 1;

    if (this.usePrimaryAttack) {
      // Attack now
      monster.injure(this.primary.getDamage() * multiplier);
    } else {
      // Attack now
      monster.injure(this.secondary.getDamage() * multiplier);
    }

    // Post Attack (Report, lv up, exp++)
    console.log(
      `[${this.role}] 攻擊 [${monster.getName()}] (HP: ${monster.getHp()}) ${
        multiplier == 2 ? "[！一陽神功 ！]" : ""
      }`
    );
    if (monster.getHp() == 0) {
      console.log(`[${this.role}] ：啊！！！ 我竟然輸比你？！\u{1F47C}`);
      this.gainExperience(monster.getExp());
    }
  }

  switchAttack() {
    this.usePrimaryAttack = !this.usePrimaryAttack;
    // if (this.usePrimaryAttack) {
    //   this.usePrimaryAttack = false;
    // } else {
    //   this.usePrimaryAttack = true;
    // }
  }
}
// Invocations of the class and its methods
type Role = "Amazon" | "Paladin" | "Barbarian" | "GM";

type Character = {
  [role in Role]: Equipment;
};

type Equipment = {
  primary: Attack;
  secondary: Attack;
};

const Characters: Character = {
  Amazon: {
    primary: new ThrowingSpear(30),
    secondary: new ThrowingSpear(40),
  },
  Paladin: {
    primary: new Swords(50),
    secondary: new MagicSpells(25),
  },
  Barbarian: {
    primary: new Swords(55),
    secondary: new ThrowingSpear(30),
  },
  GM: {
    primary: new Swords(99999),
    secondary: new ThrowingSpear(9999),
  },
};

const player1 = new GamePlayer("Amazon");
const player2 = new GamePlayer("Barbarian");
const player3 = new GamePlayer("GM");
const monster1 = new Monster(500, "怪獸1號", 5000, 0.1);
const monster2 = new Monster(1500, "怪獸2號", 5000, 0.1);
const monster3 = new Monster(15000000000, "怪獸2號", 115000, 0.1);
// console.log("player = ", player);
// console.log("monster = ", monster);
while (monster1.getHp() > 0) {
  player1.attack(monster1);
  player1.switchAttack();
}
while (monster2.getHp() > 0) {
  player2.attack(monster2);
  player2.switchAttack();
}
while (monster3.getHp() > 0) {
  player3.attack(monster3);
  player3.switchAttack();
}
console.log({ player1, player2, player3 });
