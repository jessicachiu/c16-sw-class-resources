class Circle {
  constructor(protected x: number, protected y: number) {}

  moveX(increment: number) {
    this.x += increment;
  }
  moveY(increment: number) {
    this.y += increment;
  }
  move(increment: number) {
    this.x += increment;
    this.y += increment;
  }
}

// ???? Wrong concept
class Ellipse extends Circle {}
