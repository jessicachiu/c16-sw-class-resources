import { Monster } from "./main";

interface Attack {
  getDamage: () => number;
}

class BowAndArrow implements Attack {
  private damage: number;
  constructor(damage: number) {
    this.damage = damage;
  }
  getDamage() {
    return this.damage;
  }
}

class ThrowingSpear implements Attack {
  private damage: number;
  constructor(damage: number) {
    this.damage = damage;
  }
  getDamage() {
    return this.damage;
  }
}
class Swords implements Attack {
  private damage: number;
  constructor(damage: number) {
    this.damage = damage;
  }
  getDamage() {
    return this.damage;
  }
}
class MagicSpells implements Attack {
  private damage: number;
  constructor(damage: number) {
    this.damage = damage;
  }
  getDamage() {
    return this.damage;
  }
}
interface Player {
  attack(monster: Monster): void;
  switchAttack(): void;
  gainExperience(exp: number): void;
}

export class Amazon implements Player {
  private primary: Attack;
  private secondary: Attack;
  private usePrimaryAttack: boolean;
  private exp: number;
  //@ts-ignore
  private level: number;
  constructor() {
    this.primary = new BowAndArrow(30);
    this.secondary = new ThrowingSpear(40);
    this.usePrimaryAttack = true;
    this.exp = 0;
    this.level = 1;
  }
  gainExperience(exp: number): void {
    this.exp += exp;
    this.level = Math.floor(25 + Math.sqrt(625 + 100 * this.exp)) / 50;
  }

  attack(monster: Monster): void {
    // Prepare to attack
    let multiplier: number = Math.random() > 0.9 ? 2 : 1;

    if (this.usePrimaryAttack) {
      // Attack now
      monster.injure(this.primary.getDamage() * multiplier);
    } else {
      // Attack now
      monster.injure(this.secondary.getDamage() * multiplier);
    }

    // Post Attack (Report, lv up, exp++)
    console.log(
      `[Amazon] 攻擊 [${monster.getName()}] (HP: ${monster.getHp()}) ${
        multiplier == 2 ? "[！一陽神功 ！]" : ""
      }`
    );
    if (monster.getHp() == 0) {
      console.log(`[Amazon] ：啊！！！ 我竟然輸比你？！\u{1F47C}`);
      this.gainExperience(monster.getExp());
    }
  }

  switchAttack() {
    this.usePrimaryAttack = !this.usePrimaryAttack;
    // if (this.usePrimaryAttack) {
    //   this.usePrimaryAttack = false;
    // } else {
    //   this.usePrimaryAttack = true;
    // }
  }
}
export class Paladin implements Player {
  private primary: Attack;
  private secondary: Attack;
  private usePrimaryAttack: boolean;
  private exp: number;
  //@ts-ignore
  private level: number;
  constructor() {
    this.primary = new Swords(50);
    this.secondary = new MagicSpells(25);
    this.usePrimaryAttack = true;
    this.exp = 0;
    this.level = 1;
  }
  gainExperience(exp: number): void {
    this.exp += exp;
    this.level = Math.floor(25 + Math.sqrt(625 + 100 * this.exp)) / 50;
  }

  attack(monster: Monster): void {
    // Prepare to attack
    let multiplier: number = Math.random() > 0.9 ? 2 : 1;

    if (this.usePrimaryAttack) {
      // Attack now
      monster.injure(this.primary.getDamage() * multiplier);
    } else {
      // Attack now
      monster.injure(this.secondary.getDamage() * multiplier);
    }

    // Post Attack (Report, lv up, exp++)
    console.log(
      `[Paladin] 攻擊 [${monster.getName()}] (HP: ${monster.getHp()}) ${
        multiplier == 2 ? "[！一陽神功 ！]" : ""
      }`
    );
    if (monster.getHp() == 0) {
      console.log(`[Paladin] ：啊！！！ 我竟然輸比你？！\u{1F47C}`);
      this.gainExperience(monster.getExp());
    }
  }

  switchAttack() {
    this.usePrimaryAttack = !this.usePrimaryAttack;
    // if (this.usePrimaryAttack) {
    //   this.usePrimaryAttack = false;
    // } else {
    //   this.usePrimaryAttack = true;
    // }
  }
}
// Invocations of the class and its methods
const player1 = new Amazon();
const player2 = new Paladin();
const monster1 = new Monster(500, "怪獸1號", 5000, 0.1);
const monster2 = new Monster(1500, "怪獸2號", 5000, 0.1);
// console.log("player = ", player);
// console.log("monster = ", monster);
while (monster1.getHp() > 0) {
  player1.attack(monster1);
  player1.switchAttack();
}
while (monster2.getHp() > 0) {
  player2.attack(monster2);
  player2.switchAttack();
}

console.log({ player1, player2 });
