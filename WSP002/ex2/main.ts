// Declaration of Class and its methods
class Player {
  private strength: number;
  private name: string;
  //@ts-ignore
  private exp: number;
  //@ts-ignore
  private level: number;
  constructor(strength: number, name: string) {
    this.strength = strength;
    this.name = name;
    this.exp = 0;
    this.level = 0;
  }

  attack(monster: Monster) {
    let multiplier: number = Math.random() > 0.9 ? 2 : 1;
    monster.injure(this.strength * multiplier);
    console.log(
      `[${this.name}] 攻擊 [${monster.getName()}] (HP: ${monster.getHp()}) ${
        multiplier == 2 ? "[！一陽神功 ！]" : ""
      }`
    );
    if (monster.getHp() == 0) {
      console.log(`${monster.getName()} ：啊！！！ 我竟然輸比你？！\u{1F47C}`);
      this.gainExperience(monster.getExp());
    }
  }

  gainExperience(exp: number) {
    this.exp += exp;
    this.level = Math.floor(25 + Math.sqrt(625 + 100 * this.exp)) / 50;
  }
}

export class Monster {
  private hp: number;
  private name: string;
  private exp: number;
  private dodge: number;

  constructor(hp: number, name: string, exp: number, dodge: number) {
    this.hp = hp;
    this.name = name;
    this.exp = exp;
    this.dodge = dodge;
  }

  injure(strength: number) {
    if (Math.random() > this.dodge) {
      let targetHp = this.hp - strength;
      this.hp = Math.max(0, targetHp);
    } else {
      console.log(`${this.name} ：哈！就憑你？我一野就避開左啦\u{1F618}`);
    }
  }
  getHp() {
    return this.hp;
  }
  getExp() {
    return this.exp;
  }
  getName() {
    return this.name;
  }
  // Think of how to write injure
}

export function gameStart() {
  // Invocations of the class and its methods
  const player = new Player(20, "勇者阿暉");
  const monster1 = new Monster(500, "怪獸一號", 5000, 0.1);
  const monster2 = new Monster(1000, "怪獸二號", 1000, 0.2);
  const monster3 = new Monster(200, "怪獸三號", 1000, 0.3);
  // console.log("player = ", player);
  // console.log("monster = ", monster);
  while (monster1.getHp() > 0) {
    player.attack(monster1);
  }
  while (monster2.getHp() > 0) {
    player.attack(monster2);
  }
  while (monster3.getHp() > 0) {
    player.attack(monster3);
  }
  console.log("player = ", player);
  // English counterpart: Player attacks monster
}
