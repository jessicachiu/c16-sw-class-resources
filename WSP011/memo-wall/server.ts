import express from "express";
import expressSession from "express-session";
import { isLoggedIn } from "./guard";
import http from "http";
import { Server as SocketIO } from "socket.io";
import dotenv from "dotenv";
import grant from "grant";
import { setSocketIO } from "./socketio";
import memoRoutes from "./router/memoRoutes";
import { logger } from "./logger";
import userRoutes from "./router/userRoutes";
import { dummyCounter, pageNotFound, requestLogger } from "./middlewares";
import { env } from "./env";
dotenv.config();

const grantExpress = grant.express({
    defaults: {
        origin: "http://localhost:8080",
        transport: "session",
        state: true,
    },
    google: {
        key: env.GOOGLE_CLIENT_ID || "",
        secret: env.GOOGLE_CLIENT_SECRET || "",
        scope: ["profile", "email"],
        callback: "/login/google",
    },
});

/////////////////////  Set up
export const app = express();
const server = new http.Server(app);
const io = new SocketIO(server);
setSocketIO(io);

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use(
    expressSession({
        secret: "Tecky Academy teaches typescript",
        resave: true,
        saveUninitialized: true,
    })
);

app.use(grantExpress as express.RequestHandler);
app.use(requestLogger, dummyCounter);
app.use(userRoutes);
app.use(memoRoutes);
app.use(express.static("public"));
app.use(express.static("uploads"));
app.use("/admin", isLoggedIn, express.static("protected"));
app.use(pageNotFound);

const PORT = env.PORT;

server.listen(PORT, () => {
    logger.info(`Server準備好喇： http://localhost:${PORT}/`);
});
