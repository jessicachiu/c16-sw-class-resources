

window.onload = ()=>{
    //係client 部機run 嘅嘢
    console.log("In frontend");
    loadMemos();
}


document.querySelector('#memo-form').addEventListener('submit',async function(event){
    event.preventDefault();

    const form = event.target;

    // 1. JSON Object
    // const formObj = {
    //     content: form.content.value
    // };

    // const res = await fetch('/memos',{
    //     method:"POST",
    //     headers:{
    //         "Content-Type":"application/json"
    //     },
    //     body: JSON.stringify(formObj)
    // });

    const formData = new FormData();
    formData.append('content',form.content.value);
    formData.append('image',form.image.files[0]);

    const res = await fetch('/memos',{
        method:"POST",
        body: formData
    });

    const result = await res.json();

    if(res.status == 200){
        // guaranteed 返個Response 真係啱嘅response
        console.log(result);
    }
    loadMemos();
});

document.querySelector('#login-form').onsubmit = async function(event){
    event.preventDefault();

    const form = event.target;

    const formObj = {
        username: form.username.value,
        password: form.password.value
    }

    const res = await fetch('/login',{
        method:"POST",
        headers:{
            "Content-Type":"application/json"
        },
        body: JSON.stringify(formObj)
    });

    const result = await res.json();
    if(res.status === 200){
        window.location = '/admin.html';
    }else{
        document.querySelector('#alert-container')
            .innerHTML = `<div class="alert alert-danger" role="alert">
                Login Failed! ${result.msg}
          </div>`;
    }
}

// method 2: 只係加係memo-board，然後check 返到底撳咗啲咩
// document.querySelector('#memo-board').addEventListener('click',async (event)=>{
//     if(event.target.matches('.bi-pencil-square')){
//         // Update logic
//         const pencilSquare = event.target;
//         const memo = pencilSquare.closest('.memo');
//         const memoId = memo.getAttribute('data-id');
//         const content = memo.innerText;

//         // 個id 係必須要的，你無呢個係做唔到任何嘢
//         // Fail early
//         const res = await fetch(`/memos/${memoId}`,{
//             method:"PUT",
//             headers:{
//                 "Content-Type":"application/json"
//             },
//             body:JSON.stringify({
//                 content:content
//             })
//         });

//         const result = await res.json();
//         loadMemos();
//     }else if(event.target.matches('.bi-trash')){
//         // Delete logic
//         const trash = event.target;
//         const memo = trash.closest('.memo');
//         const memoId = memo.getAttribute('data-id');

//         // 個id 係必須要的，你無呢個係做唔到任何嘢
//         // Fail early
//         const res = await fetch(`/memos/${memoId}`,{
//             method:"DELETE",
//         });

//         const result = await res.json();
//         loadMemos();
//     }
// });

async function loadMemos(){
    const res = await fetch('/memos'); // DEFAULT 係Get Method
    const memos = await res.json();
    
    const memoBoard = document.querySelector('#memo-board');
    memoBoard.innerHTML = '';
    for(let memo of memos){
        let imgTag = "";
        if(memo.image){
            imgTag = `<img src="/uploads/${memo.image}" 
            alt="${memo.content}" class="img-fluid"/>`;
        }
        memoBoard.innerHTML += `
        <div class="memo" data-id="${memo.id}" contenteditable>${memo.content}
            ${imgTag}
            <i class="bi bi-pencil-square"></i>
            <i class="bi bi-trash"></i>
        </div>
        `
    }

    // Method 1: 逐張memo 去加event listener
    const memoDivs = document.querySelectorAll('.memo');
    for(let memoDiv of memoDivs){
        memoDiv.querySelector('.bi-pencil-square').onclick = async function(event){
            const pencilSquare = event.target;
            const memo = pencilSquare.closest('.memo');
            const memoId = memo.getAttribute('data-id');
            const content = memo.innerText;

            // 個id 係必須要的，你無呢個係做唔到任何嘢
            // Fail early
            const res = await fetch(`/memos/${memoId}`,{
                method:"PUT",
                headers:{
                    "Content-Type":"application/json"
                },
                body:JSON.stringify({
                    content:content
                })
            });

            const result = await res.json();
            loadMemos();
        }
        memoDiv.querySelector('.bi-trash').onclick = async function(event){
            const trash = event.target;
            const memo = trash.closest('.memo');
            const memoId = memo.getAttribute('data-id');

            // 個id 係必須要的，你無呢個係做唔到任何嘢
            // Fail early
            const res = await fetch(`/memos/${memoId}`,{
                method:"DELETE",
            });

            const result = await res.json();
            loadMemos();
        }
    }
}


const socket = io.connect();

socket.on('new-memo',(data)=>{
    loadMemos();
})