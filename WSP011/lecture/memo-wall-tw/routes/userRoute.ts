import express from 'express'
import {hashPassword, checkPassword} from '../hash'
import fetch from 'node-fetch'
import { User } from '../models';
import { isLoggedIn } from '../guard';
import { client } from '../db';
export const userRoutes = express.Router();

userRoutes.get('/login/google',loginGoogle);
userRoutes.post("/register", register);
userRoutes.post("/login", login);
userRoutes.post("/users", isLoggedIn, createUser);
userRoutes.get("/logout",logout)

// 有password login 永遠都是post，因為唔想人地係url 睇到個password
// POST /login
async function login(req:express.Request, res:express.Response){
    const { username, password } = req.body;
    let users: User[] = (await client.query("SELECT * from users where username = $1", [username])).rows;
    const user = users[0];
    const checkPassworResult = await checkPassword(password,user.password)
    console.log(checkPassworResult);
    
    if (user && checkPassworResult) {
        // 先會set user落去session 度
        req.session["user"] = user;
        // 4. Login 成功後，redirect 去/admin.html
        res.status(200).json({ success: true }); // URL vs PATH
    } else {
        res.status(401).json({ success: false, msg: "Username/Password is incorrect!" });
    }
}

async function createUser (req:express.Request, res:express.Response){
    const { username, password } = req.body;

    // 實際應要check 埋user 個email 有無重覆
    await client.query("INSERT INTO users (username,password) values ($1,$2)", [username, password]);
    res.redirect("/index.html");
};
async function logout(req:express.Request, res:express.Response) {
    delete req.session["user"];
    res.redirect("/");
}

async function register(req:express.Request, res:express.Response){
    const { username, password } = req.body;
    const hashedPassword = await hashPassword(password)
    const result = await client.query("INSERT INTO users (username,password, created_at, updated_at) values ($1,$2,$3,$4) RETURNING id", [username, hashedPassword, "NOW()", "NOW()"]);
    console.log(result);
    if (result.rows) {
        res.end("OK");
    } else {
        res.end("NOT OK");
    }

}
async function loginGoogle (req:express.Request,res:express.Response){
    console.log('現在google 登入');
    
    const accessToken = req.session?.['grant'].response.access_token;
    const fetchRes = await fetch('https://www.googleapis.com/oauth2/v2/userinfo',{
        method:"get",
        headers:{
            "Authorization":`Bearer ${accessToken}`
        }
    });
    const googleUserInfo = await fetchRes.json();
    console.log('result',googleUserInfo)


    // 登入process

    const users = (await client.query(`SELECT * FROM users WHERE users.username = $1`,[googleUserInfo.email])).rows;
    const user = users[0];
    if(!user){
            // 如果無user 就加一個新user 入db 
            const result = await client.query("INSERT INTO users (username,password, created_at, updated_at) values ($1,$2,$3,$4) RETURNING id", [googleUserInfo.email, '', "NOW()", "NOW()"]);
            console.log(result);

        }else{
            // 如果user 已在，set番好 個session['user'] ，代表佢有登入
            if(req.session){
                req.session['user'] = {
                    id: user.id
                };    
            }

        }

    return res.redirect('/admin.html')
}

export default userRoutes