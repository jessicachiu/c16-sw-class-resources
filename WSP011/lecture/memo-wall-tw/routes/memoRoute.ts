import express from 'express'
import { client } from '../db';
import { isLoggedInAPI } from '../guard';
import { Memo } from '../models';
import { icon } from '../multer';
import { io } from '../socketio';

const memoRoutes = express.Router();




memoRoutes.post("/memos", icon,createMemo);
memoRoutes.get("/memos", getMemo);
memoRoutes.put("/memos/:id", isLoggedInAPI, updateMemo);
memoRoutes.delete("/memos/:id", isLoggedInAPI, deleteMemo);


async function createMemo(req : express.Request, res: express.Response) {
    // 如果你要upload file , 就用multer
    // 否則就用 urlencoded
    const content = req.body.content;
    // id 唔需要寫了
    await client.query("INSERT INTO memos (content,image) values ($1,$2)", [content, req.file?.filename]);
    io.emit("new-memo", "有新memo ar!!!");
    // const memos:Memo[] = await jsonfile.readFile(MEMO_JSON)
    // memos.push({
    //     id: memoIdCounter,
    //     content:content,
    //     image: req.file?.filename
    // });
    // memoIdCounter++;
    // await jsonfile.writeFile(MEMO_JSON,memos,{spaces:4});
    res.json({ success: true });
}
async function getMemo(req: express.Request, res: express.Response) {
    const q = req.query.q;
    if (q) {
        // 先寫好SQL，就識寫呢度
        const result = await client.query("SELECT * FROM memos WHERE content like $1", [`%${q}%`]);
        let memos: Memo[] = result.rows;
        res.json(memos);
    } else {
        const result = await client.query("SELECT * FROM memos");
        let memos: Memo[] = result.rows;
        res.json(memos);
    }

    // let memos:Memo[] = await jsonfile.readFile(MEMO_JSON)

    // if(q){
    //     // Big(O)? O(n)
    //     memos = memos.filter((memo)=> memo.content.includes(q +""));
    // }
}
async function updateMemo(req: express.Request, res: express.Response) {
    const id = parseInt(req.params.id);
    if (isNaN(id)) {
        res.status(400).json({ msg: "id is not an integer" });
        return;
    }
    const content = req.body.content;

    await client.query(`UPDATE memos set content = $1 WHERE id = $2`, [content, id]);
    // const memos:Memo[] = await jsonfile.readFile(MEMO_JSON)
    // // 去搵返相對應嘅id 去update 返
    // for(let memo of memos){
    //     if(memo.id === id){
    //         memo.content = content
    //     }
    // }
    // await jsonfile.writeFile(MEMO_JSON,memos,{spaces:4});
    res.json({ success: true });
}
async function deleteMemo(req: express.Request, res: express.Response) {
    const id = parseInt(req.params.id);
    if (isNaN(id)) {
        res.status(400).json({ msg: "id is not an integer" });
        return;
    }
    await client.query("DELETE from memos where id = $1", [id]);
    // const memos:Memo[] = await jsonfile.readFile(MEMO_JSON);
    // const newMemos = memos.filter(memo=>{
    //     return memo.id !== id; // 唔係呢個id 就留低，否則留唔低
    // })
    // await jsonfile.writeFile(MEMO_JSON,newMemos,{spaces:4});
    res.json({ success: true });
}

export default memoRoutes