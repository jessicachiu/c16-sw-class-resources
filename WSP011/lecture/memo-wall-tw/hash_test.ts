import {hashPassword, checkPassword} from './hash'

async function main(){
    let testInput = '123456'
    const hash = await hashPassword(testInput)
    console.log('hash = ', hash);

    const isValidUser =  await checkPassword(testInput,hash)
    console.log('isValidUser 1= ', isValidUser);

    console.log('isValidUser 2= ',await checkPassword('password',hash));
    

    

}

main()