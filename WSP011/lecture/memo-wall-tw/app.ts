import express from "express";
import expressSession from "express-session";
import { isLoggedIn } from "./guard";
import dotenv from "dotenv";
import { Server as SocketIO } from "socket.io";
import http from "http";
import grant from 'grant'
import { userRoutes } from "./routes/userRoute";
import memoRoutes from "./routes/memoRoute";
import { setSocketIO } from "./socketio";

dotenv.config();

const app = express();
const server = http.createServer(app);

export const io = new SocketIO(server);
setSocketIO(io)



app.use(express.json());
// extended allow you to use more syntax
app.use(express.urlencoded({ extended: true })); // 佢係唔識讀multipart form-data

app.use(
    expressSession({
        secret: "Memo wall",
        resave: true,
        saveUninitialized: true,
    })
);


const grantExpress = grant.express({
    "defaults":{
        "origin": "http://localhost:8080",
        "transport": "session",
        "state": true,
    },
    "google":{
        "key": process.env.GOOGLE_CLIENT_ID || "",
        "secret": process.env.GOOGLE_CLIENT_SECRET || "",
        "scope": ["profile","email"],
        "callback": "/login/google"
    }
});

app.use(grantExpress as express.RequestHandler);





// Counter Middleware
app.use((req, res, next) => {
    // 1. 去check req.session['counter']有無initialized 係0 ，無嘅話就set 做0
    if (!req.session["counter"]) {
        req.session["counter"] = 0;
    }
    // 2. 將個counter + 1
    req.session["counter"] += 1;
    // console.log(req.session);
    next(); // 繼續畀下個middleware 做嘢
});

// request access log
app.use((req, res, next) => {
    const now = new Date();
    // YYYY-mm-dd HH:MM:ss Date format
    const dateString = now.getFullYear() + "-" + pad(now.getMonth() + 1) + "-" + pad(now.getDate()) + " " + pad(now.getHours()) + ":" + pad(now.getMinutes()) + ":" + pad(now.getSeconds());

    console.log(`[${dateString}] Request ${req.path}`);

    next();
});

app.use(memoRoutes)
app.use(userRoutes)


// 留意: 唔好Public 同protected 有同樣嘅名 and path嘅 file
app.use(express.static("public"));
app.use(isLoggedIn, express.static("protected"));

app.use((req, res) => {
    //行到呢個位，其實即係乜handler都唔中
    res.redirect("/404.html"); // 轉去404.html 呢個url
});

io.on("connection", (socket) => {});

const PORT = 8080;
server.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}`);
});

function pad(num: number) {
    return (num + "").padStart(2, "0");
}
