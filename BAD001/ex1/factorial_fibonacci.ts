export function factorial(num:number):number{
    if (!Number.isInteger(num) || num < 0){
        throw new Error("Invalid Input!");
    }
    if(num == 0 || num == 1){
        return 1;
     }
    return factorial(num -1 )* num
}

export function fibonacci(num:number):number{
    //1,1,2,3,5,8,13,....
    if(num == 1 || num == 2){
        return 1;
     }
     return fibonacci(num-1)+ fibonacci(num -2);
}