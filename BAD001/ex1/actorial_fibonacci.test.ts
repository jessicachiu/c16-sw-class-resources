import {factorial,fibonacci} from './factorial_fibonacci';

describe("factorial",()=>{
    test("if number 1",()=>{
        expect(factorial(1)).toEqual(1);
    });
    test("if number 2",()=>{
        expect(factorial(2)).toEqual(2);
    });
    test("if number 5",()=>{
        expect(factorial(5)).toEqual(120);
    });

    test("Test Invalid Input",()=>{
        expect(()=>{factorial(-1);}).toThrow("Invalid Input!");
    });

    test("Test Float Point",()=>{
        expect(()=>{factorial(1.1);}).toThrow("Invalid Input!");
    });
});

describe("fibonacci",()=>{
    test("Sum of 1 element",()=>{
        expect(fibonacci(1)).toEqual(1);
    });
    test("Sum of 3 elements",()=>{
        expect(fibonacci(4)).toEqual(3);
    });
    test("Sum of 5 elements",()=>{
        expect(fibonacci(6/*position*/)).toEqual(8);
    });
})