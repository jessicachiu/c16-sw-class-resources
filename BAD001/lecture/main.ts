export function add(a: number, b: number) {
    return a + b;
}

export function minus(a: number, b: number) {
    return a - b;
}
export function multiply(a: number, b: number) {
    if (a == 0 || b == 0) {
        return 0;
    }
    return a * b;
}
export function divide(a: number, b: number) {
    return a / b;
}
export function power(a: number, b: number) {
    return a ** b;
}
export function squareRoot(a: number) {
    if (a < 0) {
        throw new Error("Invalid Input");
    }
    return Math.sqrt(a);
}
export function wrong() {
    throw new Error("Errot in wrong function");
}

export function demoCall() {
    try {
        wrong();
    } catch (error) {
        console.log(error);
    }
}

// console.log(add(3, 2));
// console.log(add(0.2, 0.7));
// console.log("adding 3 and 2000000 ", add(3, 20000000000));
