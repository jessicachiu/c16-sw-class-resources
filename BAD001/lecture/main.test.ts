import { add, minus, multiply, squareRoot, wrong } from "./main";

describe("Testing add function", () => {
    it("can add 2 numbers", () => {
        expect(add(2, 3)).toBe(5);
    });
    it("can add 2 float", () => {
        expect(add(0.2, 0.1)).toBeCloseTo(0.3);
        expect(add(0.2, 0.3)).not.toBe(0.3);
    });
    test("can add 2 negative numbers", () => {
        expect(add(-2, -3)).toBe(-5);
    });
});

describe("Testing minus function", () => {
    it("can add 2 +ve numbers", () => {
        expect(minus(2, 3)).toBe(-1);
    });
    it("can minus 2 -ve numbers", () => {
        expect(minus(-2, -3)).toBe(1);
    });
});

describe("Testing multiply function", () => {
    it("can multiply 2 +ve numbers", () => {
        expect(multiply(2, 3)).toBe(6);
    });
    it("can multiply 0 numbers", () => {
        expect(multiply(-2, 0)).toBe(0);
    });
});
describe("Testing square root ", () => {
    it("can squareRoot numbers", () => {
        expect(squareRoot(4)).toBe(2);
    });
    it("can squareRoot numbers with error", () => {
        expect(() => {
            squareRoot(-4);
        }).toThrowError("Invalid Input");
    });
});

describe("Testing error function", () => {
    it("can handle throw function", () => {
        expect(() => {
            wrong();
        }).toThrowError();
    });
});

// Skipped
xdescribe("Example testing", () => {
    it("object assignment", () => {
        const data = { one: 1 };
        data["two"] = 2;
        expect(data).toEqual({ one: 1, two: 2 });
        expect(data).toBe({ one: 1, two: 2 });
    });

    test("there is no I in team", () => {
        expect("team").not.toMatch(/I/);
    });

    test('but there is a "stop" in Christoph', () => {
        expect("Christoph").toMatch(/stop/);
    });

    const users = [
        { id: 1, name: "Hugo" },
        { id: 2, name: "Francesco" },
    ];

    test("we should have ids 1 and 2", () => {
        expect(users).toEqual(expect.arrayContaining([expect.objectContaining({ id: 1 }), expect.objectContaining({ id: 2 })]));
    });

    test("id should match", () => {
        const obj = {
            id: "111",
            productName: "Jest Handbook",
            url: "https://jesthandbook.com",
        };
        expect(obj.id).toEqual("111");
    });
});
