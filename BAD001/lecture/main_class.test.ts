import Calculator from "./main_class";

describe("Test on calculator", () => {
    let calculator: Calculator;
    let luckyNumbers: number[];

    // For 以下每個test case 開始前既準備功夫，每個test case 前都行
    beforeEach(() => {
        calculator = new Calculator();
    });

    // For 以下所有test case 開始前既準備功夫，只行一次
    beforeAll(() => {
        luckyNumbers = [1, 3, 5, 7, 12, 44];
    });

    it("can calculate to lucky number", () => {
        expect(luckyNumbers).toContain(calculator.add(22, 22));
        expect(luckyNumbers).toContain(calculator.minus(10, 3));
        expect(luckyNumbers).toContain(calculator.squareRoot(9));
        expect(luckyNumbers).not.toContain(calculator.squareRoot(121));
        expect(luckyNumbers).not.toContain(calculator.squareRoot(255));
    });

    test("can sqrt", () => {
        expect(calculator.squareRoot(9)).toBe(3);
        expect(() => {
            calculator.squareRoot(-100);
        }).toThrowError("Invalid Input");
    });

    it("can add to display", () => {
        calculator.addToCalculator(5);
        calculator.addToCalculator(5);
        calculator.addToCalculator(5);
        expect(calculator.getDisplayNumber()).toBe(15);
    });

    it("can add to display", () => {
        calculator.minusToCalculator(5);
        calculator.minusToCalculator(5);
        calculator.minusToCalculator(5);
        expect(calculator.getDisplayNumber()).toBe(-15);
    });
});
