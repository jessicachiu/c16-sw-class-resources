class Calculator {
    private displayNumber: number;

    constructor() {
        this.displayNumber = 0;
    }

    addToCalculator = (a: number) => {
        this.displayNumber += a;
    };

    minusToCalculator = (a: number) => {
        this.displayNumber -= a;
    };

    add = (a: number, b: number) => {
        return a + b;
    };
    minus = (a: number, b: number) => {
        return a - b;
    };
    multiply = (a: number, b: number) => {
        if (a == 0 || b == 0) {
            return 0;
        }
        return a * b;
    };
    divide = (a: number, b: number) => {
        return a / b;
    };
    power = (a: number, b: number) => {
        return a ** b;
    };
    squareRoot = (a: number) => {
        if (a < 0) {
            throw new Error("Invalid Input");
        }
        return Math.sqrt(a);
    };
    getDisplayNumber() {
        return this.displayNumber;
    }
}
export default Calculator;
