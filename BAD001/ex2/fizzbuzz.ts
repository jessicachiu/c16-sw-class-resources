//Time:        5.663 s
export function fizzbuzz(num:number){
    let str = '';
    for(let round = 1; round<=num; round++){
        if (round%3 == 0 && round%5 == 0){
            //Fizz Buzz
            str = str + 'Fizz Buzz, '
        }else if (round%3 == 0){
            //Fuzz
            str = str + 'Fizz, '
        }else if (round%5 == 0){
            //Buzz
            str = str + 'Buzz, '
        }else{
            //number
            str = str + `${round}, `
        }
    }
    return str;
}

//Time:        2.653 s, estimated 3 s
export function fizzbuzzClosure(){
    let round = 1;
    let str = '';
    return ()=>{
        if (round%3 == 0 && round%5 == 0){
            //Fizz Buzz
            str = str + 'Fizz Buzz, '
        }else if (round%3 == 0){
            //Fuzz
            str = str + 'Fizz, '
        }else if (round%5 == 0){
            //Buzz
            str = str + 'Buzz, '
        }else{
            //number
            str = str + `${round}, `
        }
        round = round + 1;
        return str;
    }   
}