import {/*fizzbuzz,*/fizzbuzzClosure} from './fizzbuzz';

/*describe("test FizzBuzz",()=>{
    test("If 1",()=>{
        expect(fizzbuzz(1)).toEqual("1, ");
    });
    test("If 2",()=>{
        expect(fizzbuzz(2)).toEqual("1, 2, ");
    });
    test("If 3",()=>{
        expect(fizzbuzz(3)).toEqual("1, 2, Fizz, ");
    });
    test("If 5",()=>{
        expect(fizzbuzz(5)).toEqual("1, 2, Fizz, 4, Buzz, ");
    });

    test("If 11",()=>{
        expect(fizzbuzz(11)).toEqual("1, 2, Fizz, 4, Buzz, Fizz, 7, 8, Fizz, Buzz, 11, ");
    });

    test("If 15",()=>{
        expect(fizzbuzz(15)).toEqual("1, 2, Fizz, 4, Buzz, Fizz, 7, 8, Fizz, Buzz, 11, Fizz, 13, 14, Fizz Buzz, ");
    });

    test("If 35",()=>{
        expect(fizzbuzz(35)).toEqual("1, 2, Fizz, 4, Buzz, Fizz, 7, 8, Fizz, Buzz, 11, Fizz, 13, 14, Fizz Buzz, 16, 17, Fizz, 19, Buzz, Fizz, 22, 23, Fizz, Buzz, 26, Fizz, 28, 29, Fizz Buzz, 31, 32, Fizz, 34, Buzz, ");
    });

});*/

describe("Test FizzBuzz Closure",()=>{
    let fizzbuzz:()=>string;
    beforeAll(()=>{
        fizzbuzz = fizzbuzzClosure();
    });
    test("If 1",()=>{
        expect(fizzbuzz()).toEqual("1, ");
    });
    test("If 2",()=>{
        expect(fizzbuzz()).toEqual("1, 2, ");
    });
    test("If 3",()=>{
        expect(fizzbuzz()).toEqual("1, 2, Fizz, ");
    });
    test("If 5",()=>{
        fizzbuzz();//4
        expect(fizzbuzz()/*5*/).toEqual("1, 2, Fizz, 4, Buzz, ");
    });

    test("If 11",()=>{
        let result = '';
        for(let r=6;r<=11;r++){
            result = fizzbuzz();
        }
        expect(result).toEqual("1, 2, Fizz, 4, Buzz, Fizz, 7, 8, Fizz, Buzz, 11, ");
    });

    test("If 15",()=>{
        let result = '';
        for(let r=12;r<=15;r++){
            result = fizzbuzz();
        }
        expect(result).toEqual("1, 2, Fizz, 4, Buzz, Fizz, 7, 8, Fizz, Buzz, 11, Fizz, 13, 14, Fizz Buzz, ");
    });

    test("If 35",()=>{
        let result = '';
        for(let r=16;r<=35;r++){
            result = fizzbuzz();
        }
        expect(result).toEqual("1, 2, Fizz, 4, Buzz, Fizz, 7, 8, Fizz, Buzz, 11, Fizz, 13, 14, Fizz Buzz, 16, 17, Fizz, 19, Buzz, Fizz, 22, 23, Fizz, Buzz, 26, Fizz, 28, 29, Fizz Buzz, 31, 32, Fizz, 34, Buzz, ");
    });
});