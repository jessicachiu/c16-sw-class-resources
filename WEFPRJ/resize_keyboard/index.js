const unitLength = 20;
const boxColor = 150;
const strokeColor = 50;
let columns; /* To be determined by window width*/
let rows; /* To be determined by window height */
let currentBoard;
let nextBoard;
let newBoard;
let keyboardMode = false;
let keyboardIndex = { x: 0, y: 0 };
let keyboardBoard;
let keyboardDragMode = false;
fr = 20;
function setup() {
  /* Set the canvas to be under the element #canvas*/
  const canvas = createCanvas(windowWidth, windowHeight - 100);
  canvas.parent(document.querySelector("#canvas"));

  /*Calculate the number of columns and rows */
  columns = floor(width / unitLength) - 1;
  rows = floor(height / unitLength) - 1;
  frameRate(fr);
  // columns = 20
  // rows = 20

  /*Making both currentBoard and nextBoard 2-dimensional matrix that has (columns * rows) boxes. */
  currentBoard = [];
  nextBoard = [];
  newBoard = [];
  keyboardBoard = [];
  for (let i = 0; i < columns; i++) {
    currentBoard[i] = [];
    nextBoard[i] = [];
    newBoard[i] = [];
    keyboardBoard[i] = [];
  }
  // Now both currentBoard and nextBoard are array of array of undefined values.
  init(); // Set the initial values of the currentBoard and nextBoard
}

function reSetup() {
  /* Set the canvas to be under the element #canvas*/
  const canvas = createCanvas(windowWidth, windowHeight - 100);
  canvas.parent(document.querySelector("#canvas"));

  /*Calculate the number of columns and rows */
  columns = floor(width / unitLength) - 1;
  rows = floor(height / unitLength) - 1;

  // Now both currentBoard and nextBoard are array of array of undefined values.
  reInit(); // Set the initial values of the currentBoard and nextBoard
}

function init() {
  let newBoard = [];
  for (let i = 0; i < columns; i++) {
    for (let j = 0; j < rows; j++) {
      currentBoard[i][j] = 0;
      nextBoard[i][j] = 0;
      keyboardBoard[i][j] = 0;
    }
  }
}

function reInit() {
  // console.log("columns = ", columns);
  // console.log("rows = ", rows);
  for (let i = 0; i < columns; i++) {
    newBoard[i] = [];
    keyboardBoard[i] = [];
  }
  try {
    for (let i = 0; i < columns; i++) {
      for (let j = 0; j < rows; j++) {
        newBoard[i][j] = currentBoard[i][j];
      }
    }
  } catch (error) {
    console.log("suppress");
  }

  currentBoard = newBoard.slice(0);
  nextBoard = newBoard.slice(0);

  // console.log("currentBoard = ", currentBoard);
}

function generate() {
  // console.log('--------- BEFORE -----------')
  // myPrint()
  //Loop over every single box on the board
  for (let x = 0; x < columns; x++) {
    for (let y = 0; y < rows; y++) {
      // Count all living members in the Moore neighborhood(8 boxes surrounding)
      let neighbors = 0;
      for (let i of [-1, 0, 1]) {
        for (let j of [-1, 0, 1]) {
          if (i === 0 && j === 0) {
            // the cell itself is not its own neighbor
            continue;
          }
          // The modulo operator is crucial for wrapping on the edge
          if (
            currentBoard[(x + i + columns) % columns][(y + j + rows) % rows] > 0
          ) {
            neighbors++;
          }
          // neighbors += currentBoard[(x + i + columns) % columns][(y + j + rows) % rows];
        }
      }

      // Rules of Life
      if (currentBoard[x][y] > 0 && neighbors < 2) {
        // Die of Loneliness
        nextBoard[x][y] = 0;
      } else if (currentBoard[x][y] > 0 && neighbors > 3) {
        // Die of Overpopulation
        nextBoard[x][y] = 0;
      } else if (currentBoard[x][y] == 0 && neighbors == 3) {
        // New life due to Reproduction
        nextBoard[x][y] = 0.4;
      } else {
        // Stasis
        nextBoard[x][y] =
          currentBoard[x][y] * 1.02 > 1 ? 1 : currentBoard[x][y] * 1.02;
      }
    }
  }

  // Swap the nextBoard to be the current Board
  [currentBoard, nextBoard] = [nextBoard, currentBoard];
  // currentBoard = nextBoard
  // console.log('--------- AFTER -----------')

  // myPrint()
}

function myPrint() {
  console.log(`------------ CURRENT BOARD ------------`);
  for (currentBoardItem of currentBoard) {
    console.log(currentBoardItem);
  }
  console.log(`------------ NEXT BOARD ------------`);
  for (nextBoardItem of nextBoard) {
    console.log(nextBoardItem);
  }
}

function draw() {
  background(255);
  generate();
  console.log("draw");
  for (let i = 0; i < columns; i++) {
    for (let j = 0; j < rows; j++) {
      if (currentBoard[i][j] == 0) {
        // fill(boxColor);
        fill(255);
      } else {
        // fill(boxColor);
        fill(`rgba(86, 145, 204,${currentBoard[i][j]})`);
      }
      stroke(strokeColor);
      rect(i * unitLength, j * unitLength, unitLength, unitLength);
    }
  }
}

function mouseDragged() {
  /**
   * If the mouse coordinate is outside the board
   */
  if (mouseX > unitLength * columns || mouseY > unitLength * rows) {
    return;
  }
  const x = Math.floor(mouseX / unitLength);
  const y = Math.floor(mouseY / unitLength);
  currentBoard[x][y] = 1;
  console.log(x, y, unitLength);
  fill(boxColor);
  stroke(strokeColor);
  rect(x * unitLength, y * unitLength, unitLength, unitLength);
}

/**
 * When mouse is pressed
 */
function mousePressed() {
  noLoop();
  mouseDragged();
}

/**
 * When mouse is released
 */
function mouseReleased() {
  loop();
}
function windowResized() {
  noLoop();
  resizeCanvas(windowWidth - 100, windowHeight - 100, true);
  reSetup();
  loop();
  // init();
}
document.onkeydown = checkKeyDown;
document.onkeyup = checkKeyUp;
function checkKeyUp(e) {
  e = e || window.event;
  // console.log("keyboardIndex = ", keyboardIndex, e.keyCode);
  switch (e.keyCode) {
    case 32: // drag mode enable
      keyboardDragMode = false;
      break;
  }
}

function checkKeyDown(e) {
  e = e || window.event;
  // console.log("keyboardIndex = ", keyboardIndex, e.keyCode);

  if (e.keyCode == 75) {
    keyboardMode = !keyboardMode;
    if (keyboardMode) {
      console.log("keyboard mode acticated");
      frameRate(0);
      noLoop();
      keyboardBoard = currentBoard.slice(0);
      drawKeyboardCursor();
    } else {
      console.log("keyboard mode NOT acticated");

      for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
          currentBoard[i][j] = keyboardBoard[i][j];
          keyboardBoard[i][j] = 0;
        }
      }
      frameRate(20);
      loop();
    }
  }
  if (keyboardMode) {
    switch (e.keyCode) {
      case 32: // drag mode enable
        keyboardDragMode = true;
        break;
      case LEFT_ARROW:
        keyboardIndex.x = (keyboardIndex.x - 1 + columns) % columns;
        drawKeyboardCursor();
        break;

      case RIGHT_ARROW:
        keyboardIndex.x = (keyboardIndex.x + 1 + columns) % columns;
        drawKeyboardCursor();
        break;
      case DOWN_ARROW:
        keyboardIndex.y = (keyboardIndex.y + 1 + rows) % rows;
        drawKeyboardCursor();
        break;
      case UP_ARROW:
        keyboardIndex.y = (keyboardIndex.y - 1 + rows) % rows;
        drawKeyboardCursor();
        break;
    }
  }
}

function drawKeyboardCursor() {
  if (!keyboardMode) {
    return;
  }
  if (keyboardDragMode) {
    keyboardBoard[keyboardIndex.x][keyboardIndex.y] = 1;
  }
  for (let i = 0; i < columns; i++) {
    for (let j = 0; j < rows; j++) {
      if (keyboardBoard[i][j] == 0) {
        fill(255);
      } else {
        fill(`rgba(86, 145, 204,1)`);
      }

      if (i == keyboardIndex.x && j == keyboardIndex.y) {
        fill("red");
      }

      stroke(strokeColor);
      // console.log("rect");
      rect(i * unitLength, j * unitLength, unitLength, unitLength);
    }
  }
}
