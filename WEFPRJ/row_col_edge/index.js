let columns = 20;
let rows = 20;

let currentBoard = [];
let boardElem = document.querySelector("#board");
let pivotXElem = document.querySelector("#pivotX");
let pivotYElem = document.querySelector("#pivotY");
let columnsElem = document.querySelector("#columns");
let rowsElem = document.querySelector("#rows");
let neighbourElem = document.querySelector("#neighbour");

function init() {
  for (let i = 0; i < columns; i++) {
    currentBoard[i] = [];
  }

  for (let i = 0; i < columns; i++) {
    for (let j = 0; j < rows; j++) {
      currentBoard[i][j] = Math.random() < 0.2 ? 1 : 0;
    }
  }
  columnsElem.value = columns;
  rowsElem.value = rows;
}

function draw() {
  let htmlString = "";
  for (let i = 0; i < columns; i++) {
    // Indicator column
    if (i == 0) {
      htmlString += '<div class="col">';
      htmlString += `<div class='square-indicator'></div>`;

      for (let j = 0; j < rows; j++) {
        htmlString += `<div class='square-indicator'>${j}</div>`;
      }
      htmlString += "</div>";
    }

    // Board column
    htmlString += '<div class="col">';
    for (let j = 0; j < rows; j++) {
      // Indicator row
      if (j == 0) {
        htmlString += `<div class='square-indicator'>${i}</div>`;
      }
      // Board row
      if (currentBoard[i][j] == 1) {
        htmlString += `<div class="square filled" id="square_${i}_${j}"></div>`;
      } else {
        htmlString += `<div class="square" id="square_${i}_${j}"></div>`;
      }
    }
    htmlString += "</div>";
  }
  boardElem.innerHTML = htmlString;
  addEventOnSquare();
}
function actionOnNeighbour(pivotElem, action) {
  let id = pivotElem.getAttribute("id");
  let pivot_x = parseInt(id.split("_")[1]);
  let pivot_y = parseInt(id.split("_")[2]);

  // hightlight target neighbour
  let neighbour_x;
  let neighbour_y;
  let neighbourCount = 0;
  for (let i of [-1, 0, 1]) {
    for (let j of [-1, 0, 1]) {
      if (i === 0 && j === 0) {
        // the cell itself is not its own neighbor
        continue;
      }
      neighbour_x = (pivot_x + i + columns) % columns;
      neighbour_y = (pivot_y + j + rows) % rows;
      if (currentBoard[neighbour_x][neighbour_y] == 1) {
        neighbourCount++;
      }

      let neighbourElem = document.querySelector(
        `#square_${neighbour_x}_${neighbour_y}`
      );
      if (action == "hightlight") {
        neighbourElem.classList.add("selected-neighbour");
      }
      if (action == "clean") {
        neighbourElem.classList.remove("selected-neighbour");
      }
    }
  }
  neighbourElem.value = neighbourCount;
}
function addEventOnSquare() {
  let squareElems = document.querySelectorAll(".square");

  for (let squareElem of squareElems) {
    let id = squareElem.getAttribute("id");
    let pivot_x = parseInt(id.split("_")[1]);
    let pivot_y = parseInt(id.split("_")[2]);

    squareElem.addEventListener("mouseenter", function (event) {
      squareElem.innerHTML = `(${pivot_x},${pivot_y})`;
      pivotXElem.value = pivot_x;
      pivotYElem.value = pivot_y;
      actionOnNeighbour(squareElem, "hightlight");
    });
    squareElem.addEventListener("mouseleave", function (event) {
      squareElem.innerHTML = "";
      actionOnNeighbour(squareElem, "clean");
    });
  }
}
init();
draw();

// let toogleElem = document.querySelector("#toggle-btn");
// toogleElem.addEventListener("click", function (event) {
//   let action = toogleElem.innerHTML.toLocaleLowerCase();
//   console.log("action: ", action);

// });
