
from playwright.sync_api import sync_playwright




with sync_playwright() as p:
    browser = p.chromium.launch(headless=False)
    page = browser.new_page()

    page.goto("https://www.hko.gov.hk/en/index.html")
    index_text = page.evaluate("document.querySelector('body').innerText")

    page.goto("https://www.hko.gov.hk/en/wxinfo/currwx/fnd.htm")
    fnd_text = page.evaluate("document.querySelector('body').innerText")
    
    with open('en_hko.txt','w') as f:
        f.write(index_text)
        f.write(fnd_text)


    page.goto("https://www.hko.gov.hk/tc/index.html")
    zh_hk_index_text = page.evaluate("document.querySelector('body').innerText")

    page.goto("https://www.hko.gov.hk/tc/wxinfo/currwx/fnd.htm")
    zh_hk_fnd_text = page.evaluate("document.querySelector('body').innerText")
    
    with open('zh-hk_hko.txt','w') as f:
        f.write(zh_hk_index_text)
        f.write(zh_hk_fnd_text)

    browser.close()