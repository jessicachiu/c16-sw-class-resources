import express from "express";
import expressSession from "express-session";
import { format } from "fecha";
// import { Request, Response } from "express";

/////////////////////  Set up
const app = express();

app.use(
    expressSession({
        secret: "Tecky Academy teaches typescript",
        resave: true,
        saveUninitialized: true,
    })
);

//////////////////////  Main Route / Logic

app.use((req, res, next) => {
    // [2018-11-12 09:21:31] Request /index.js
    let dateTimeString = format(new Date(), "YYYY-MM-DD HH:mm:ss");
    console.log(`[${dateTimeString}] ${req.method} ${req.path}`);
    next();
});
app.use((req, res, next) => {
    let counter = req.session["counter"] || 0;
    counter++;
    req.session["counter"] = counter;
    console.log(` 你上左黎 : ${req.session["counter"]}`);
    next();
});

app.use(express.static("public"));

app.use((req, res, next) => {
    res.redirect("/404.html");
});

const PORT = 8080;

app.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}/`);
});
