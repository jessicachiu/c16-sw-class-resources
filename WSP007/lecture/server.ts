import express, { Request, Response } from "express";
import jsonfile from "jsonfile";
import path from "path";
import multer from "multer";

// Config
const app = express();

//multer Config
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.resolve("./uploads"));
    },
    filename: function (req, file, cb) {
        cb(
            null,
            `${file.fieldname}-${Date.now()}.${file.mimetype.split("/")[1]}`
        );
    },
});
const upload = multer({ storage });

interface User {
    name: string;
    age: number;
    email: string;
    description?: string;
}

// 將你d Data 拆好  放係req.body
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

// Routes

// Middleware 去睇每一個req 既內容
app.use((req, res, next) => {
    console.log(
        `${req.method} : ${req.path} : query  = ${JSON.stringify(
            req.query
        )} : body = ${JSON.stringify(req.body)}}`
    );
    next();
});

app.get(
    "/students/:classId/subject/:subject",
    function (req: Request, res: Response) {
        console.log(`params = ${JSON.stringify(req.params)}`);
        const { classId } = req.params;
        let subject = req.params.subject;
        console.log("Get students success classid= ", classId, subject);
        res.end("Get students success Yeah");
    }
);

app.post("/students", async function (req, res) {
    const users: User[] = await jsonfile.readFile(path.resolve("./users.json"));
    const { name, age, email, description } = req.body;
    if (!name || !age) {
        res.status(401).json({
            data: "data 錯",
            code: "SYS00017",
        });
        return;
    }
    users.push({
        name,
        age,
        email,
        description,
    });
    await jsonfile.writeFile(path.resolve("./users.json"), users, {
        spaces: 4,
    });
    res.end(`成功加 student ${name} : ${age}`);
});

app.post("/students/test", function (req, res) {
    const { name, studentId, age } = req.body;
    console.table({ name, studentId, age });
    if (age > 20) {
        res.status(402).json({
            data: "唔適合",
            code: "SYS00017",
        });
    } else {
        res.end("OK post");
    }
});

app.post("/application", upload.single("profile"), async (req, res, next) => {
    try {
        const users: User[] = await jsonfile.readFile(
            path.resolve("./users.json")
        );
        const { name, age, email, description } = req.body;
        if (!name || !email) {
            res.status(401).json({
                data: "data 錯",
                code: "SYS00017",
            });
            return;
        }
        users.push({
            name,
            age,
            email,
            description,
        });
        await jsonfile.writeFile(path.resolve("./users.json"), users, {
            spaces: 4,
        });
        // 唔好咁寫, refresh 會不停 send form
        //res.sendFile(path.resolve("./public/index.html"));
        console.log("file =", req.file);
        console.log("files =", req.files);

        res.end("OK");
        // res.redirect("/");
    } catch (error) {
        res.end(JSON.stringify(error));
    }

    /*....*/
});

app.use(express.static("public"));

// Static 開機用
const PORT = 8081;
app.listen(PORT, () => {
    console.log(`Listening on : http://localhost:${PORT}`);
});
