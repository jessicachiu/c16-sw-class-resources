import express from "express";
import expressSession from "express-session";
import { format } from "fecha";
import { Request, Response } from "express";
import multer from "multer";
import jsonfile from 'jsonfile';
import {isLoggedIn} from './guard';

const storage = multer.diskStorage({
    destination:function(req,file,cb){/* cb = callback */
        cb(null,"uploads")
    },
    filename:function(req,file,cb){
        cb(null,`memo-image-${file.fieldname}.${file.mimetype.split('/')[1]}`)
    }
})
const upload = multer({storage});

/////////////////////  Set up
const app = express();

app.use(
    expressSession({
        secret: "Tecky Academy teaches typescript",
        resave: true,
        saveUninitialized: true,
    })
);

//////////////////////  Main Route / Logic

app.use((req, res, next) => {
    // [2018-11-12 09:21:31] Request /index.js
    let dateTimeString = format(new Date(), "YYYY-MM-DD HH:mm:ss");
    console.log(`[${dateTimeString}] ${req.method} ${req.path}`);
    next();
});
app.use((req, res, next) => {
    let counter = req.session["counter"] || 0;
    counter++;
    req.session["counter"] = counter;
    console.log(` 你上左黎 : ${req.session["counter"]}`);
    next();
});

interface Memo{
    content:string,
    image?:string
}

app.post("/memo", upload.single("image"), async (req: Request, res: Response) => {
    try {
        const {content} = req.body;
        const image = req.file?.filename;
        const memos: Memo[] = await jsonfile.readFileSync("memos.json");
        if (image) {
            memos.push({
                content/*: content*/,
                image/*: image*/
            })
        } else {
            memos.push({
                content/*: content*/
            })
        }
        jsonfile.writeFileSync("memos.json", memos);
        res.redirect('/');
    } catch (err) {
        res.redirect(`/?err=${err}`)
    }
});

app.get("/memos",async(req:Request,res:Response)=>{
    const memos = await jsonfile.readFileSync("memos.json");
    res.json(memos);
})

interface User{
    username:string,
    password:string
}

app.post("/login", async(req:Request,res:Response)=>{
    try{
    //const username = req.body.username;
    //const password = req.body.password;
    const {username,password} = req.body;
    const users:User[] = await jsonfile.readFileSync("users.json");
    const usernameCheck = users.filter(user=>user.username === username);
    if(usernameCheck.length == 0){
        return res.redirect(`/?err=User doesn't exist!`);
    }
    if(users[0].password != password){ //demo only, assume no duplicate email
        return res.redirect(`/?err=Wrong Password!`);
    }
    req.session["user"]=users[0];
    res.redirect("/admin.html");
}catch(err){
    console.log(err);
    return res.redirect(`/?err=${err}`);
}
})
app.use(express.static("public"));
app.use(isLoggedIn,express.static("protected"));
// app.use((req, res, next) => {
//     res.redirect("/404.html");
// });

const PORT = 8080;

app.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}/`);
});


/*
Ex 2:
* 0. create admin.html which is under protected folder
* 1. Create POST /login
* 2. req.body: {username:,password}
* 3. check with it to data in "users.json"
* 4. in users.json contain array
* 5. element: {username:..,password:...}
* 6. if matched, update req.session["user"]
* 7. redirect to admin.html
* 8. if not matched, redirect to /
* 9. created "isLoggedIn" middleware - check with session. If ok, call next(), redirect /

Ex 1:
* 1. Create POST /memo which store the new memo in memos.json (jsonfile.writeFile)
* 2. In memos.json, the array is stored
* 3.The element of object need 2 fields:
* 4. =>content: memo content
* 5. =>image: added if the memo has image which is FILENAME only (multi-part + multer)
* 6. Create GET /memo which get all of memos from memos.json (res.json!)
* 7. memos.json are NOT allowed inside the public folder (why??)

*/