import express from "express";
import expressSession from "express-session";
import { format } from "fecha";
import { Request, Response } from "express";
import multer from "multer";
import {isLoggedIn} from './guard';
import bodyParser from 'body-parser';
import http from 'http';
import {Server as SocketIO} from 'socket.io';

import {Client} from 'pg';
import dotenv from 'dotenv';
dotenv.config();
const client = new Client({
    user:process.env.DB_USERNAME,
    password:process.env.DB_PASSWORD,
    database:process.env.DB_NAME,
    host:process.env.DB_HOST
});
client.connect();

const storage = multer.diskStorage({
    destination:function(req,file,cb){/* cb = callback */
        cb(null,"uploads")
    },
    filename:function(req,file,cb){
        cb(null,`memo-image-${file.fieldname}.${file.mimetype.split('/')[1]}`)
    }
})
const upload = multer({storage});

/////////////////////  Set up
const app = express();
const server = new http.Server(app);
const io = new SocketIO(server);
app.use(bodyParser.urlencoded())
app.use(bodyParser.json())

app.use(
    expressSession({
        secret: "Tecky Academy teaches typescript",
        resave: true,
        saveUninitialized: true,
    })
);

//////////////////////  Main Route / Logic
app.use(express.static("public"));
app.use((req, res, next) => {
    // [2018-11-12 09:21:31] Request /index.js
    let dateTimeString = format(new Date(), "YYYY-MM-DD HH:mm:ss");
    console.log(`[${dateTimeString}] ${req.method} ${req.path}`);
    next();
});
app.use((req, res, next) => {
    let counter = req.session["counter"] || 0;
    counter++;
    req.session["counter"] = counter;
    console.log(` 你上左黎 : ${req.session["counter"]}`);
    next();
});

app.post("/memo", upload.single("image"), async (req: Request, res: Response) => {
    try {
        const {content} = req.body;
        const image = req.file?.filename;
        if (image) {
            await client.query(
                `insert into memos(content,image,created_at,updated_at) values ('${content}','${image}',NOW(),NOW())
            `);
            io.emit("new-memo",{
                content,
                image
            });
        } else {
            await client.query(
                `insert into memos(content,created_at,updated_at) values ('${content}',NOW(),NOW())
            `);
            io.emit("new-memo",{
                content
            });
        }
        res.json({success:true});
    } catch (err) {
        console.log(err)
        res.json({success:false,error:err});
    }
});

app.get("/memos",async(req:Request,res:Response)=>{
    /*
    const {rows} = await client.query(`select * from memos`); 
    */
    const results = await client.query(`select * from memos order by id DESC`);
    res.json(results.rows);
})

app.post("/login", async (req: Request, res: Response) => {
    try {
        const { username, password } = req.body;
        const results = await client.query(`select * from staff where username='${username}'`);
        if (results.rows.length == 0){
            return res.json({ success: false, error: "User doesn't exist!" });
        }
        if (results.rows[0].password != password){
            return res.json({ success: false, error: "Wrong Password!" });
        }
        req.session["user"] = results.rows[0];
        return res.json({ user: results.rows[0] });
    } catch (err) {
        console.log(err);
        return res.json({ success: false, error: err });
    }
})

app.get("/user", (req: Request, res: Response) => {
    res.json({user:req.session["user"]});
});

app.put("/memo/:id",upload.single("image"),async (req: Request, res: Response) => {
    try {
        const {id/*string!*/} = req.params;
        const {content} = req.body;
        const image = req.file?.filename;
        if (image){
            await client.query(
                `update memos set content='${content}',image='${image}' where id=${id}`
                );
        }else{
            await client.query(
                `update memos set content='${content}' where id=${id}`
                );
        }
        io.emit("update-memo-list");
        res.json({success:true});
    } catch (err) {
        console.log(err)
        res.json({success:false,error:err});
    }
});

app.delete("/memo/:id", async (req: Request, res: Response) => {
    try {
        const { id } = req.params;
        await client.query(`delete from memos where id=${id}`);
        io.emit("update-memo-list");
        res.json({ success: true });
    } catch (err) {
        console.log(err);
        res.json({ success: false });
    }
});

app.get("/logout",async (req: Request, res: Response) => {
    req.session.destroy((err)=>{
        if(err){
            return res.json({success:false});
        }
        return res.json({success:true});
    });
});

io.on("connection",function(socket){
    console.log("[Incoming Socket] "+socket)
});


app.use(express.static("uploads"));
app.use(isLoggedIn,express.static("protected"));
// app.use((req, res, next) => {
//     res.redirect("/404.html");
// });

const PORT = 8080;

server.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}/`);
});