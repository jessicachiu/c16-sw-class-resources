const err = new URL(window.location.href).searchParams.get("err");
if(err){
    alert(err);
}
let editMemoId = -1;
let memos = [];
async function loadMemos(){
    const res = await fetch("/memos");
    memos = await res.json();
    const memoBoardDiv = document.querySelector(".memo-board");
    memoBoardDiv.innerHTML = '';
    let idx = 0;
    for(let memo of memos){
        updateMemoDiv(memo,memoBoardDiv);
        idx++; //idx = idx + 1;
    }

    for(idx = 0;idx<memos.length;idx++){
        updateMemoEvent(memos[idx]);
    }
}

function updateMemoDiv(memo,memoBoardDiv){
    if (memo.image){
        memoBoardDiv.innerHTML += `
        <div class='memo'>
        <div class="memo-layout">
        <img src="${memo.image}"/>
        <div>${memo.content}</div>
        </div>
        <div class='icon-btn edit'>
            <i id="edit-${memo.id}" class="fas fa-edit"></i>
        </div>
        <div class='icon-btn trash'>
            <i id="del-${memo.id}" class="fas fa-trash-alt"></i>
        </div>
    </div>
        `
    }else{
        memoBoardDiv.innerHTML += `
        <div class='memo'>
        ${memo.content}
        <div class='icon-btn edit'>
            <i id="edit-${memo.id}" class="fas fa-edit"></i>
        </div>
        <div class='icon-btn trash'>
            <i id="del-${memo.id}" class="fas fa-trash-alt"></i>
        </div>
    </div>
        `
    }
}

function updateMemoEvent(memo){
    document.querySelector(`#edit-${memo.id}`).addEventListener("click",(event)=>{
        const id = event.target.id.split("-")[1];
        const editForm = document.querySelector("#edit-memo-form");
        const newForm = document.querySelector("#memo-form");
        const editContent = document.querySelector("#edit-content");
    
        newForm.classList.remove("show");
        newForm.classList.add("hide");

        editForm.classList.remove("hide");
        editForm.classList.add("show");
        editContent.value = memos.filter(memo=>memo.id == id)[0].content;
        editMemoId = id;
    });

    document.querySelector(`#del-${memo.id}`).addEventListener("click",async (event)=>{
       const id = event.target.id.split("-")[1];
       const res = fetch(`/memo/${id}`,{
           method:"Delete"
       });
       await loadMemos();
    });
}

async function getCurrentUser(){
    const res = await fetch("/user");
    const result = await res.json();
    const adminDiv = document.querySelector("#admin-from");
    const userInfoDiv = document.querySelector("#userInfo");
    const loginName = document.querySelector("#loginName");
    if(result.user){
        adminDiv.classList.remove("show");
        adminDiv.classList.add("hide");

        userInfoDiv.classList.remove("hide");
        userInfoDiv.classList.add("show");

        loginName.innerHTML = result.user.username;
    }else{
        adminDiv.classList.remove("hide");
        adminDiv.classList.add("show");
        
        userInfoDiv.classList.remove("show");
        userInfoDiv.classList.add("hide");

        loginName.innerHTML = '';
    }
}


loadMemos();
getCurrentUser();

//create new-memo-form submit event
document.querySelector("#memo-form").addEventListener("submit",async(event)=>{
    event.preventDefault();
    const form = event.target;
    const formData = new FormData();
    formData.append("content",form.content.value);
    formData.append("image",form.image.files[0]);

    const res = await fetch("/memo",{
        method:"POST",
        body:formData
    });
    const result = await res.json();
    if (result.success){
        await loadMemos();
    }
});
//create edit-memo-form submit event
document.querySelector("#edit-memo-form").addEventListener("submit",async(event)=>{
    event.preventDefault();
    const form = event.target;
    const formData = new FormData();
    formData.append("content",form.content.value);
    formData.append("image",form.image.files[0]);

    const res = await fetch(`/memo/${editMemoId}`,{
        method:"PUT",
        body:formData
    });
    const result = await res.json();
    if (result.success){
        await loadMemos();
        editMemoId = -1;

        const editForm = document.querySelector("#edit-memo-form");
        const newForm = document.querySelector("#memo-form");

        newForm.classList.remove("hide");
        newForm.classList.add("show");
        editForm.classList.remove("show");
        editForm.classList.add("hide");
    }
});

//create admin-form submit event
document.querySelector("#admin-from").addEventListener("submit",async(event)=>{
    event.preventDefault();
    const form = event.target;
    const body = {
        username:form.username.value,
        password:form.password.value
    }
    const res = await fetch("/login",{
        method: "POST",
        headers:{
            "Content-Type":"application/json"
        },
        body:JSON.stringify(body)
    });
    const result = await res.json();
    if (result.user){
        window.location = "/admin.html";
    }else{
        window.location = "/";
    }
});

document.querySelector("#userInfo").addEventListener("submit",async ()=>{
    const res = await fetch("/logout"); // GET /logout
    const result = await res.json();
    if(result.success){
        window.location = "/";
    }
});

const socket = io.connect();
socket.on("new-memo",(memo)=>{
    memos.push(memo);
    const memoBoardDiv = document.querySelector(".memo-board");
    updateMemoDiv(memo,memoBoardDiv);
    updateMemoEvent(memo);
});
socket.on("update-memo-list",async()=>{
    await loadMemos();
});