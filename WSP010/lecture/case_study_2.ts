import { Client } from "pg";
import dotenv from "dotenv";
dotenv.config();

const client = new Client({
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    host: process.env.DB_HOST,
});

console.log(process.env.DDDDDD)
async function main() {
    await client.connect();

    const result = await client.query(/*sql*/ `
    SELECT 
        s.id as student_id,    
        s.name as student_name,         
        s.level as student_level,        
        s.date_of_birth student_dob,
        s.teacher_id ,
        t.id as teacher_id,
        t.name as teacher_name,
        t.date_of_birth as teacher_dob

        FROM students as s inner join teachers as t on t.id = s.teacher_id;
    
    
    
    `);
    console.table(result.rows);
    let sqlResult = result.rows
    let groupedTeachers = new Map();

    for (let item of sqlResult) {
        const student = {
            student_name: item.student_name,
            student_id: item.student_id,
            student_dob: item.student_dob,
        };
        if (groupedTeachers.has(item.teacher_name)) {
            groupedTeachers.get(item.teacher_name).students.push(student);
        } else {
            const teacher = {
                teacher_name: item.teacher_name,
                teacher_id: item.teacher_id,
                teacher_dob: item.teacher_dob,
                students: [student],
            };
            groupedTeachers.set(item.teacher_name, teacher);
        }
    }
    const printResult = Array.from(groupedTeachers.values());
    console.log(JSON.stringify(printResult,null,4));

    await client.end();
    console.log("PG session ended");
}

main();
