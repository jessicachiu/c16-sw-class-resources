import {Client} from 'pg';
import dotenv from 'dotenv';
dotenv.config();

const client = new Client({
    user:process.env.DB_USERNAME,
    password:process.env.DB_PASSWORD,
    database:process.env.DB_NAME,
    host:process.env.DB_HOST
});

async function main(){
    
    await client.connect();

    const result = await client.query(/*sql*/`INSERT INTO teachers (name,date_of_birth) VALUES ($1,$2) RETURNING id`,['Bobby','1970-01-01']);
    const teacherId = result.rows[0].id

    await client.query(/*sql*/` INSERT INTO students (name,level,date_of_birth,teacher_id)
      VALUES ($1,$2,$3,$4),
             ($5,$6,$7,$8),
             ($9,$10,$11,$12)
    `,[
       "Peter",25,'1995-05-15',teacherId,
       "John",25,"1985-06-16",teacherId,
       "Simon", 25, "198-07-17",null
    ]);
    // console.table(result.rows);
    

    await client.end()
    console.log("PG session ended");
    
}

main()