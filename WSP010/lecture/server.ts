import express from 'express';
import http from 'http';
import {Server as SocketIO} from 'socket.io';
import expressSession from 'express-session'

//....


const app = express();
const server = new http.Server(app);
const io = new SocketIO(server);

const sessionMiddleware = expressSession({
    secret: 'Tecky Academy teaches typescript',
    resave:true,
    saveUninitialized:true,
    cookie:{secure:false}
});

app.use(sessionMiddleware);

io.use((socket,next)=>{
    let req = socket.request as express.Request
    let res = req.res as express.Response
    sessionMiddleware(req, res, next as express.NextFunction)
});


let counter =1
io.on('connection', function (socket) {
    console.log('有Client socket 連線：',socket.id);
    // 只有果個socket 知道
    socket.emit('secret-mission', `你收到secret mission${counter}`)

    // 同全部socket 講野
    io.emit("new-connection",{msg:"有新仔到", user:socket.id});
    counter++

    // You can set any values you want to session here.
    (socket.request as express.Request).session['data'] = 'XXX';
    // There is no auto save for session.
    (socket.request as express.Request).session.save();


    socket.on('receive-mission',async (data)=>{
        console.log('有人收到任務',socket.id);
        
    });

});
io.on('disconnected',()=>{
console.log('有人走左');

})

app.post('/apple',(req,res)=>{
    // logic of adding apple.
    io.emit("new-apple","Congratulations! New Apple Created!");
    res.json({updated:1});
});

app.use(express.static('public'))
//....
const PORT = 8080;
server.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}/`);
})