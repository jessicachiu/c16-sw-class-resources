import express from "express";
import { checkPassword, hashPassword } from "../hash";
import { client } from "../db";
import fetch from "node-fetch";
import { logger } from "../logger";

const userRoutes = express.Router();

userRoutes.post("/register", register);
userRoutes.get("/user", getUser);
userRoutes.get("/logout", logout);
userRoutes.post("/login", login);
userRoutes.get("/login/google", loginGoogle);

async function login(req: express.Request, res: express.Response) {
    try {
        const { username, password } = req.body;
        const results = await client.query(`select * from staff where username='${username}'`);
        if (results.rows.length == 0) {
            return res.json({ success: false, error: "User 不存在" });
        }
        if (!checkPassword(password, results.rows[0].password)) {
            return res.json({ success: false, error: "電郵或密碼錯誤" });
        }
        req.session["user"] = results.rows[0];
        return res.json({ user: results.rows[0] });
    } catch (err) {
        logger.error("err : ", err);
        return res.json({ success: false, error: err });
    }
}

function getUser(req: express.Request, res: express.Response) {
    res.json({ user: req.session["user"] });
}

async function logout(req: express.Request, res: express.Response) {
    req.session.destroy((err) => {
        if (err) {
            return res.json({ success: false });
        }
        return res.json({ success: true });
    });
}

async function loginGoogle(req: express.Request, res: express.Response) {
    const accessToken = req.session?.["grant"].response.access_token;
    const fetchRes = await fetch("https://www.googleapis.com/oauth2/v2/userinfo", {
        method: "get",
        headers: {
            Authorization: `Bearer ${accessToken}`,
        },
    });
    const googleUserInfo = await fetchRes.json();
    const users = (await client.query(`SELECT * FROM staff WHERE username = $1`, [googleUserInfo.email])).rows;
    let user = users[0];
    if (!user) {
        let hashedPassword = await hashPassword((Math.random() + 1).toString(36));
        const createUserResult = await client.query(`INSERT INTO staff (username,password ,created_at ,updated_at) VALUES ($1,$2,$3,$4) RETURNING *`, [googleUserInfo.email, hashedPassword, "NOW()", "NOW()"]);
        const createUserRowCount = createUserResult.rowCount;
        user = createUserResult.rows[0];
        if (createUserRowCount != 1) {
            res.status(401).json({ msg: "insert fail" });
            return;
        }
    }

    if (req.session) {
        req.session["user"] = user;
    }
    return res.redirect("/admin/admin.html");
}

async function register(req: express.Request, res: express.Response) {
    const { username, password } = req.body;
    if (username && password) {
        const hashedPassword = await hashPassword(password);
        const createUserResult = await client.query(`INSERT INTO staff (username,password ,created_at ,updated_at) VALUES ($1,$2,$3,$4) RETURNING id`, [username, hashedPassword, "NOW()", "NOW()"]);
        const createUserRowCount = createUserResult.rowCount;
        console.log(createUserRowCount);

        if (createUserRowCount === 1) {
            res.json({ msg: "Create User Success" });
        } else {
            res.status(401).json({ msg: "Create User Fail" });
        }
    } else {
        res.status(401).json({ msg: "Invalid input" });
    }
}

export default userRoutes;
