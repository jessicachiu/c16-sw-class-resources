import express, { Request, Response } from "express";
import { client } from "../db";
import { isLoggedIn } from "../guard";
import { logger } from "../logger";
import { multerFormSingle } from "../multer";
import { io } from "../socketio";

const memoRoutes = express.Router();

memoRoutes.post("/memo", multerFormSingle, createMemo);
memoRoutes.put("/memo/:id", multerFormSingle, updateMemoById);
memoRoutes.get("/memos", getAllMemos);
memoRoutes.delete("/memo/:id", deleteMemoById);
memoRoutes.delete("/memos", deleteAllMemo);
memoRoutes.post("/like/memo/:id", isLoggedIn, likeMemoById);
memoRoutes.get("/admin/like_memos", isLoggedIn, getLikedMemosById);

async function getLikedMemosById(req: Request, res: Response) {
    try {
        console.log(req.query);

        const userId = req.query.user_id;
        console.log(req.query["user_id"]);
        console.log(req.query.user_id);

        console.log("userId = ", userId);

        const results = await client.query(
            /*sql*/
            `
        SELECT memos.*  from staff 
            INNER JOIN likes on likes.user_id = staff.id
            INNER JOIN memos on likes.memo_id = memos.id
            WHERE staff.id = $1;
        `,
            [userId]
        );
        res.json({ success: true, data: results.rows });
    } catch (error) {
        logger.error("Error :" + JSON.stringify(error));
        res.status(500).json({ err: "System error" });
    }
}

async function likeMemoById(req: Request, res: Response) {
    try {
        const { id } = req.params;
        let userId = req.session["user"].id;
        console.log({ userId });
        console.log(id);

        await client.query(/*sql*/ `
        insert into likes(user_id,memo_id,created_at,updated_at) values ('${userId}','${id}',NOW(),NOW())
        `);
        res.json({ success: true });
    } catch (err) {
        logger.error("error:" + JSON.stringify(err));
        res.json({ success: false });
    }
}

async function deleteAllMemo(req: Request, res: Response) {
    try {
        await client.query(/*sql*/ `DELETE FROM memos`);
        res.json({ msg: "Cleared All memo" });
    } catch (error) {
        logger.error("Err:", error);
        res.json({ msg: "Clear memos fail" });
    }
}
async function createMemo(req: Request, res: Response) {
    try {
        const { content } = req.body;
        const image = req.file?.filename;
        if (image) {
            await client.query(
                `insert into memos(content,image,created_at,updated_at) values ('${content}','${image}',NOW(),NOW())
            `
            );
            io.emit("new-memo", {
                content,
                image,
            });
        } else {
            await client.query(
                `insert into memos(content,created_at,updated_at) values ('${content}',NOW(),NOW())
            `
            );
            io.emit("new-memo", {
                content,
            });
        }
        res.json({ success: true });
    } catch (err) {
        logger.error("error:", err);
        res.json({ success: false, error: err });
    }
}

async function getAllMemos(req: Request, res: Response) {
    const results = await client.query(`select * from memos order by id DESC`);
    res.json(results.rows);
}

async function updateMemoById(req: Request, res: Response) {
    try {
        const { id /*string!*/ } = req.params;
        const { content } = req.body;
        const image = req.file?.filename;
        if (image) {
            await client.query(`update memos set content='${content}',image='${image}' where id=${id}`);
        } else {
            await client.query(`update memos set content='${content}' where id=${id}`);
        }
        io.emit("update-memo-list");
        res.json({ success: true });
    } catch (err) {
        logger.error("error:", err);
        res.json({ success: false, error: err });
    }
}

async function deleteMemoById(req: Request, res: Response) {
    try {
        const { id } = req.params;
        await client.query(`delete from memos where id=${id}`);
        io.emit("update-memo-list");
        res.json({ success: true });
    } catch (err) {
        logger.error("error:", err);
        res.json({ success: false });
    }
}

export default memoRoutes;
