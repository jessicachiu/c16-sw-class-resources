import express from "express";
import { appleController } from "../server";

export const appleRoutes = express.Router();

appleRoutes.get("/", appleController.get);
appleRoutes.post("/", appleController.post);
appleRoutes.put("/:id", appleController.put);
appleRoutes.delete("/:id", appleController.delete);
