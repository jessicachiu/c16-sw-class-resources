import { Request, Response } from "express";
import { Client } from "pg";
import { AppleController } from "../controllers/AppleController";
import { AppleService } from "../services/AppleSrvice";
import SocketIO from "socket.io";

jest.mock("express");

describe("AppleController", () => {
    let controller: AppleController;
    let service: AppleService;
    let resJsonSpy: jest.SpyInstance;
    let io: SocketIO.Server;
    let emit: jest.Mock;
    let req: Request;
    let res: Response;

    beforeEach(function () {
        service = new AppleService({} as Client);
        jest.spyOn(service, "getApples").mockImplementation(() => Promise.resolve([{ id: 1, breed: "fuji", weight: 100, price: 200 }]));
        jest.spyOn(service, "addApple").mockImplementation((body) => Promise.resolve(1));
        jest.spyOn(service, "updateApple").mockImplementation((id, body) => Promise.resolve(1));
        jest.spyOn(service, "deleteApple").mockImplementation((id) => Promise.resolve(1));

        emit = jest.fn((event, msg) => null);
        io = {
            to: jest.fn(() => ({ emit })),
        } as any;

        controller = new AppleController(service, io);
        req = {
            body: {},
            params: {},
            session: {
                socketIds: [1],
                user: {
                    id: 1,
                },
            },
        } as any as Request;
        res = {
            json: jest.fn(() => {}),
        } as any as Response;
        resJsonSpy = jest.spyOn(res, "json");
    });

    it("should handle get method correctly", async () => {
        await controller.get(req, res);
        expect(service.getApples).toBeCalledTimes(1);
        expect(resJsonSpy).toBeCalledWith([{ id: 1, breed: "fuji", weight: 100, price: 200 }]);
    });

    it("should handle post method correctly", async () => {
        req.body = {
            breed: "Red delicious",
            weight: 230,
        };
        await controller.post(req, res);
        expect(io.to).toBeCalledWith(1);
        expect(emit).toBeCalledWith("new-apple", "Congratulations! New Apple Created!");
        expect(service.addApple).toBeCalledTimes(1);
        expect(service.addApple).toBeCalledWith(req.body);
        expect(res.json).toBeCalledWith({ updated: 1 });
    });

    // it("should handle put method correctly", async () => {
    //     req.body = {
    //         breed: "Red delicious",
    //         weight: 230,
    //     };
    //     req.params = {
    //         id: "1",
    //     };
    //     await controller.put(req, res);
    //     expect(service.updateApple).toBeCalledTimes(1);
    //     expect(service.updateApple).toBeCalledWith(parseInt(req.params.id), req.body);
    //     expect(res.json).toBeCalledWith({ updated: 1 });
    // });

    // it("should handle delete method correctly", async () => {
    //     req.params = {
    //         id: "1",
    //     };
    //     await controller.delete(req, res);
    //     expect(service.deleteApple).toBeCalledTimes(1);
    //     expect(service.deleteApple).toBeCalledWith(parseInt(req.params.id));
    //     expect(res.json).toBeCalledWith({ updated: 1 });
    // });
});
