import { Request, Response } from "express";
import SocketIO from "socket.io";
import { AppleService } from "../services/AppleSrvice";

export class AppleController {
    constructor(private appleService: AppleService, private io: SocketIO.Server) {}

    get = async (req: Request, res: Response) => {
        const results = await this.appleService.getApples();
        // delete results[0]["price"];
        res.json(results);
    };

    post = async (req: Request, res: Response) => {
        await this.appleService.addApple(req.body);
        if (req.session) {
            for (let socketId of (req.session as any).socketIds) {
                this.io.to(socketId).emit("new-apple", "Congratulations! New Apple Created!");
            }
        }
        res.json({ updated: 1 });
    };

    put = async (req: Request, res: Response) => {
        const id = parseInt(req.params.id);
        if (isNaN(id)) {
            res.status(400).json({ msg: "id is not a number" });
            return;
        }
        const updated = await this.appleService.updateApple(id, req.body);
        res.json({ updated });
    };

    delete = async (req: Request, res: Response) => {
        const id = parseInt(req.params.id);
        if (isNaN(id)) {
            res.status(400).json({ msg: "id is not a number" });
            return;
        }
        await this.appleService.deleteApple(id);
        res.json({ updated: 1 });
    };
}
