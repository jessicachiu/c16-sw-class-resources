import { Client } from "pg";
import { Apple } from "../models/Apple";

export class AppleService {
    constructor(private client: Client) {}

    async getApples(): Promise<Apple[]> {
        const result = await this.client.query(/* sql */ `SELECT * FROM apples`);
        return result.rows;
    }

    async addApple(body: { breed: string; weight: number }) {
        const { breed, weight } = body;
        const result = await this.client.query(/*sql*/ ` INSERT INTO apples (breed,weight) values ($1, $2)`, [breed, weight]);
        return result.rowCount;
    }

    async updateApple(id: number, body: Apple) {
        const { breed, weight } = body;
        const result = await this.client.query(/* sql */ `UPDATE apples set breed = $1 , weight = $2 where id = $3`, [breed, weight, id]);
        return result.rowCount;
    }

    async deleteApple(id: number) {
        const result = await this.client.query(/* sql */ `DELETE FROM  apples  where id = $1`, [id]);
        return result.rowCount;
    }

    async handleSocketIOData(data: any) {
        return true;
    }
}
