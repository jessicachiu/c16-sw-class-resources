export interface Apple {
    id: number;
    weight: number;
    breed: string;
}
