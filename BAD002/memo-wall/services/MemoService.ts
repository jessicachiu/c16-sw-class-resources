import { client } from "../db";
import { logger } from "../logger";

export class MemoService{
    constructor(){}
    createMemo = async(content:string,image:string|undefined)=>{
        try {
            if (image) {
                await client.query(
                    `insert into memos(content,image,created_at,updated_at) values ('${content}','${image}',NOW(),NOW())
                `
                );

            } else {
                await client.query(
                    `insert into memos(content,created_at,updated_at) values ('${content}',NOW(),NOW())
                `
                );
            }
            return { success: true };
        } catch (err) {
            logger.error("error:", err);
            return { success: false, error: err };
        }
    }
    updateMemoById = async(id:string,content:string,image:string|undefined)=>{}
    getAllMemos = async()=>{
        const results = await client.query(`select * from memos order by id DESC`);
        return results.rows;
    }
    deleteMemoById=async(id:string)=>{
        try {
            await client.query(`delete from memos where id=${id}`);
            return { success: true };
        } catch (err) {
            logger.error("error:", err);
            return { success: false };
        }
    }
    deleteAllMemo = async () => {
        try {
            await client.query(/*sql*/ `DELETE FROM memos`);
            return { msg: "Cleared All memo" };
        } catch (error) {
            logger.error("Error :" + JSON.stringify(error));
            return { err: "System error" };
        }
    }
    likeMemoById = async (id: string, userId: string) => {
        try {
            await client.query(/*sql*/ `
        insert into likes(user_id,memo_id,created_at,updated_at) values ('${userId}','${id}',NOW(),NOW())
        `);
            return { success: true };
        } catch (error) {
            logger.error("Error :" + JSON.stringify(error));
            return { err: "System error" };
        }
    }
    getLikedMemosById = async(userId:any)=>{
        try {
            const results = await client.query(
                /*sql*/
                `
            SELECT memos.*  from staff 
                INNER JOIN likes on likes.user_id = staff.id
                INNER JOIN memos on likes.memo_id = memos.id
                WHERE staff.id = $1;
            `,
                [userId]
            );
            return { success: true, data: results.rows };
        } catch (error) {
            logger.error("Error :" + JSON.stringify(error));
            return { err: "System error" };
        }
    }
}