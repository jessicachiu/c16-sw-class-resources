import { client } from "../db";
import {hashPassword,checkPassword} from '../hash';

export class UserService{
    constructor(){};
    register = async(username:string, password:string)=>{
        if (username && password) {
            const hashedPassword = await hashPassword(password);
            const createUserResult = await client.query(`INSERT INTO staff (username,password ,created_at ,updated_at) VALUES ($1,$2,$3,$4) RETURNING id`, [username, hashedPassword, "NOW()", "NOW()"]);
            const createUserRowCount = createUserResult.rowCount;
            console.log(createUserRowCount);
    
            if (createUserRowCount === 1) {
                return { msg: "Create User Success" };
            } else {
                return { msg: "Create User Fail" };
            }
        } else {
            return { msg: "Invalid input" };
        }
    }

    login = async(username:string, password:string)=>{
        const results = await client.query(`select * from staff where username='${username}'`);
        if (results.rows.length == 0) {
            return { success: false, error: "User 不存在" };
        }
        if (!checkPassword(password, results.rows[0].password)) {
            return { success: false, error: "電郵或密碼錯誤" };
        }
        return { user: results.rows[0] };
    }

    loginGoogle = async (googleUserInfo:any) => {
        const users = (await client.query(`SELECT * FROM staff WHERE username = $1`, [googleUserInfo.email])).rows;
        let user = users[0];
        if (!user) {
            let hashedPassword = await hashPassword((Math.random() + 1).toString(36));
            const createUserResult = await client.query(`INSERT INTO staff (username,password ,created_at ,updated_at) VALUES ($1,$2,$3,$4) RETURNING *`, [googleUserInfo.email, hashedPassword, "NOW()", "NOW()"]);
            const createUserRowCount = createUserResult.rowCount;
            user = createUserResult.rows[0];
            if (createUserRowCount != 1) {
                return { msg: "insert fail" };
            }
        }
        return user;
    }
}