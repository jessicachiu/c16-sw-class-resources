let editMemoId = -1;
let memos = [];
export async function loadMemos() {
    const res = await fetch("/memos");
    memos = await res.json();
    const memoBoardDiv = document.querySelector(".memo-board");
    memoBoardDiv.innerHTML = "";
    let idx = 0;
    for (let memo of memos) {
        updateMemoDiv(memo, memoBoardDiv);
        idx++; //idx = idx + 1;
    }

    for (idx = 0; idx < memos.length; idx++) {
        updateMemoEvent(memos[idx]);
    }
}

function updateMemoDiv(memo, memoBoardDiv) {
    if (memo.image) {
        memoBoardDiv.innerHTML += `
        <div class='memo'>
        <div class="memo-layout">
        <img src="${memo.image}"/>
        <div>${memo.content}</div>
        </div>
        <div class='icon-btn edit'>
            <i id="edit-${memo.id}" class="fas fa-edit"></i>
        </div>
        <div class='icon-btn trash'>
            <i id="del-${memo.id}" class="fas fa-trash-alt"></i>
        </div>
        <div class='icon-btn like'>
            <i id="like-${memo.id}" class="fas fa-heart"></i>
        </div>
    </div>
        `;
    } else {
        memoBoardDiv.innerHTML += `
        <div class='memo'>
        ${memo.content}
        <div class='icon-btn edit'>
            <i id="edit-${memo.id}" class="fas fa-edit"></i>
        </div>
        <div class='icon-btn trash'>
            <i id="del-${memo.id}" class="fas fa-trash-alt"></i>
        </div>
        <div class='icon-btn like'>
            <i id="like-${memo.id}" class="fas fa-heart"></i>
        </div>
    </div>
        `;
    }
}

function updateMemoEvent(memo) {
    document.querySelector(`#edit-${memo.id}`).addEventListener("click", (event) => {
        const id = event.target.id.split("-")[1];
        const editForm = document.querySelector("#edit-memo-form");
        const newForm = document.querySelector("#memo-form");
        const editContent = document.querySelector("#edit-content");

        newForm.classList.remove("show");
        newForm.classList.add("hide");

        editForm.classList.remove("hide");
        editForm.classList.add("show");
        editContent.value = memos.filter((memo) => memo.id == id)[0].content;
        editMemoId = id;
    });

    document.querySelector(`#del-${memo.id}`).addEventListener("click", async (event) => {
        const id = event.target.id.split("-")[1];
        const res = fetch(`/memo/${id}`, {
            method: "Delete",
        });
        await loadMemos();
    });
    document.querySelector(`#like-${memo.id}`).addEventListener("click", async (event) => {
        const id = event.target.id.split("-")[1];
        const res = fetch(`/like/memo/${id}`, {
            method: "POST",
        });
        // await loadMemos();
    });
}

const socket = io.connect();
socket.on("new-memo", (memo) => {
    memos.push(memo);
    const memoBoardDiv = document.querySelector(".memo-board");
    updateMemoDiv(memo, memoBoardDiv);
    updateMemoEvent(memo);
});
socket.on("update-memo-list", async () => {
    await loadMemos();
});
