import { goToBar, Person } from "./main";

it("Testing goToBar", () => {
    // Arrange
    const john = new Person(15);
    const peter = new Person(20);

    const johnDrinkSpy = jest.spyOn(john, "drink");
    const peterDrinkSpy = jest.spyOn(peter, "drink");

    //Action
    goToBar([john, peter]);

    // Assertion
    expect(johnDrinkSpy).not.toBeCalled();
    expect(peterDrinkSpy).toBeCalled();
});
