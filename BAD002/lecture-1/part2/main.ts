export class Person {
    constructor(public age: number) {}

    drink() {
        console.log("I am drunk");
    }
}

export function goToBar(people: Person[]) {
    const adults = people.filter((person) => person.age > 18);
    adults.map((adult) => adult.drink());
}
