import { Person } from "./main";
import { goToBarAsync } from "./main_async";

it("Testing goToBar with a teenager", async () => {
    const john = new Person(15);
    const peter = new Person(20);

    const johnSpy = jest.spyOn(john, "drink");
    const peterSpy = jest.spyOn(peter, "drink");

    const adults = goToBarAsync([john, peter]);
    expect(johnSpy).not.toBeCalled();
    expect(peterSpy).not.toBeCalled();
    expect(adults).rejects.toEqual([peter]);

    // try {
    //     await goToBarAsync([john, peter]);
    // } catch (err) {
    //     expect(err).toEqual([peter]);
    // }
});

it("Testing goToBar with all adult", () => {
    const sam = new Person(25);
    const peter = new Person(20);
    const samSpy = jest.spyOn(sam, "drink");
    const peterSpy = jest.spyOn(peter, "drink");

    const adults = goToBarAsync([sam, peter]);
    expect(samSpy).toBeCalled();
    expect(peterSpy).toBeCalled();
    expect(adults).resolves.toEqual([sam, peter]);
});
