function filter(numbers: number[], 過濾器: (number: number, dummyInput: string) => boolean) {
    const filtered = [];
    for (let num of numbers) {
        if (過濾器(num, "無用既string input")) {
            filtered.push(num);
        }
    }
    return filtered;
}

// function 雙數過濾器(numberInput: number) {
//     return numberInput % 2 == 0;
// }
// function 單數過濾器(numberInput: number) {
//     return numberInput % 2 == 0;
// }
// function 乜都唔得過濾器(numberInput: number) {
//     return false;
// }
// const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
// console.log(filter(numbers, 單數過濾器));

export default filter;
