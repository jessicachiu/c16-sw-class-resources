import filter from "./filter";

let numbers: number[];

beforeAll(() => {
    numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
});

it("Testing filter", () => {
    const mock雙數過濾器 = jest.fn((numberInput: number, dummyInput: string) => {
        return numberInput % 2 == 0;
    });

    const filtered = filter(numbers, mock雙數過濾器);
    // console.table(mock雙數過濾器.mock.results);
    expect(mock雙數過濾器.mock.results[0].value).toBeFalsy();
    expect(mock雙數過濾器.mock.calls.length).toBe(10);
    expect(mock雙數過濾器.mock.calls[0][1]).toBe("無用既string input");
    expect(filtered.length).toBe(5);
});
