import { printNumbers } from "./app";
// import { printNumbersWithTimer } from "./app";
import filter from "./filter";

// Mock the filter module
jest.mock("./filter");
// jest.useFakeTimers();
it("Testing printNumbers", () => {
    // Since we are using typescript, tell compiler filter is a Mock.
    // Mock return value to be [1,3,5]
    // Arrange
    (filter as jest.Mock).mockReturnValue([1, 3, 5]);
    // Also need to mock console.log
    console.log = jest.fn();

    // Action
    // IMPORTANT!! printNumbers is the testing function , it is never mock!!
    printNumbers();
    // printNumbersWithTimer();

    // Assertion
    // Verification
    expect(filter).toBeCalledTimes(1);
    // jest.advanceTimersByTime(5000);
    expect(console.log).toBeCalledWith([1, 3, 5]);
});
