import filter from "./filter";

export function printNumbers() {
    // console.log(filter);

    const oddNumbers = filter([1, 2, 3, 4, 5], (num) => num % 2 != 0);
    // const oddNumbers = [1, 3, 5];
    console.log(oddNumbers);
}
export function printNumbersWithTimer() {
    // console.log(filter);

    const oddNumbers = filter([1, 2, 3, 4, 5], (num) => num % 2 != 0);
    // const oddNumbers = [1, 3, 5];
    setTimeout(() => {
        console.log(oddNumbers);
    }, 5000);
}
