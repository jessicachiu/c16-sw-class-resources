/*
Let's write a program to benchmark 
your function written in last exercise (listAllJsRecursive).
 */

/*
1. interactive interface 
2. read the report
3. run the benchmark
    - 3 Tests (run listAllJsRecursive 1, 10, 100 times)
    - write result to result.json (JSON.stringify())
    - record memory status
 */

import {readLinePromise} from './ex1'; // from previous exercise
import {listAllJsRecursive} from './fs';
import moment from 'moment'
import os from 'os';
import jsonfile from 'jsonfile';


const readCommand = async ()=>{
    while(true){ // game-loop, eval-loop
        // Exit by Ctrl+C
        const answer = await readLinePromise("Please choose read the report(1) or run the benchmark(2):");
        const option = parseInt(answer,10);
        console.log(`Option ${answer} chosen.`);
        if(option == 1){
            await readTheReport();  
        }else if(option == 2){
            await runTheBenchmark(); 
        }else{
            console.log("Please input 1 or 2 only.");
        }
    }
}

readCommand();

async function runTheBenchmark(){
    let result = await jsonfile.readFile("result.json");
    const testResult1 = await loopListAllJsRecursive(1);
    result.push(testResult1);
    const testResult2 = await loopListAllJsRecursive(10);
    result.push(testResult2);
    const testResult3 = await loopListAllJsRecursive(100);
    result.push(testResult3);
   await jsonfile.writeFile("result.json",result);
}

async function loopListAllJsRecursive(times:number){

    const startDate = moment();
    //const startMem = os.freemem();
    let mem = 0;
    for(let counter=0;counter<times;counter++){
        const startMem = os.freemem();
        await listAllJsRecursive("./test");
        const endMem = os.freemem();
        mem = mem + Math.abs(endMem-startMem);

    }
    const endDate = moment();
    //const endMem = os.freemem();

    return {
        startDate:startDate,
        endDate:endDate,
        timeNeeded:endDate.diff(startDate),
        extraMemUsed:mem,//KB
        name:`${times} times`
    }
}

type Report = Trial[]

interface Trial{
    startDate:string,
    endDate:string,
    timeNeeded:number,
    extraMemUsed:number,
    name:string
}

async function readTheReport(){
    const report:Report = await jsonfile.readFile("result.json");
    console.table(report);
}