import fs from "fs";
import path from "path";
import util from "util";

const fsReaddirUtilPromise = util.promisify(fs.readdir);
const fsStatUtilPromise = util.promisify(fs.stat);

export async function listAllJsRecursive(dir: string) {
    let jsFiles: string[] = [];
    try {
        const fullFolderPath = path.resolve(dir);
        const files = await fsReaddirUtilPromise(fullFolderPath);

        for (let file of files) {
            const fullFilePath = path.resolve(path.join(fullFolderPath, file));
            const stats = await fsStatUtilPromise(fullFilePath);

            if (path.extname(file) === ".js" && stats.isFile()) {
                jsFiles.push(fullFilePath);
            } else if (stats.isDirectory()) {
                const jsFilesInner = await listAllJsRecursive(fullFilePath);
                jsFiles = jsFiles.concat(jsFilesInner);
            }
        }
    } catch (error) {
        console.log(error);
    }
    return jsFiles;
}