import readline from 'readline';

const readLineInterface = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});

export function readLinePromise(question: string) {
    return new Promise<string>((resolve, reject) => {
        readLineInterface.question(question, (answer: string) => {
            resolve(answer);
        });
    });
}