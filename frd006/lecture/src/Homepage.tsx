import { push } from "connected-react-router";
import { useEffect } from "react"
import { useDispatch } from "react-redux"

export function Homepage() {
  const dispatch = useDispatch();

  useEffect(() => {
    const timerId = window.setTimeout(() => {
      dispatch(push('/game')) // programmatic way to change path
    }, 2000)

    return () => {
      console.log('Homepage 走咗 lu')

      window.clearTimeout(timerId)
    }
  }, [])
  return (
    <div>
      This is homepage
    </div>
  )
}