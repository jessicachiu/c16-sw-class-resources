import produce from "immer";
import { GameActions } from "./actions";

export interface Player {
  id: number;
  name: string;
  color: string;
}

export interface GameState {
  players: Player[],
  progress: {
    [playerId: string]: number
  },
  losers: number[] // id of the loser
  canMove: boolean
}

const initialState: GameState = {
  players: [],
  progress: {},
  losers: [],
  canMove: false
}

export function gameReducer(state: GameState = initialState, action: GameActions): GameState {
  // no side-effect
  
  switch (action.type) {
    case '@@game/ADD_PLAYER':
      return {
        ...state,
        players: [...state.players, {
          id: action.id,
          "name": action.name,
          "color": action.color
        }],
        progress: {
          ...state.progress,
          [action.id]: 0
        }
      };
    case '@@game/PLAYER_MOVE':
      return produce(state, state => {
        if (state.canMove) {
          state.progress[action.id] = state.progress[action.id] == null ? 0 : state.progress[action.id] + 1
        } else {
          state.losers.push(action.id)
        }
      })
      // if (state.canMove) {
      //   return {
      //     ...state,
      //     "progress": {
      //       ...state.progress,
      //       [action.id]: state.progress[action.id] == null ? 0 : state.progress[action.id] + 1 
      //     }
      //   }
      // } else {
      //   return {
      //     ...state,
      //     losers: [...state.losers, action.id]
      //   }
      // }
    case '@@game/SWITCH_CAN_MOVE':
      return {
        ...state,
        canMove: !state.canMove
      }
  }
  return state;
}