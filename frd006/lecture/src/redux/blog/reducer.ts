export interface BlogState {
  blogs: {
    id: number;
    title: string;
    content: string;
  }[]
}

const initialState: BlogState = {
  blogs: [{
    id: 1, title: '白石角有站了', content: '有消息指。..',
  }, {
    id: 2, title: 'Lorem ipsum dolor sit amet', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed id nunc vel nulla eleifend sollicitudin eu sit amet lorem. Donec tincidunt iaculis leo eu convallis. Cras feugiat sollicitudin nulla et egestas. Phasellus vel tortor nisl. Pellentesque dui tellus, tempor id dapibus eget, pellentesque in leo. Curabitur ac sapien quis nunc eleifend faucibus. Aliquam erat volutpat. Phasellus neque libero, ullamcorper vitae vestibulum at, lobortis nec enim. Nulla eu suscipit dolor. Suspendisse nisi dui, eleifend id cursus eu, pharetra sit amet eros. Praesent pretium malesuada lorem, nec auctor libero hendrerit varius. Nam dictum in tortor sit amet interdum. Nunc dapibus velit nec facilisis congue.'
  }]
}

export function blogReducer(state: BlogState = initialState): BlogState {
  return state;
}