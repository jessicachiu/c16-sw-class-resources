import { LeaderboardActions } from "./actions";
import produce from 'immer';

export interface LeaderboardState {
  leaders: {
    name: string,
    date: string
  }[]
}

const initialState: LeaderboardState = {
  leaders: []
}

export function leaderboardReducer(state: LeaderboardState = initialState, action: LeaderboardActions): LeaderboardState {
  return produce(state, state => {
    switch (action.type) {
      case '@@leaderboard/ADD_LEADER':
        state.leaders.push({
          name: action.name,
          date: action.date
        })
        break;
    }
  });
}