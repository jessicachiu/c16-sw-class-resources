import { connectRouter, routerMiddleware, RouterState } from "connected-react-router";
import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import { gameReducer, GameState } from "./redux/game/reducer";
import { leaderboardReducer, LeaderboardState } from "./redux/leaderboard/reducer";
import { createBrowserHistory } from 'history';
import { blogReducer, BlogState } from "./redux/blog/reducer";

export const history = createBrowserHistory();

export interface RootState {
  game: GameState
  leaderboard: LeaderboardState
  blog: BlogState
  router: RouterState
}

declare global {
  /* tslint:disable:interface-name */
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
  }
}

const reducer = combineReducers<RootState>({
  game: gameReducer,
  leaderboard: leaderboardReducer,
  blog: blogReducer,
  router: connectRouter(history)
})

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(reducer, 
  composeEnhancers(
    applyMiddleware(routerMiddleware(history))
  )
);