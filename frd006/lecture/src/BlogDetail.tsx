import { useSelector } from "react-redux"
import { useParams } from "react-router"
import { RootState } from "./store"

export function BlogDetail() {
  const blogId = parseInt(useParams<{id: string}>().id) // 自己呃自己的 type check，小心！

  const blog = useSelector((state: RootState) => {
    return state.blog.blogs.find(blog => blog.id === blogId)
  })

  return (
    <div>
      { blog == null ? <div>無此 Blog</div> : (
        <div>
          <h2>{blog.title}</h2>
          <p>{blog.content}</p>
        </div>
      )}
    </div>
  )
}