import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { RootState } from "./store";

export function BlogList() {
  const blogs = useSelector((state: RootState) => state.blog.blogs)

  return (
    <div>
      {blogs.map(blog => (
        <div><Link to={`/blogs/${blog.id}`}>{blog.title}</Link></div>
      ))}
    </div>
  )
}