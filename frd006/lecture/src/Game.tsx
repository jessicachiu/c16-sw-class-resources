import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { addPlayer, playerMove, switchCanMove } from './redux/game/actions';
import { RootState } from './store';
import { addLeaderboard } from './redux/leaderboard/actions';

export function Game() {
  const [start, setStart] = useState(true)

  const [name, setName] = useState('')
  const [id, setId] = useState('')
  const [color, setColor] = useState('')

  const dispatch = useDispatch();
  const players = useSelector((state: RootState) => state.game.players);
  const progress = useSelector((state: RootState) => state.game.progress);
  const canMove = useSelector((state: RootState) => state.game.canMove);
  const losers = useSelector((state: RootState) => state.game.losers);

  useEffect(() => {
    // [TODO] to be fixed by Redux Thunk
    for (let player of players) {
      const playerId = player.id
      if (progress[playerId] === 10 && losers.indexOf(playerId) === -1) {
        dispatch(addLeaderboard(player.name, new Date().toISOString()))
      }
    }
  }, [players, progress, losers, dispatch])

  useEffect(() => {
    if (start) {
      // mount, appear
      const timerId = window.setInterval(() => {
        dispatch(switchCanMove())
      }, 2000)
  
      return () => {
        // unmount, disappear
        clearInterval(timerId)
      }
    }
  }, [start, dispatch])

  useEffect(() => {
    const keyPressHandler = (e: KeyboardEvent) => {
      if (!isNaN(parseInt(e.key))) {
        dispatch(playerMove(parseInt(e.key)))
      }
    }
    document.addEventListener('keypress', keyPressHandler)
    return () => {
      // 記得好手尾
      document.removeEventListener('keypress', keyPressHandler)
    }
  }, [dispatch])

  return (
    <div className="App">
      <form onSubmit={e => {
        e.preventDefault();

        dispatch(addPlayer(parseInt(id), name, color))
      }}>
        <h3>Add Player</h3>
        <p><label>Id: <input value={id} onKeyPress={e => {
          e.stopPropagation();
        }} onChange={e => {
          setId(e.currentTarget.value)
        }} /></label></p>
        <p><label>Name: <input value={name} onKeyPress={e => {
          e.stopPropagation();
        }} onChange={e => setName(e.currentTarget.value)} /></label></p>
        <p><label>Color: <input value={color} onKeyPress={e => {
          e.stopPropagation();
        }} onChange={e => setColor(e.currentTarget.value)} /></label></p>
        <p><input type="submit" /></p>
      </form>
      <div>
        <button onClick={() => setStart(!start)}>{start ? 'Stop' : 'Start'}</button>
      </div>
      <div>
        { canMove ? <div className="move"></div> : <div className="dont-move"></div>}
      </div>
      <div>
        {players.map(player => (
          <div key={player.id} className="player">
            ({player.id}) {player.name}
            <span style={{left: (progress[player.id] + 100) + 'px', backgroundColor: losers.indexOf(player.id) > -1 ? '#ff9999' : player.color}}></span>
          </div>
        ))}
      </div>
    </div>
  );
}