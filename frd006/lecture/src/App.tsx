import React, { useEffect, useState } from 'react';
import { Route, Switch } from 'react-router';
import { Link } from 'react-router-dom';
import './App.css';
import { BlogDetail } from './BlogDetail';
import { BlogList } from './BlogList';
import { Game } from './Game';
import { Homepage } from './Homepage';

function App() {
  // const [currentPage, setCurrentPage] = useState('homepage');

  return (
    <div>
      <div>
        <Link to="/">Homepage</Link>
        <Link to="/game">Game</Link>
        <Link to="/blogs">Blog</Link>
        {/* <a href="#" onClick={() => {
          window.history.pushState('', '', '/homepage')
          setCurrentPage('homepage')
        }}>Homepage</a>
        <a href="#" onClick={() => {
          window.history.pushState('', '', '/game')
          setCurrentPage('game')
        }}>Game</a> */}
      </div>
      <Switch>
        <Route path="/" exact><Homepage /></Route>
        <Route path="/game" exact><Game /></Route>
        <Route path="/blogs" exact><BlogList /></Route>
        <Route path="/blogs/:id" exact><BlogDetail /></Route>
        <div>Not Found!</div>
      </Switch>
      {/* { currentPage === 'homepage' && <Homepage /> } */}
      {/* { currentPage === 'game' && <Game /> } */}
    </div>
  )
}

export default App;
