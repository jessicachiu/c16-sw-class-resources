import { connectRouter, routerMiddleware, RouterState } from "connected-react-router";
import { createBrowserHistory } from "history";
import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import { productsReducer, ProductsState } from "./redux/products/reducer";
import { userReducer, UserState } from "./redux/user/reducer";
import { userBidsReducer, UserBidsState } from "./redux/userBids/reducer";

export const history = createBrowserHistory();

export interface RootState {
  userBids: UserBidsState,
  products: ProductsState,
  user: UserState,
  router: RouterState,
}

const reducer = combineReducers<RootState>({
  userBids: userBidsReducer,
  products: productsReducer,
  user: userReducer,
  router: connectRouter(history)
})

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
  }
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(reducer, composeEnhancers(
  applyMiddleware(routerMiddleware(history))
))