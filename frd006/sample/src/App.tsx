import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Link, Route, Switch } from 'react-router-dom';
import { Homepage } from './pages/Homepage';
import { FAQ } from './pages/FAQ';
import { BidCreate } from './pages/BidCreate';
import { BidHistory } from './pages/BidHistory';
import { BidDetail } from './pages/BidDetail';
import { Login } from './pages/Login';

function App() {
  return (
    <div className="App">
      <div className="nav">
        <Link to="/">Homepage</Link>
        <Link to="/faq">FAQ</Link>
        <Link to="/bid/create">Bid Your Stuff</Link>
        <Link to="/bid/history">My Bids</Link>
        <Link to="/login">Login/Register</Link>
      </div>
      <Switch>
        <Route path="/" exact><Homepage /></Route>
        <Route path="/faq"><FAQ /></Route>
        <Route path="/bid/create"><BidCreate /></Route>
        <Route path="/bid/history"><BidHistory /></Route>
        <Route path="/bid/:id"><BidDetail /></Route>
        <Route path="/login"><Login /></Route>
      </Switch>
    </div>
  );
}

export default App;
