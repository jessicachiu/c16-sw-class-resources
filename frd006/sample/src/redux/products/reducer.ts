export interface Product {
  id: number;
  name: string;
  latestBid: number;
  deadline: number; // timestamp
  image: string; // url
  minimumBid: number;
  eachBidAmount: number;
  username: string;
  categoryId: number;
}

export interface Category {
  id: number;
  name: string;
  order: number;
  productIds: number[];
}

export interface ProductsState {
  products: {
    [id: string]: Product
  };
  categories: {
    [id: string]: Category
  },
}

const initialState: ProductsState = {
  products: {
    '1': {
      id: 1,
      name: 'PS5',
      latestBid: 5,
      deadline: Date.now() + 120000,
      image: 'https://i.expansys.net/img/p/328407/playstation-5-gaming-console.jpg',
      minimumBid: 10,
      eachBidAmount: 10,
      username: 'Alex',
      categoryId: 1
    },
    '2': {
      id: 2,
      name: 'Switch',
      latestBid: 5,
      deadline: Date.now() + 120000,
      image: 'https://i.expansys.net/img/p/328407/playstation-5-gaming-console.jpg',
      minimumBid: 10,
      eachBidAmount: 10,
      username: 'Alex',
      categoryId: 1
    },
    '3': {
      id: 3,
      name: 'Lumia 1020',
      latestBid: 5000,
      deadline: Date.now() + 240000,
      image: 'https://i.expansys.net/img/p/328407/playstation-5-gaming-console.jpg',
      minimumBid: 15,
      eachBidAmount: 15,
      username: 'Alex',
      categoryId: 2
    },
    '4': {
      id: 4,
      name: 'iPhone 13 Pro Max',
      latestBid: 3000,
      deadline: Date.now() + 120000,
      image: 'https://i.expansys.net/img/p/328407/playstation-5-gaming-console.jpg',
      minimumBid: 100,
      eachBidAmount: 100,
      username: 'Gordon',
      categoryId: 2
    },
  },
  categories: {
    '1': {
      id: 1,
      name: 'Console',
      order: 99,
      productIds: [1, 2]
    },
    '2': {
      id: 1,
      name: 'Cellphone',
      order: 1,
      productIds: [3, 4]
    },
  }
}

export function productsReducer(state: ProductsState = initialState): ProductsState {
  return initialState
}