import { push } from "connected-react-router"
import { differenceInSeconds } from "date-fns"
import format from "date-fns/format"
import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "../store"

export function BidItem(props: {
  id: number,
  hideTime?: boolean
}) { 
  const product = useSelector((state: RootState) => state.products.products[props.id])
  const [interval, setInterval] = useState(differenceInSeconds(product.deadline, new Date()))

  const dispatch = useDispatch();
 
  useEffect(() => {
    const timerId = window.setInterval(() => {
      setInterval(differenceInSeconds(product.deadline, new Date()))

    }, 1000);

    return () => {
      window.clearInterval(timerId)
    }
  }, [setInterval])

  return (
    <div className="bid-item" onClick={() => {
      dispatch(push(`/bid/${product?.id}`))
    }}>
      {product && (<>
        <img src={product.image} />
        <h3>{product.name}</h3>
        <p>Bid: {product.latestBid}</p>
        <p>Seller: {product.username}</p>
        { !props.hideTime && <p>尚餘: {interval}秒</p> }
      </>)}
  </div>
  )
}