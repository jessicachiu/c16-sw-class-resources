import { Server } from "http";
import { Page, chromium, Browser } from "playwright";
import { startServer } from "./main";
describe("Login", () => {
    let page: Page;
    let browser: Browser;
    let server: Server;
    beforeAll(async () => {
        browser = await chromium.launch({
            headless: true,
        });
        page = await browser.newPage();
        server = await startServer();
    });
    afterAll(async () => {
        await page.close();
        await browser.close();
        server.close();
    });

    it('should display "Login" text on title', async () => {
        await page.goto("http://localhost:8080");
        const title = await page.title();
        expect(title).toContain("Login");
    });

    it("should successfully login", async () => {
        await page.goto("http://localhost:8080");

        await page.$("[name=username]");
        // console.log("pageSelectResult = ", pageSelectResult);

        await page.screenshot({ path: "./login_init.png" });
        await page.evaluate(() => {
            // Inisde fake Browser

            const username = document.querySelector("[name=username]");
            // console.log("username  =", username);

            const password = document.querySelector("[name=password]");
            if (!username) {
                throw new Error("No username input found");
            }
            if (!password) {
                throw new Error("No password input found");
            }
            if (username && password) {
                (username as HTMLInputElement).value = "tecky";
                (password as HTMLInputElement).value = "tecky";
            }
        });
        await page.screenshot({ path: "./login_typed.png" });
        await page.evaluate(() => {
            const submit = document.querySelector("[type=submit]");
            if (!submit) {
                throw new Error("No submit button found");
            }

            if (submit) {
                (submit as HTMLInputElement).click();
            }
        });
        await page.waitForNavigation();
        await page.screenshot({ path: "./home.png" });
        const studentMain = await page.evaluate(() => document.querySelector("#student-main"));
        const title = await page.title();
        expect(title).toContain("Home");
        expect(studentMain).toBeDefined(); // <---- not as expected
        expect(studentMain).toBeTruthy();
        expect(studentMain).not.toBeNull();
    });
});
