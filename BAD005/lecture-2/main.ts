import express from "express";
import { Server } from "http";
const app = express();

app.use(express.static("public"));

export function startServer() {
    return new Promise<Server>((resolve, reject) => {
        let server = app.listen(8080, () => {
            console.log(`Listening on  http://localhost:8080`);
        });
        resolve(server);
    });
}
