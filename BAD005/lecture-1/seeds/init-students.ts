import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("students").del();

    // Inserts seed entries
    await knex("students").insert([
        { name: "Peter", level: "30", status: true, date_of_birth: "1999-01-01" },
        { name: "Amy", level: "130", status: false, date_of_birth: "2000-01-01" },
        { name: "Sam", level: "0", status: true, date_of_birth: "1998-01-01" },
        { name: "Timmy", level: "70", status: true, date_of_birth: "1989-01-01" },
    ]);
}
