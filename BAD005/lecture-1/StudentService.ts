import { Knex } from "knex";

export class StudentService {
    constructor(private knex: Knex) {}

    getStudents = async () => {
        const results = await this.knex.select("name", "level").from("students");
        return results;
    };

    getActiveStudents = async () => {
        const results = await this.knex.select("*").from("students").where({
            status: true,
        });
        return results;
    };

    deleteStudents = async () => {
        // this.knex("students").del();
        await this.knex.delete().from("students");
    };
    updateStudents = async (option: any) => {
        const results = await this.knex("students").update(option).returning(["name", "level", "status"]);
        return results;
    };
}
