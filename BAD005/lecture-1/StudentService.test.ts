import Knex from "knex";
const knexfile = require("./knexfile"); // Assuming you test case is inside `services/ folder`
const knex = Knex(knexfile["testing"]); // Now the connection is a testing connection.
import { StudentService } from "./StudentService";

describe("StudentService.ts", () => {
    let studentService: StudentService;
    let sampleData: any;
    beforeAll(async () => {
        await knex.migrate.rollback({}, true);
        await knex.migrate.latest();
        await knex.seed.run();
    });
    afterAll(async () => {
        // await knex.seed.run();
        knex.destroy();
    });

    beforeEach(async () => {
        studentService = new StudentService(knex);
        sampleData = {
            name: "Peter",
            level: "30",
            date_of_birth: "1990-01-01",
        };
        await knex("students").del();
        await knex.insert(sampleData).into("students");
    });

    it("should get all students", async () => {
        const students = await studentService.getStudents();
        expect(students.length).toBe(1);
        delete sampleData["date_of_birth"];
        expect(students[0]).toEqual(sampleData);
    });

    it("should get all active students", async () => {
        const students = await studentService.getActiveStudents();
        expect(students.length).toBe(1);
    });

    it("should delete all students", async () => {
        await studentService.deleteStudents();
        const students = await studentService.getStudents();
        expect(students.length).toBe(0);
    });

    it("should get updated students", async () => {
        let options = {
            name: "Bobby",
            level: "0",
            status: false,
        };
        const students = await studentService.updateStudents(options);
        const student = students[0];
        delete student["date_of_birth"];
        expect(students.length).toBe(1);
        expect(student).toEqual({ name: "Bobby", level: "0", status: false });
    });
});
