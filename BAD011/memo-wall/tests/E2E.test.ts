import { Page, chromium, Browser } from "playwright";
import "../server"; //<--start server

jest.mock("../logger");

xdescribe("E2E Test", () => {
    let browser: Browser;
    let page: Page;

    //console.log = jest.fn();

    beforeAll(async () => {
        browser = await chromium.launch({
            headless: true,
        });
        page = await browser.newPage();
    });

    test("E2E Login", async () => {
        await page.goto("http://localhost:8080");
        await page.evaluate(() => {
            const username = document.querySelector("[name=username]");
            const password = document.querySelector("[name=password]");

            (username as HTMLInputElement).value = "admin@tecky.io";
            (password as HTMLInputElement).value = "123456";

            const submit = document.querySelector("#loginBtn");
            if (submit) {
                (submit as HTMLInputElement).click();
            }
        });
        const adminTitle = await page.evaluate(() => {
            return document.querySelector("#admin-name");
        });
        expect(adminTitle).toBeDefined();
    });

    test("Get All Memo", async () => {
        await page.goto("http://localhost:8080");
        await page.evaluate(() => {
            const memoTextArea = document.querySelector("[name=content]");
            (memoTextArea as HTMLInputElement).value = "Memo#3";

            const submit = document.querySelector("#memo-form");
            if (submit) {
                (submit as HTMLInputElement).click();
            }
        });
        const newMemos = await page.evaluate(() => [document.querySelector("#edit-1"), document.querySelector("#edit-2"), document.querySelector("#edit-3")]);
        expect(newMemos.length).toBe(3);
    });

    afterAll(async () => {
        await page.close();
        await browser.close();
    });
});
