import { loadMemos } from "./memo.js";
const err = new URL(window.location.href).searchParams.get("err");
if (err) {
    alert(err);
}

async function getCurrentUser() {
    const res = await fetch("/user");
    const result = await res.json();
    const adminDiv = document.querySelector("#admin-from");
    const userInfoDiv = document.querySelector("#userInfo");
    const loginName = document.querySelector("#loginName");
    if (result.user) {
        adminDiv.classList.remove("show");
        adminDiv.classList.add("hide");

        userInfoDiv.classList.remove("hide");
        userInfoDiv.classList.add("show");
        loginName.innerHTML = result.user.username || result.user.name;
    } else {
        adminDiv.classList.remove("hide");
        adminDiv.classList.add("show");

        userInfoDiv.classList.remove("show");
        userInfoDiv.classList.add("hide");

        loginName.innerHTML = "";
    }
}
window.onload = () => {
    loadMemos();
    getCurrentUser();
};

//create new-memo-form submit event
document.querySelector("#memo-form").addEventListener("submit", async (event) => {
    event.preventDefault();
    const form = event.target;
    const formData = new FormData();
    formData.append("content", form.content.value);
    formData.append("image", form.image.files[0]);

    const res = await fetch("/memo", {
        method: "POST",
        body: formData,
    });
    const result = await res.json();
    if (result.success) {
        await loadMemos();
        form.reset();
        document.querySelector("#image-preview1").src = "";
        document.querySelector(".custom-file-label").innerHTML = "";
    }
});
//create edit-memo-form submit event
document.querySelector("#edit-memo-form").addEventListener("submit", async (event) => {
    event.preventDefault();
    const form = event.target;
    const formData = new FormData();
    formData.append("content", form.content.value);
    formData.append("image", form.image.files[0]);

    const res = await fetch(`/memo/${editMemoId}`, {
        method: "PUT",
        body: formData,
    });
    const result = await res.json();
    if (result.success) {
        await loadMemos();
        form.reset();
        document.querySelector("#image-preview2").src = "";
        document.querySelector(".custom-file-label").innerHTML = "";
        editMemoId = -1;

        const editForm = document.querySelector("#edit-memo-form");
        const newForm = document.querySelector("#memo-form");

        newForm.classList.remove("hide");
        newForm.classList.add("show");
        editForm.classList.remove("show");
        editForm.classList.add("hide");
    }
});

//create admin-form submit event
document.querySelector("#admin-from").addEventListener("submit", async (event) => {
    event.preventDefault();
    const form = event.target;
    const body = {
        username: form.username.value,
        password: form.password.value,
    };
    const res = await fetch("/login", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(body),
    });
    const result = await res.json();
    if (result.user) {
        console.log("login success");
        window.location = "/admin/admin.html";
    } else {
        document.querySelector("#login-result").innerHTML = result.error;
    }
});

document.querySelector("#userInfo").addEventListener("submit", async () => {
    const res = await fetch("/logout"); // GET /logout
    const result = await res.json();
    if (result.success) {
        window.location = "/";
    }
});

document.querySelector("#inputGroupFile01").addEventListener("change", function (event) {
    const [file] = event.target.files;
    if (file) {
        document.querySelector("#image-preview1").src = URL.createObjectURL(file);
        document.querySelector(".custom-file-label").innerHTML = file.name;
    }
});
document.querySelector("#inputGroupFile02").addEventListener("change", function (event) {
    const [file] = event.target.files;
    if (file) {
        document.querySelector("#image-preview2").src = URL.createObjectURL(file);
        document.querySelector(".custom-file-label").innerHTML = file.name;
    }
});
vex.dialog.confirm({
    message: "Are you absolutely sure you want to destroy the alien planet?",
    callback: function (value) {
        if (value) {
            console.log("Successfully destroyed the planet.");
        } else {
            console.log("Chicken.");
        }
    },
});
