import React, { useEffect } from 'react';
import logo from './logo.svg';
import './App.css';
import { Link, Route, Switch } from 'react-router-dom';
import { Homepage } from './pages/Homepage';
import { FAQ } from './pages/FAQ';
import { BidCreate } from './pages/BidCreate';
import { BidHistory } from './pages/BidHistory';
import { BidDetail } from './pages/BidDetail';
import { Login } from './pages/Login';
import { useDispatch, useSelector } from 'react-redux';
import { checkCurrentUser, logoutThunk } from './redux/user/actions';
import { RootState } from './store';

function App() {
  const dispatch = useDispatch();
  const isAuthenticate = useSelector((state: RootState) => state.user.isAuthenticate)

  useEffect(() => {
    dispatch(checkCurrentUser());
  }, [])
  
  return (
    <div className="App">
      <div className="nav">
        <Link to="/">Homepage</Link>
        <Link to="/faq">FAQ</Link>
        <Link to="/bid/create">Bid Your Stuff</Link>
        <Link to="/bid/history">My Bids</Link>
        { !isAuthenticate && <Link to="/login">Login/Register</Link> }
        { isAuthenticate && <a href="#" onClick={e => {
          e.preventDefault();
          dispatch(logoutThunk());
        }}>Logout</a>}
      </div>
      <Switch>
        <Route path="/" exact><Homepage /></Route>
        <Route path="/faq"><FAQ /></Route>
        { isAuthenticate && <Route path="/bid/create"><BidCreate /></Route> }
        <Route path="/bid/history"><BidHistory /></Route>
        <Route path="/bid/:id"><BidDetail /></Route>
        <Route path="/login"><Login /></Route>
      </Switch>
    </div>
  );
}

export default App;
