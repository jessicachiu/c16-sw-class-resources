import fetchMock from 'fetch-mock';
import configureMockStore, { MockStoreEnhanced } from 'redux-mock-store'
import thunk from 'redux-thunk'
import { RootState, RootThunkDispatch } from '../../store';
import { fetchCategories, loadCategories, loadProducts } from './actions';

describe('Product thunks', () => {
  let store: MockStoreEnhanced<RootState, RootThunkDispatch>;

  beforeEach(() => {
    const mockStore = configureMockStore<RootState, RootThunkDispatch>([thunk]);
    store = mockStore();
  })

  afterEach(() => {
    fetchMock.restore()
  });
  
  it('fetch categories and products', async () => {
    const categoriesResult = [{"id":1,"name":"Console","order":99,"productIds":[1,2]},{"id":2,"name":"Cellphone","order":1,"productIds":[3,4]}];
    
    fetchMock.get(`${process.env.REACT_APP_BACKEND_URL}/categories`, {
      body: categoriesResult,
      status: 200
    });

    fetchMock.get(`${process.env.REACT_APP_BACKEND_URL}/categories/1/products`, {
      body: JSON.stringify('1'),
      status: 200
    });

    fetchMock.get(`${process.env.REACT_APP_BACKEND_URL}/categories/2/products`, {
      body: JSON.stringify('2'),
      status: 200
    });

    await store.dispatch(fetchCategories());

    expect(store.getActions()).toEqual([
      loadProducts('1' as any, 1),
      loadProducts('2' as any, 2),
      loadCategories(categoriesResult)
    ])
  })
})