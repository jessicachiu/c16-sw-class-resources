import { Dispatch } from "react";
import { RootState, RootThunkDispatch } from "../../store";
import axios from 'axios'

export interface Product {
  id: number;
  name: string;
  latestBid: number;
  deadline: number; // timestamp
  image: string; // url
  minimumBid: number;
  eachBidAmount: number;
  username: string;
  categoryId: number;
}

export interface Category {
  id: number;
  name: string;
  order: number;
  productIds: number[];
}

// (ordinary) Action creator (returns object)

export function loadCategories(categories: Category[]) {
  return {
    type: '@@products/LOAD_CATEGORIES' as const,
    categories
  }
}

export function loadProducts(products: Product[], categoryId: number) {
  return {
    type: '@@products/LOAD_PRODUCTS' as const,
    products,
    categoryId
  }
}

export type ProductsActions = ReturnType<typeof loadCategories> | 
                              ReturnType<typeof loadProducts>;

// Thunk action creator (returns function)

export function fetchCategories() {
  return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
    const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/categories`,
      // In case you wish to use sesssion:
      // {
      //   credentials: "include"
      // }
      {
        "headers": {
          "Authorization": `Bearer ${getState().user.token}`
        }
      }
    )
    const json = await res.json();

    if (!Array.isArray(json)) {
      return;
    }

    for (let category of json) {
      const res = await axios.get(`${process.env.REACT_APP_BACKEND_URL}/categories/${category.id}/products`, {
        headers: {
          Authorization: `Bearer ${getState().user.token}`
        }
      })
      const productJson = res.data

      dispatch(loadProducts(productJson, category.id));
    }

    dispatch(loadCategories(json));
  }
}