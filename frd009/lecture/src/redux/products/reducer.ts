import produce from "immer";
import { Category, Product, ProductsActions } from "./actions"

export interface ProductsState {
  products: {
    [id: string]: Product
  };
  categories: {
    [id: string]: Category
  } | null,
}

const initialState: ProductsState = {
  products: {},
  categories: null
}

export function productsReducer(state: ProductsState = initialState, action: ProductsActions): ProductsState {
  return produce(state, state => {
    if (action.type === '@@products/LOAD_CATEGORIES') {
      if (state.categories == null) {
        state.categories = {}
      }
      for (let category of action.categories) {
        state.categories[category.id] = category
      }
    } else if (action.type === '@@products/LOAD_PRODUCTS') {
      if (state.categories == null) {
        state.categories = {}
      }
      for (let product of action.products) {
        state.products[product.id] = product
      }
      if (state.categories[action.categoryId] != null) {
        state.categories[action.categoryId].productIds = action.products.map(p => p.id)
      }
    }
  });
}