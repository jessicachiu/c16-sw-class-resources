import { push } from "connected-react-router"
import { differenceInSeconds } from "date-fns"
import format from "date-fns/format"
import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "../store"
import { loadCategories, loadProducts } from "../redux/products/actions"
import { useProducts } from "../hooks/useProducts"

export function BidItem(props: {
  id: number,
  hideTime?: boolean
}) { 
  const isLoading = useProducts();

  const product = useSelector((state: RootState) => state.products.products[props.id])
  const [interval, setInterval] = useState(differenceInSeconds(product?.deadline, new Date()))

  const dispatch = useDispatch();
  
  useEffect(() => {
    const timerId = window.setInterval(() => {
      setInterval(differenceInSeconds(product?.deadline, new Date()))

    }, 1000);

    return () => {
      window.clearInterval(timerId)
    }
  }, [product, setInterval])

  return (
    <div className="bid-item" onClick={() => {
      dispatch(push(`/bid/${product?.id}`))
    }}>
      {isLoading && <>載入中，請稍後⋯⋯</>}
      {!isLoading && product == null && <>無此產品</>}
      {product && (<>
        <img src={product.image} />
        <h3>{product.name}</h3>
        <p>Bid: {product.latestBid}</p>
        <p>Seller: {product.username}</p>
        { !props.hideTime && <p>尚餘: {interval}秒</p> }
      </>)}
  </div>
  )
}