import { format } from "date-fns"
import { useEffect, useState } from "react"
import { useParams } from "react-router"
import { BidItem } from "../components/BidItem"

export function BidDetail() {
  const productId = parseInt(useParams<{id: string}>().id)
  const [bidsHistory, setBidsHistory] = useState<{
    username: string,
    amount: number,
    date: number
  }[]>([])

  useEffect(() => {
    // [TODO] ajax/fetch here
    setBidsHistory([
      {username: 'alex', amount: 999, date: Date.now() - 1000}
    ])
  }, [setBidsHistory])

  return (
    <div>
      <h1>BidDetail</h1>

      <BidItem id={productId} />

      <h3>Bid History</h3>
      {bidsHistory.map(bidHistory => (
        <div>
          <p>Buyer: {bidHistory.username}</p>
          <p>Amount: {bidHistory.amount}</p>
          <p>Date: {format(bidHistory.date, 'yyyy-MM-dd HH:mm:ss')}</p>
        </div>
      ))}

      <h3>Your Bid Here</h3>
      <form>
        <input />
        <input type="submit" />
      </form>
    </div>
  )
}