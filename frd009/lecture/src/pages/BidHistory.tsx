import { useSelector } from "react-redux"
import { BidItem } from "../components/BidItem"
import { RootState } from "../store"

export function BidHistory() {
  const finishedBids = useSelector((state: RootState) => state.userBids.bids.filter(bid => bid.finish))
  const unfinishedBids = useSelector((state: RootState) => state.userBids.bids.filter(bid => !bid.finish))

  return (
    <div>
      <h1>BidHistory</h1>
      
      <div>
        <h2>Current Bids</h2>
        <div className="bid-list">
        {unfinishedBids.map(bid => {
          return (
            <div>
              <p>Success: {bid.success ? '成功' : '唔夠高價'}</p>
              <BidItem id={bid.productId} />
            </div>
          )
        })}
        </div>
      </div>

      <div>
        <h2>Past Bids</h2>
        <div className="bid-list">
        {finishedBids.map(bid => {
          return (
            <div>
              <p>Success: {bid.success ? '成功' : '唔夠高價'}</p>
              <BidItem id={bid.productId} hideTime={true} />
            </div>
          )
        })}
        </div>
      </div>

    </div>
  )
}