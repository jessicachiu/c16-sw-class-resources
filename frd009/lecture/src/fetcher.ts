export const fetcher = (input: RequestInfo, init?: RequestInit) => fetch(input, init).then(r => r.json())

export const fetchProductsWithCategories = (categories: number[]) => {
  return Promise.all(categories.map(id => fetcher(`${process.env.REACT_APP_BACKEND_URL}/categories/${id}/products`)))
}