import express from 'express'
import cors from 'cors'
import jwtSimple from 'jwt-simple'
import jwt from './jwt'
import { isLoggedIn } from './guards'
import axios from 'axios'
import dotenv from 'dotenv'
import Knex from 'knex'

dotenv.config()

const knex = Knex(require('./knexfile')[process.env.NODE_ENV ?? 'development'])

const app = express()

app.use(express.json())
app.use(express.urlencoded())

app.use(cors({
  origin: [process.env.FRONTEND_URL!]
}))

app.post('/login', (req, res) => {
  if (req.body.username === 'alex' && req.body.password === '123') {
    // session way:
    // req.session.id = 1;

    // jwt way:
    const payload = {
      id: 1
    }
    const token = jwtSimple.encode(payload, jwt.jwtSecret)
    
    res.json({
      token: token
    })
  } else {
    res.status(401).json({
      token: null,
      message: 'Incorrect username or password'
    })
  }
})

app.post('/login/facebook', async (req, res) => {
  try {
    const accessToken = req.body.accessToken
    // @ts-ignore
    const facebookRes = await axios.get(`https://graph.facebook.com/me?access_token=${accessToken}&fields=id,name,email,picture`)
  
    // UserService find or create user
    //
    // Same as WSP

    // Give a JWT to frontend
    const payload = {
      id: 1 // real user database id
    }
    const token = jwtSimple.encode(payload, jwt.jwtSecret)
    
    res.json({
      token: token
    })
  } catch (e) {
    console.error(e)
    res.status(401).json({
      token: null,
      message: 'Incorrect token'
    })
  }
})

app.get('/user/current', isLoggedIn, (req, res) => {
  res.json(req.user)
})

app.get('/users', async (req, res) => {
  res.json(await knex.select('*').from('users'))
})

app.get('/categories/:id/products', isLoggedIn, (req, res) => {
  if (req.params.id === '1') {
    res.json([
      {
        id: 1,
        name: 'PS5',
        latestBid: 5,
        deadline: Date.now() + 120000,
        image: 'https://i.expansys.net/img/p/328407/playstation-5-gaming-console.jpg',
        minimumBid: 10,
        eachBidAmount: 10,
        username: 'Alex',
        categoryId: 1
      },
      {
        id: 2,
        name: 'Switch',
        latestBid: 5,
        deadline: Date.now() + 120000,
        image: 'https://i.expansys.net/img/p/328407/playstation-5-gaming-console.jpg',
        minimumBid: 10,
        eachBidAmount: 10,
        username: 'Alex',
        categoryId: 1
      }
    ]);
  } else {
    res.json([
      {
        id: 3,
        name: 'Lumia 1020',
        latestBid: 5000,
        deadline: Date.now() + 240000,
        image: 'https://i.expansys.net/img/p/328407/playstation-5-gaming-console.jpg',
        minimumBid: 15,
        eachBidAmount: 15,
        username: 'Alex',
        categoryId: 2
      },
      {
        id: 4,
        name: 'iPhone 13 Pro Max',
        latestBid: 3000,
        deadline: Date.now() + 120000,
        image: 'https://i.expansys.net/img/p/328407/playstation-5-gaming-console.jpg',
        minimumBid: 100,
        eachBidAmount: 100,
        username: 'Gordon',
        categoryId: 2
      },
    ])
  }
});

app.get('/categories', isLoggedIn, (req, res) => {
  res.json([
    {
      id: 1,
      name: 'Console',
      order: 99,
      productIds: [1, 2]
    },
    {
      id: 2,
      name: 'Cellphone',
      order: 1,
      productIds: [3, 4]
    }
  ])
});

app.get('/userBids', (req, res) => {
  res.json([
    {productId: 1, amount: 200, finish: true, success: false},
    {productId: 2, amount: 300, finish: false, success: false},
    {productId: 3, amount: 400, finish: true, success: true},
    {productId: 4, amount: 500, finish: false, success: true},
  ])
});

const port = process.env.PORT ?? 8080
app.listen(port, () => {
  console.log(`Listening on port ${port}`)
})