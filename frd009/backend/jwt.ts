import dotenv from 'dotenv'
dotenv.config()

export default {
  // symmetric 對稱的, 唔安全的!
  // 真正 production 請用 asymmetric (public key/private key)
  jwtSecret: process.env.JWT_SECRET!,
  jwtSession: {
    session: false
  }
}